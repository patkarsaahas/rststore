// actions/courierActions.js
import axios from 'axios';
import {
  FETCH_AVAILABLE_COURIERS_REQUEST,
  FETCH_AVAILABLE_COURIERS_SUCCESS,
  FETCH_AVAILABLE_COURIERS_FAIL
} from '../constants/shipConstants';
import { GetToken } from '../shiprocket';

export const fetchAvailableCouriers = (params) => async (dispatch) => {
  try {
    dispatch({ type: FETCH_AVAILABLE_COURIERS_REQUEST });

    const token = await GetToken({
      email: 'patkarsaahas@gmail.com',
      password: 'tempUse@486',
    });

    const { data } = await axios.get(
      'https://apiv2.shiprocket.in/v1/external/courier/serviceability/',
      {
        params,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      }
    );

    dispatch({
      type: FETCH_AVAILABLE_COURIERS_SUCCESS,
      payload: data.data.available_courier_companies
    });
    localStorage.setItem('courierInfo', JSON.stringify(data));
  } catch (error) {
    dispatch({
      type: FETCH_AVAILABLE_COURIERS_FAIL,
      payload: error.response && error.response.data.message
        ? error.response.data.message
        : error.message,
    });
  }
};

export const generateAWB = async (shipmentId) => {
  try {
    const token = await GetToken({
      email: 'patkarsaahas@gmail.com',
      password: 'tempUse@486',
    });

    const response = await axios.post(
      'https://apiv2.shiprocket.in/v1/external/courier/assign/awb',
      { shipment_id: shipmentId },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    if (response.data) {
      console.log('AWB Assigned:', response.data);
      return response.data;
    } else {
      throw new Error('AWB assignment failed.');
    }
  } catch (error) {
    console.error('Error generating AWB:', error);
    throw error;
  }
};

