import axios from 'axios';
import {
	ORDER_CREATE_FAIL,
	ORDER_CREATE_REQUEST,
	ORDER_CREATE_SUCCESS,
	ORDER_DETAILS_FAIL,
	ORDER_DETAILS_REQUEST,
	ORDER_DETAILS_SUCCESS,
	ORDER_PAY_REQUEST,
	ORDER_PAY_SUCCESS,
	ORDER_PAY_FAIL,
	ORDER_LIST_REQUEST,
	ORDER_LIST_SUCCESS,
	ORDER_LIST_FAIL,
	ORDER_ADMIN_REQUEST,
	ORDER_ADMIN_SUCCESS,
	ORDER_ADMIN_FAIL,
	ORDER_DELIVER_REQUEST,
	ORDER_DELIVER_SUCCESS,
	ORDER_DELIVER_FAIL,
	ORDER_TOTAL_SALES_FAIL,
	ORDER_TOTAL_SALES_REQUEST,
	ORDER_TOTAL_SALES_SUCCESS,
	ORDER_RAZOR_CREATE_REQUEST,
	ORDER_RAZOR_CREATE_SUCCESS,
	ORDER_RAZOR_CREATE_FAIL,
	ORDER_RAZORPAY_REQUEST,
	ORDER_RAZORPAY_SUCCESS,
	ORDER_RAZORPAY_FAIL,
	ORDER_UPDATE_SHIPROCKET_ID_REQUEST,
	ORDER_UPDATE_SHIPROCKET_ID_SUCCESS,
	ORDER_UPDATE_SHIPROCKET_ID_FAIL,
	ORDER_CANCEL_REQUEST,
	ORDER_CANCEL_SUCCESS,
	ORDER_CANCEL_FAIL,
} from '../constants/orderConstants';

export const createOrder = (order) => async (dispatch, getState) => {
	try {
		dispatch({ type: ORDER_CREATE_REQUEST });

		const {
			userLogin: { userInfo },
		} = getState();

		const config = {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${userInfo.token}`,
			},
		};

		const { data } = await axios.post('/api/orders', order, config);

		dispatch({ type: ORDER_CREATE_SUCCESS, payload: data });
		return data; // Ensure the created order data is returned
	} catch (error) {
		dispatch({
			type: ORDER_CREATE_FAIL,
			payload: error.response && error.response.data.message ? error.response.data.message : error.message,
		});
		throw new Error(error.message); // Throw error to handle in the calling function
	}
};

export const deleteOrder = (id) => async (dispatch, getState) => {
	try {
		dispatch({ type: ORDER_CANCEL_REQUEST });

		const {
			userLogin: { userInfo },
		} = getState();

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
			},
		};

		await axios.delete(`/api/orders/${id}`, config);

		dispatch({ type: ORDER_CANCEL_SUCCESS });
	} catch (err) {
		dispatch({
			type: ORDER_CANCEL_FAIL,
			payload:
				err.response && err.response.data.message
					? err.response.data.message
					: err.message,
		});
	}
};

export const createRazorPayOrder = (order) => async (dispatch, getState) => {
	try {
		dispatch({ type: ORDER_RAZOR_CREATE_REQUEST });

		const {
			userLogin: { userInfo },
		} = getState();

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
				'Content-Type': 'application/json',
			},
		};

		const { data } = await axios.post(`/api/orders/razorpay`, order, config);

		console.log("Razorpay Order Created:", data);

		
		dispatch({ type: ORDER_RAZOR_CREATE_SUCCESS, payload: data });
		
		return data;

	} catch (err) {
		console.error("Error creating Razorpay order:", err.response?.data || err.message);
		dispatch({
			type: ORDER_RAZOR_CREATE_FAIL,
			payload:
				err.response && err.response.data.message
					? err.response.data.message
					: err.message,
		});
		throw err; // Re-throw to handle the error in the caller
	}
};

export const getOrderDetails = (id) => async (dispatch, getState) => {
	try {
		dispatch({ type: ORDER_DETAILS_REQUEST });

		const {
			userLogin: { userInfo },
		} = getState();

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
			},
		}

		const { data } = await axios.get(`/api/orders/${id}`, config)

		dispatch({ type: ORDER_DETAILS_SUCCESS, payload: data });
	} catch (err) {
		dispatch({
			type: ORDER_DETAILS_FAIL,
			payload:
				err.response && err.response.data.message
					? err.response.data.message
					: err.message,
		});
	}
}


export const payOrder = (orderId, paymentResult) => async (dispatch, getState) => {
	try {
		dispatch({ type: ORDER_PAY_REQUEST })
		const {
			userLogin: { userInfo },

		} = getState();

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
				'Content-Type': 'application/json',
			}
		}

		const { data } = await axios.put(`/api/orders/${orderId}/pay`, paymentResult, config);

		dispatch({ type: ORDER_PAY_SUCCESS, payload: data });
	} catch (err) {
		dispatch({
			type: ORDER_PAY_FAIL,
			payload:
				err.response && err.response.data.message
					? err.response.data.message
					: err.message,
		});
	}
}


export const payRazorOrder = (orderId, paymentResult) => async (dispatch, getState) => {
	try {
		dispatch({ type: ORDER_RAZORPAY_REQUEST })
		const {
			userLogin: { userInfo },

		} = getState();

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
				'Content-Type': 'application/json',
			}
		}

		const { data } = await axios.put(`/api/orders/razorpay/${orderId}/pay`, {paymentResult}, config);

		dispatch({ type: ORDER_RAZORPAY_SUCCESS, payload: data });
	} catch (err) {
		dispatch({
			type: ORDER_RAZORPAY_FAIL,
			payload:
				err.response && err.response.data.message
					? err.response.data.message
					: err.message,
		});
	}
}


export const listMyOrders = () => async (dispatch, getState) => {
	try {
		dispatch({ type: ORDER_LIST_REQUEST });

		const {
			userLogin: {
				userInfo
			},
		} = getState();

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
			}
		}

		const { data } = await axios.get(`/api/orders/myorders`, config)

		dispatch({ type: ORDER_LIST_SUCCESS, payload: data });
	} catch (err) {
		dispatch({
			type: ORDER_LIST_FAIL,
			payload:
				err.response && err.response.data.message
					? err.response.data.message
					: err.message,
		});
	}
}

export const listOrders = () => async (dispatch, getState) => {
	try {
		dispatch({ type: ORDER_ADMIN_REQUEST });

		const {
			userLogin: { userInfo },
		} = getState();

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
			},
		};

		const { data } = await axios.get(`/api/orders`, config);
		console.log(data);

		dispatch({ type: ORDER_ADMIN_SUCCESS, payload: data })
	} catch (err) {
		dispatch({ type: ORDER_ADMIN_FAIL, payload: err.response && err.response.data.message ? err.response.data.message : err.message, })
	}
}

export const deliverOrder = (order) => async (dispatch, getState) => {
	try {
		dispatch({ type: ORDER_DELIVER_REQUEST });

		const {
			userLogin: { userInfo },
		} = getState();

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
				'Content-Type': 'application/json',
			},
		};

		const { data } = await axios.put(`/api/orders/${order._id}/deliver`, order, config)

		dispatch({ type: ORDER_DELIVER_SUCCESS, payload: data });
	} catch (err) {
		dispatch({
			type: ORDER_DELIVER_FAIL, payload: err.response && err.response.data.message
				? err.response.data.message
				: err.message,
		})
	}
}

export const ordersTotalSales = () => async (dispatch, getState) => {
	try {
		dispatch({ type: ORDER_TOTAL_SALES_REQUEST })

		const {
			userLogin: { userInfo }
		} = getState();

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`
			}
		}

		const { data } = await axios.get(`/api/orders/totalsales`, config);
		dispatch({ type: ORDER_TOTAL_SALES_SUCCESS, payload: data });
	} catch (err) {
		dispatch({
			type: ORDER_TOTAL_SALES_FAIL,
			payload:
				err.response && err.response.data.message ? err.response.data.message : err.message,
		})
	}
}

export const updateOrderWithShiprocketId = (orderId, shiprocketOrderId, shiprocketShipmentId) => async (dispatch, getState) => {
	try {
		dispatch({ type: ORDER_UPDATE_SHIPROCKET_ID_REQUEST });

		const { userLogin: { userInfo } } = getState();

		const config = {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${userInfo.token}`
			}
		};

		const { data } = await axios.put(`/api/orders/${orderId}/update-shiprocket-id`, { shiprocketOrderId, shiprocketShipmentId }, config);

		dispatch({
			type: ORDER_UPDATE_SHIPROCKET_ID_SUCCESS,
			payload: data
		});

	} catch (error) {
		dispatch({
			type: ORDER_UPDATE_SHIPROCKET_ID_FAIL,
			payload: error.response && error.response.data.message
				? error.response.data.message
				: error.message,
		});
	}
};