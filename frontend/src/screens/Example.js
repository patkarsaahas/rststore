import React, { useEffect, useState } from 'react'

const Example = () => {
    const [count, setCount] = useState(0);
    function handleClick(){
      setCount(count + 1)
    }
    useEffect(() => {
        // Update the document title using the browser API
        document.title = `You clicked ${count} times`;

      });
    
  return (
    <div>
        <p>You have clicked {count} times</p>
        <button onClick={handleClick}>Increment</button>
    </div>
  )
}

export default Example