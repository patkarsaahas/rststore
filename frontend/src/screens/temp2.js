// import { Box, Flex, Grid, Heading, Image, Link, Text, Button, Select } from '@chakra-ui/react';
// import { PayPalButtons, PayPalScriptProvider } from '@paypal/react-paypal-js';
// import { useEffect, useState } from 'react';
// import { useDispatch, useSelector } from 'react-redux';
// import { Link as RouterLink, useParams } from 'react-router-dom';
// import { deliverOrder, getOrderDetails, payOrder } from '../actions/orderActions';
// import Loader from '../components/Loader';
// import Message from '../components/Message';
// import { ORDER_DELIVER_RESET, ORDER_PAY_RESET } from '../constants/orderConstants';
// import { fetchAvailableCouriers } from '../actions/courierActions';

// const OrderScreen = () => {
// 	const dispatch = useDispatch();
// 	const { id: orderId } = useParams();
// 	const [selectedCourier, setSelectedCourier] = useState();
// 	const [estimatedDelivery, setEstimatedDelivery] = useState('');

// 	const orderDetails = useSelector((state) => state.orderDetails);
// 	const { order, loading, error } = orderDetails;

// 	const availableCouriersState = useSelector((state) => state.availableCouriers);
// 	const { loading: loadingCouriers, error: errorCouriers, availableCouriers } = availableCouriersState;

// 	const orderPay = useSelector((state) => state.orderPay);
// 	const { loading: loadingPay, success: successPay } = orderPay;

// 	const orderDeliver = useSelector((state) => state.orderDeliver);
// 	const { loading: loadingDeliver, success: successDeliver } = orderDeliver;

// 	const userLogin = useSelector((state) => state.userLogin);
// 	const { userInfo } = userLogin;

// 	const handleCourierChange = (e) => {
// 		const selectedCourier = availableCouriers.find(courier => courier.courier_name === e.target.value);
// 		setSelectedCourier(selectedCourier);
// 		if (selectedCourier) {
// 			setEstimatedDelivery(selectedCourier.etd); // Assuming the ETD is part of the courier object
// 		}
// 	};

// 	const calculateTotalPrice = () => {
// 		return (
// 			order.itemsPrice +
// 			(selectedCourier ? selectedCourier.rate : order.shippingPrice) +
// 			order.taxPrice
// 		);
// 	};

// 	useEffect(() => {
// 		dispatch({ type: ORDER_PAY_RESET });
// 		dispatch({ type: ORDER_DELIVER_RESET });

// 		if (!order || successPay || successDeliver || order._id !== orderId) {
// 			dispatch(getOrderDetails(orderId));
// 		} else if (order.shippingAddress) {
// 			const storedCourierInfo = localStorage.getItem('courierInfo');
// 			if (storedCourierInfo) {
// 				const storedCouriers = JSON.parse(storedCourierInfo).data.available_courier_companies;
// 				dispatch({
// 					type: 'FETCH_AVAILABLE_COURIERS_SUCCESS',
// 					payload: storedCouriers,
// 				});
// 			} else {
// 				const params = {
// 					pickup_postcode: '400068',
// 					delivery_postcode: order.shippingAddress.postalCode,
// 					weight: 2,
// 					cod: order.paymentMethod === 'COD' ? 1 : 0,
// 				};
// 				dispatch(fetchAvailableCouriers(params));
// 			}
// 		}
// 		if (selectedCourier) {
// 			setEstimatedDelivery(selectedCourier.etd);
// 		}
// 	}, [dispatch, orderId, order, successPay, successDeliver, selectedCourier]);

// 	const successPaymentHandler = (paymentResult) => {
// 		dispatch(payOrder(orderId, paymentResult));
// 	};

// 	const deliverHandler = () => dispatch(deliverOrder(order));

// 	console.log("Available Couriers State:", availableCouriersState);
// 	return loading ? (
// 		<Loader />
// 	) : error ? (
// 		<Message type='error'>{error}</Message>
// 	) : (
// 		<>
// 			<Flex w='full' py='5' direction='column'>
// 				<Grid templateColumns='3fr 2fr' gap='20'>
// 					{/* Column 1 */}
// 					<Flex direction='column'>
// 						{/* Shipping */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Shipping
// 							</Heading>
// 							<Text>
// 								Name: <strong>{order.user.name}</strong>
// 							</Text>
// 							<Text>
// 								Email:{' '}
// 								<strong>
// 									<a href={`mailto:${order.user.email}`}>{order.user.email}</a>
// 								</strong>
// 							</Text>
// 							<Text>
// 								<strong>Address: </strong>
// 								{order.shippingAddress.address}, {order.shippingAddress.city},{' '}
// 								{order.shippingAddress.postalCode},{' '}
// 								{order.shippingAddress.country}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isDelivered ? (
// 									<Message type='success'>
// 										Delivered on {order.deliveredAt}
// 									</Message>
// 								) : (
// 									<Message type='warning'>Not Delivered</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Payment Method */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Payment Method
// 							</Heading>
// 							<Text>
// 								<strong>Method: </strong>
// 								{order.paymentMethod?.toUpperCase()}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isPaid ? (
// 									<Message type='success'>Paid on {order.paidAt}</Message>
// 								) : (
// 									<Message type='warning'>Not Paid</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Order Items */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Order Items
// 							</Heading>
// 							<Box>
// 								{order.orderItems.length === 0 ? (
// 									<Message>No Order Info</Message>
// 								) : (
// 									<Box py='2'>
// 										{order.orderItems.map((item, idx) => (
// 											<Flex
// 												key={idx}
// 												alignItems='center'
// 												justifyContent='space-between'>
// 												<Flex py='2' alignItems='center'>
// 													<Image
// 														src={item.image}
// 														alt={item.name}
// 														w='12'
// 														h='12'
// 														objectFit='cover'
// 														mr='6'
// 													/>
// 													<Link
// 														fontWeight='bold'
// 														fontSize='xl'
// 														as={RouterLink}
// 														to={`/products/${item.product}`}>
// 														{item.name}
// 													</Link>
// 												</Flex>

// 												<Text fontSize='lg' fontWeight='semibold'>
// 													{item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
// 												</Text>
// 											</Flex>
// 										))}
// 									</Box>
// 								)}
// 							</Box>
// 						</Box>

// 						{/* Available Couriers */}
// 						{
// 							!order.isPaid ? (

// 								<Box borderBottom="1px" py="6" borderColor="gray.300">
// 									<Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
// 										Available Couriers
// 									</Heading>
// 									{loadingCouriers ? (
// 										<Loader />
// 									) : errorCouriers ? (
// 										<Message type="error">{errorCouriers}</Message>
// 									) : availableCouriers && availableCouriers.length > 0 ? (
// 										<Box>
// 											<Select
// 												placeholder="Select a courier"
// 												value={selectedCourier?.courier_name || ''}
// 												onChange={handleCourierChange}
// 											>
// 												{availableCouriers.map((courier, index) => (
// 													<option key={index} value={courier.courier_name}>
// 														{courier.courier_name + ' ' + courier.rate}
// 													</option>
// 												))}
// 											</Select>
// 										</Box>
// 									) : (
// 										<Message type="info">No couriers available.</Message>
// 									)}
// 								</Box>
// 							) : (
// 								<Box mt='4'>
// 									<Text fontSize='xl'>
// 										<strong>Estimated Delivery Time: </strong>
// 										{estimatedDelivery}
// 									</Text>
// 								</Box>

// 							)}

// 					</Flex>

// 					{/* Column 2 */}
// 					<Flex
// 						direction='column'
// 						bgColor='white'
// 						justifyContent='space-between'
// 						py='8'
// 						px='8'
// 						shadow='md'
// 						rounded='lg'
// 						borderColor='gray.300'>
// 						<Box>
// 							<Heading mb='6' as='h2' fontSize='3xl' fontWeight='bold'>
// 								Order Summary
// 							</Heading>

// 							{/* Items Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Items</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.itemsPrice}
// 								</Text>
// 							</Flex>

// 							{/* Shipping Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Shipping</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{selectedCourier ? selectedCourier.rate : order.shippingPrice}
// 								</Text>
// 							</Flex>

// 							{/* Tax Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Tax</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.taxPrice}
// 								</Text>
// 							</Flex>

// 							{/* Total Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Total</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{calculateTotalPrice()}
// 								</Text>
// 							</Flex>


// 						</Box>

// 						{/* PAYMENT BUTTON */}
// 						{!order.isPaid && (
// 							<Box>
// 								{loadingPay ? (
// 									<Loader />
// 								) : (
// 									<PayPalScriptProvider
// 										options={{
// 											clientId:
// 												'AUovBO90owBawRxVyoBse0wH_X0K-k_NbScKdEK1ULHjvn3QI2QJgVnoLb11FFYyG69iFdDcNhcGIkjL',
// 											components: 'buttons',
// 										}}>
// 										<PayPalButtons
// 											createOrder={(data, actions) => {
// 												// const totalPrice = calculateTotalPrice().toFixed(2);
// 												return actions.order.create({
// 													purchase_units: [
// 														{
// 															amount: {
// 																value: calculateTotalPrice(),
// 															},
// 														},
// 													],
// 												});
// 											}}
// 											onApprove={(data, actions) => {
// 												return actions.order.capture().then((details) => {
// 													console.log(details);
// 													const paymentResult = {
// 														id: details.id,
// 														status: details.status,
// 														update_time: details.update_time,
// 														email_address: details.email_address,
// 													};
// 													successPaymentHandler(paymentResult);
// 												});
// 											}}
// 										/>
// 									</PayPalScriptProvider>
// 								)}
// 							</Box>
// 						)}

// 						{/* Order Deliver Button */}
// 						{loadingDeliver && <Loader />}
// 						{userInfo && userInfo.isAdmin && order.isPaid && !order.isDelivered && (
// 							<Button type='button' colorScheme='teal' onClick={deliverHandler}>Mark as Delivered</Button>
// 						)}


// 					</Flex>
// 				</Grid>
// 			</Flex>
// 		</>
// 	);
// };

// export default OrderScreen;


// import { Box, Flex, Grid, Heading, Image, Link, Text, Button, Select } from '@chakra-ui/react';
// import { PayPalButtons, PayPalScriptProvider } from '@paypal/react-paypal-js';
// import { useEffect, useState } from 'react';
// import { useDispatch, useSelector } from 'react-redux';
// import { Link as RouterLink, useParams } from 'react-router-dom';
// import { deliverOrder, getOrderDetails, payOrder } from '../actions/orderActions';
// import Loader from '../components/Loader';
// import Message from '../components/Message';
// import { ORDER_DELIVER_RESET, ORDER_PAY_RESET } from '../constants/orderConstants';
// import { fetchAvailableCouriers } from '../actions/courierActions';

// const OrderScreen = () => {
// 	const dispatch = useDispatch();
// 	const { id: orderId } = useParams();
// 	const [selectedCourier, setSelectedCourier] = useState();
// 	const [estimatedDelivery, setEstimatedDelivery] = useState('');

// 	const orderDetails = useSelector((state) => state.orderDetails);
// 	const { order, loading, error } = orderDetails;

// 	const availableCouriersState = useSelector((state) => state.availableCouriers);
// 	const { loading: loadingCouriers, error: errorCouriers, availableCouriers } = availableCouriersState;

// 	const orderPay = useSelector((state) => state.orderPay);
// 	const { loading: loadingPay, success: successPay } = orderPay;

// 	const orderDeliver = useSelector((state) => state.orderDeliver);
// 	const { loading: loadingDeliver, success: successDeliver } = orderDeliver;

// 	const userLogin = useSelector((state) => state.userLogin);
// 	const { userInfo } = userLogin;

// 	const handleCourierChange = (e) => {
// 		const selectedCourier = availableCouriers.find(courier => courier.courier_name === e.target.value);
// 		setSelectedCourier(selectedCourier);
// 		if (selectedCourier) {
// 			setEstimatedDelivery(selectedCourier.etd); // Assuming the ETD is part of the courier object
// 		}
// 	};

// 	const calculateTotalPrice = () => {
// 		return (
// 			order.itemsPrice +
// 			(selectedCourier ? selectedCourier.rate : order.shippingPrice) +
// 			order.taxPrice
// 		);
// 	};

// 	useEffect(() => {
// 		dispatch({ type: ORDER_PAY_RESET });
// 		dispatch({ type: ORDER_DELIVER_RESET });

// 		if (!order || successPay || successDeliver || order._id !== orderId) {
// 			dispatch(getOrderDetails(orderId));
// 		} else if (order.shippingAddress) {
// 			const params = {
// 				pickup_postcode: '400068',
// 				delivery_postcode: order.shippingAddress.postalCode,
// 				weight: 2,
// 				cod: order.paymentMethod === 'COD' ? 1 : 0,
// 			};
// 			dispatch(fetchAvailableCouriers(params));
// 		}
// 	}, [dispatch, orderId, order, successPay, successDeliver, selectedCourier]);

// 	const successPaymentHandler = (paymentResult) => {
// 		dispatch(payOrder(orderId, paymentResult));
// 	};

// 	const deliverHandler = () => dispatch(deliverOrder(order));

// 	console.log("Available Couriers State:", availableCouriersState);
// 	return loading ? (
// 		<Loader />
// 	) : error ? (
// 		<Message type='error'>{error}</Message>
// 	) : (
// 		<>
// 			<Flex w='full' py='5' direction='column'>
// 				<Grid templateColumns='3fr 2fr' gap='20'>
// 					{/* Column 1 */}
// 					<Flex direction='column'>
// 						{/* Shipping */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Shipping
// 							</Heading>
// 							<Text>
// 								Name: <strong>{order.user.name}</strong>
// 							</Text>
// 							<Text>
// 								Email:{' '}
// 								<strong>
// 									<a href={`mailto:${order.user.email}`}>{order.user.email}</a>
// 								</strong>
// 							</Text>
// 							<Text>
// 								<strong>Address: </strong>
// 								{order.shippingAddress.address}, {order.shippingAddress.city},{' '}
// 								{order.shippingAddress.postalCode},{' '}
// 								{order.shippingAddress.country}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isDelivered ? (
// 									<Message type='success'>
// 										Delivered on {order.deliveredAt}
// 									</Message>
// 								) : (
// 									<Message type='warning'>Not Delivered</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Payment Method */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Payment Method
// 							</Heading>
// 							<Text>
// 								<strong>Method: </strong>
// 								{order.paymentMethod?.toUpperCase()}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isPaid ? (
// 									<Message type='success'>Paid on {order.paidAt}</Message>
// 								) : (
// 									<Message type='warning'>Not Paid</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Order Items */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Order Items
// 							</Heading>
// 							<Box>
// 								{order.orderItems.length === 0 ? (
// 									<Message>No Order Info</Message>
// 								) : (
// 									<Box py='2'>
// 										{order.orderItems.map((item, idx) => (
// 											<Flex
// 												key={idx}
// 												alignItems='center'
// 												justifyContent='space-between'>
// 												<Flex py='2' alignItems='center'>
// 													<Image
// 														src={item.image}
// 														alt={item.name}
// 														w='12'
// 														h='12'
// 														objectFit='cover'
// 														mr='6'
// 													/>
// 													<Link
// 														fontWeight='bold'
// 														fontSize='xl'
// 														as={RouterLink}
// 														to={`/products/${item.product}`}>
// 														{item.name}
// 													</Link>
// 												</Flex>

// 												<Text fontSize='lg' fontWeight='semibold'>
// 													{item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
// 												</Text>
// 											</Flex>
// 										))}
// 									</Box>
// 								)}
// 							</Box>
// 						</Box>

// 						{/* Available Couriers */}
// 						{
// 							!order.isPaid ? (

// 								<Box borderBottom="1px" py="6" borderColor="gray.300">
// 									<Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
// 										Available Couriers
// 									</Heading>
// 									{loadingCouriers ? (
// 										<Loader />
// 									) : errorCouriers ? (
// 										<Message type="error">{errorCouriers}</Message>
// 									) : availableCouriers && availableCouriers.length > 0 ? (
// 										<Box>
// 											<Select
// 												placeholder="Select a courier"
// 												value={selectedCourier?.courier_name || ''}
// 												onChange={handleCourierChange}
// 											>
// 												{availableCouriers.map((courier, index) => (
// 													<option key={index} value={courier.courier_name}>
// 														{courier.courier_name + ' ' + courier.rate}
// 													</option>
// 												))}
// 											</Select>
// 										</Box>
// 									) : (
// 										<Message type="info">No couriers available.</Message>
// 									)}
// 								</Box>
// 							) : (
// 								<Box mt='4'>
// 									<Text fontSize='xl'>
// 										<strong>Estimated Delivery Time: </strong>
// 										{estimatedDelivery}
// 									</Text>
// 								</Box>

// 							)}

// 					</Flex>

// 					{/* Column 2 */}
// 					<Flex
// 						direction='column'
// 						bgColor='white'
// 						justifyContent='space-between'
// 						py='8'
// 						px='8'
// 						shadow='md'
// 						rounded='lg'
// 						borderColor='gray.300'>
// 						<Box>
// 							<Heading mb='6' as='h2' fontSize='3xl' fontWeight='bold'>
// 								Order Summary
// 							</Heading>

// 							{/* Items Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Items</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.itemsPrice}
// 								</Text>
// 							</Flex>

// 							{/* Shipping Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Shipping</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{selectedCourier ? selectedCourier.rate : order.shippingPrice}
// 								</Text>
// 							</Flex>

// 							{/* Tax Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Tax</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.taxPrice}
// 								</Text>
// 							</Flex>

// 							{/* Total Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Total</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{calculateTotalPrice()}
// 								</Text>
// 							</Flex>


// 						</Box>

// 						{/* PAYMENT BUTTON */}
// 						{!order.isPaid && (
// 							<Box>
// 								{loadingPay ? (
// 									<Loader />
// 								) : (
// 									<PayPalScriptProvider
// 										options={{
// 											clientId:
// 												'AUovBO90owBawRxVyoBse0wH_X0K-k_NbScKdEK1ULHjvn3QI2QJgVnoLb11FFYyG69iFdDcNhcGIkjL',
// 											components: 'buttons',
// 										}}>
// 										<PayPalButtons
// 											createOrder={(data, actions) => {
// 												// const totalPrice = calculateTotalPrice().toFixed(2);
// 												return actions.order.create({
// 													purchase_units: [
// 														{
// 															amount: {
// 																value: calculateTotalPrice(),
// 															},
// 														},
// 													],
// 												});
// 											}}
// 											onApprove={(data, actions) => {
// 												return actions.order.capture().then((details) => {
// 													console.log(details);
// 													const paymentResult = {
// 														id: details.id,
// 														status: details.status,
// 														update_time: details.update_time,
// 														email_address: details.email_address,
// 													};
// 													successPaymentHandler(paymentResult);
// 												});
// 											}}
// 										/>
// 									</PayPalScriptProvider>
// 								)}
// 							</Box>
// 						)}

// 						{/* Order Deliver Button */}
// 						{loadingDeliver && <Loader />}
// 						{userInfo && userInfo.isAdmin && order.isPaid && !order.isDelivered && (
// 							<Button type='button' colorScheme='teal' onClick={deliverHandler}>Mark as Delivered</Button>
// 						)}


// 					</Flex>
// 				</Grid>
// 			</Flex>
// 		</>
// 	);
// };

// export default OrderScreen;
// -------------------------------------------------------------------------------------------------------------------------
// import { Box, Flex, Grid, Heading, Image, Link, Text, Button, Select } from '@chakra-ui/react';
// import { PayPalButtons, PayPalScriptProvider } from '@paypal/react-paypal-js';
// import { useEffect, useMemo, useState } from 'react';
// import { useDispatch, useSelector } from 'react-redux';
// import { Link as RouterLink, useParams } from 'react-router-dom';
// import { deliverOrder, getOrderDetails, payOrder } from '../actions/orderActions';
// import Loader from '../components/Loader';
// import Message from '../components/Message';
// import { ORDER_DELIVER_RESET, ORDER_PAY_RESET } from '../constants/orderConstants';
// import { fetchAvailableCouriers } from '../actions/courierActions';

// const OrderScreen = () => {
// 	const dispatch = useDispatch();
// 	const { id: orderId } = useParams();
// 	const [selectedCourier, setSelectedCourier] = useState(null);
// 	const [estimatedDelivery, setEstimatedDelivery] = useState('');

// 	const orderDetails = useSelector((state) => state.orderDetails);
// 	const { order, loading, error } = orderDetails;

// 	const availableCouriersState = useSelector((state) => state.availableCouriers);
// 	const { loading: loadingCouriers, error: errorCouriers, availableCouriers } = availableCouriersState;

// 	const orderPay = useSelector((state) => state.orderPay);
// 	const { loading: loadingPay, success: successPay } = orderPay;

// 	const orderDeliver = useSelector((state) => state.orderDeliver);
// 	const { loading: loadingDeliver, success: successDeliver } = orderDeliver;

// 	const userLogin = useSelector((state) => state.userLogin);
// 	const { userInfo } = userLogin;

// 	const handleCourierChange = (e) => {
// 		const selectedCourier = availableCouriers.find(courier => courier.courier_name === e.target.value);
// 		setSelectedCourier(selectedCourier);
// 		if (selectedCourier) {
// 			setEstimatedDelivery(selectedCourier.etd); // Assuming the ETD is part of the courier object
// 			localStorage.setItem('selectedCourier', JSON.stringify(selectedCourier));
// 		}
// 	};

// 	const calculateTotalPrice = useMemo(() => {
// 		return (
// 			order?.itemsPrice +
// 			(selectedCourier ? selectedCourier.rate : order?.shippingPrice) +
// 			order?.taxPrice
// 		);
// 	}, [order, selectedCourier]);

// 	useEffect(() => {
// 		dispatch({ type: ORDER_PAY_RESET });
// 		dispatch({ type: ORDER_DELIVER_RESET });

// 		if (!order || successPay || successDeliver || order._id !== orderId) {
// 			dispatch(getOrderDetails(orderId));
// 		} else if (order.shippingAddress) {
// 			const params = {
// 				pickup_postcode: '400068',
// 				delivery_postcode: order.shippingAddress.postalCode,
// 				weight: 2,
// 				cod: order.paymentMethod === 'COD' ? 1 : 0,
// 			};

// 			const storedCourierInfo = localStorage.getItem('courierInfo');
// 			const storedSelectedCourier = localStorage.getItem('selectedCourier');

// 			if (storedCourierInfo) {
// 				const storedCouriers = JSON.parse(storedCourierInfo).data.available_courier_companies;
// 				dispatch({
// 					type: 'FETCH_AVAILABLE_COURIERS_SUCCESS',
// 					payload: storedCouriers,
// 				});
// 			} else {
// 				dispatch(fetchAvailableCouriers(params));
// 			}

// 			if (storedSelectedCourier) {
// 				const selectedCourier = JSON.parse(storedSelectedCourier);
// 				setSelectedCourier(selectedCourier);
// 				setEstimatedDelivery(selectedCourier.etd);
// 			}
// 		}
// 	}, [dispatch, orderId, order, successPay, successDeliver]);

// 	const successPaymentHandler = (paymentResult) => {
// 		dispatch(payOrder(orderId, paymentResult));
// 	};

// 	const deliverHandler = () => dispatch(deliverOrder(order));

// 	console.log("Available Couriers State:", availableCouriersState);
// 	return loading ? (
// 		<Loader />
// 	) : error ? (
// 		<Message type='error'>{error}</Message>
// 	) : (
// 		<>
// 			<Flex w='full' py='5' direction='column'>
// 				<Grid templateColumns='3fr 2fr' gap='20'>
// 					{/* Column 1 */}
// 					<Flex direction='column'>
// 						{/* Shipping */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Shipping
// 							</Heading>
// 							<Text>
// 								Name: <strong>{order.user.name}</strong>
// 							</Text>
// 							<Text>
// 								Email:{' '}
// 								<strong>
// 									<a href={`mailto:${order.user.email}`}>{order.user.email}</a>
// 								</strong>
// 							</Text>
// 							<Text>
// 								<strong>Address: </strong>
// 								{order.shippingAddress.address}, {order.shippingAddress.city},{' '}
// 								{order.shippingAddress.postalCode},{' '}
// 								{order.shippingAddress.country}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isDelivered ? (
// 									<Message type='success'>
// 										Delivered on {order.deliveredAt}
// 									</Message>
// 								) : (
// 									<Message type='warning'>Not Delivered</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Payment Method */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Payment Method
// 							</Heading>
// 							<Text>
// 								<strong>Method: </strong>
// 								{order.paymentMethod?.toUpperCase()}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isPaid ? (
// 									<Message type='success'>Paid on {order.paidAt}</Message>
// 								) : (
// 									<Message type='warning'>Not Paid</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Order Items */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Order Items
// 							</Heading>
// 							<Box>
// 								{order.orderItems.length === 0 ? (
// 									<Message>No Order Info</Message>
// 								) : (
// 									<Box py='2'>
// 										{order.orderItems.map((item, idx) => (
// 											<Flex
// 												key={idx}
// 												alignItems='center'
// 												justifyContent='space-between'>
// 												<Flex py='2' alignItems='center'>
// 													<Image
// 														src={item.image}
// 														alt={item.name}
// 														w='12'
// 														h='12'
// 														objectFit='cover'
// 														mr='6'
// 													/>
// 													<Link
// 														fontWeight='bold'
// 														fontSize='xl'
// 														as={RouterLink}
// 														to={`/products/${item.product}`}>
// 														{item.name}
// 													</Link>
// 												</Flex>

// 												<Text fontSize='lg' fontWeight='semibold'>
// 													{item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
// 												</Text>
// 											</Flex>
// 										))}
// 									</Box>
// 								)}
// 							</Box>
// 						</Box>

// 						{/* Available Couriers */}
// 						{
// 							!order.isPaid ? (

// 								<Box borderBottom="1px" py="6" borderColor="gray.300">
// 									<Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
// 										Available Couriers
// 									</Heading>
// 									{loadingCouriers ? (
// 										<Loader />
// 									) : errorCouriers ? (
// 										<Message type="error">{errorCouriers}</Message>
// 									) : availableCouriers && availableCouriers.length > 0 ? (
// 										<Box>
// 											<Select
// 												placeholder="Select a courier"
// 												value={selectedCourier?.courier_name || ''}
// 												onChange={handleCourierChange}
// 											>
// 												{availableCouriers.map((courier, index) => (
// 													<option key={index} value={courier.courier_name}>
// 														{courier.courier_name + ' ' + courier.rate}
// 													</option>
// 												))}
// 											</Select>
// 										</Box>
// 									) : (
// 										<Message type="info">No couriers available.</Message>
// 									)}
// 								</Box>
// 							) : (
// 								<Box mt='4'>
// 									<Text fontSize='xl'>
// 										<strong>Estimated Delivery Time: </strong>
// 										{estimatedDelivery}
// 									</Text>
// 								</Box>

// 							)}

// 					</Flex>

// 					{/* Column 2 */}
// 					<Flex
// 						direction='column'
// 						bgColor='white'
// 						justifyContent='space-between'
// 						py='8'
// 						px='8'
// 						shadow='md'
// 						rounded='lg'
// 						borderColor='gray.300'>
// 						<Box>
// 							<Heading mb='6' as='h2' fontSize='3xl' fontWeight='bold'>
// 								Order Summary
// 							</Heading>

// 							{/* Items Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Items</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.itemsPrice}
// 								</Text>
// 							</Flex>

// 							{/* Shipping Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Shipping</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{selectedCourier ? selectedCourier.rate : order.shippingPrice}
// 								</Text>
// 							</Flex>

// 							{/* Tax Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Tax</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.taxPrice}
// 								</Text>
// 							</Flex>

// 							{/* Total Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Total</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{calculateTotalPrice}
// 								</Text>
// 							</Flex>


// 						</Box>

// 						{/* PAYMENT BUTTON */}
// 						{!order.isPaid && (
// 							<Box>
// 								{loadingPay ? (
// 									<Loader />
// 								) : (
// 									<PayPalScriptProvider
// 										options={{
// 											clientId:
// 												'AUovBO90owBawRxVyoBse0wH_X0K-k_NbScKdEK1ULHjvn3QI2QJgVnoLb11FFYyG69iFdDcNhcGIkjL',
// 											components: 'buttons',
// 										}}>
// 										<PayPalButtons
// 											createOrder={(data, actions) => {
// 												const totalPrice = calculateTotalPrice.toFixed(2);
// 												return actions.order.create({
// 													purchase_units: [
// 														{
// 															amount: {
// 																value: totalPrice,
// 															},
// 														},
// 													],
// 												});
// 											}}
// 											onApprove={(data, actions) => {
// 												return actions.order.capture().then((details) => {
// 													console.log(details);
// 													const paymentResult = {
// 														id: details.id,
// 														status: details.status,
// 														update_time: details.update_time,
// 														email_address: details.email_address,
// 													};
// 													successPaymentHandler(paymentResult);
// 												});
// 											}}
// 										/>
// 									</PayPalScriptProvider>
// 								)}
// 							</Box>
// 						)}

// 						{/* Order Deliver Button */}
// 						{loadingDeliver && <Loader />}
// 						{userInfo && userInfo.isAdmin && order.isPaid && !order.isDelivered && (
// 							<Button type='button' colorScheme='teal' onClick={deliverHandler}>Mark as Delivered</Button>
// 						)}


// 					</Flex>
// 				</Grid>
// 			</Flex>
// 		</>
// 	);
// };

// export default OrderScreen;
// --------------------------------------------------------------------------------------------------------------------------
// import {
//     Box,
//     Button,
//     Flex,
//     Grid,
//     Heading,
//     Image,
//     Link,
//     Text,
//     // RadioGroup,
//     // Radio,
//     // Stack,
//   } from '@chakra-ui/react';
//   import { useEffect, useState } from 'react';
//   import { useDispatch, useSelector } from 'react-redux';
//   import { Link as RouterLink, useNavigate } from 'react-router-dom';
//   import { createOrder } from '../actions/orderActions';
//   import CheckoutSteps from '../components/CheckoutSteps';
//   import Message from '../components/Message';
//   import axios from 'axios';
//   import { fetchAvailableCouriers } from '../actions/courierActions';
//   import { GetToken } from '../shiprocket';
//   // import { GetToken, CourierServiceability } from '../shiprocket';
  
//   const PlaceOrderScreen = () => {
//     const dispatch = useDispatch();
//     const navigate = useNavigate();
//     // const [isServiceable, setIsServiceable] = useState(null);
  
//     const cart = useSelector((state) => state.cart);
  
//     cart.itemsPrice = cart.cartItems.reduce(
//       (acc, currVal) => acc + currVal.price * +currVal.qty,
//       0
//     );
//     cart.shippingPrice = cart.cartItems < 10000 ? 10000 : 0;
//     cart.taxPrice = (28 * cart.itemsPrice) / 100;
//     cart.totalPrice = cart.itemsPrice + cart.shippingPrice + cart.taxPrice;
  
//     const orderCreate = useSelector((state) => state.orderCreate);
//     const { order, success } = orderCreate;
  
//     const placeOrderHandler = async () => {
//       try {
//           // setAvailableCouriers(availableCouriers);
  
//           const params = {
//             pickup_postcode: '400068',
//             delivery_postcode: cart.shippingAddress.postalCode,
//             weight:2,
//             cod:1,
//           }
//           dispatch(fetchAvailableCouriers(params));
//           const token = await GetToken({
//             email: 'patkarsaahas@gmail.com',
//             password: 'tempUse@486',
//           });
//           dispatch(
//             createOrder({
//               orderItems: cart.cartItems,
//               shippingAddress: cart.shippingAddress,
//               paymentMethod: cart.paymentMethod,
//               itemsPrice: cart.itemsPrice,
//               shippingPrice: cart.shippingPrice,
//               taxPrice: cart.taxPrice,
//               totalPrice: cart.totalPrice,
//             })
//           );
  
//           const shiprocketOrder = {
//             order_id: Math.random() * 1000,
//             order_date: new Date().toISOString(),
//             pickup_location: 'Borivali',
//             billing_customer_name: cart.shippingAddress.name || 'Test Name',
//             billing_last_name: '',
//             billing_address: cart.shippingAddress.address,
//             billing_city: cart.shippingAddress.city,
//             billing_pincode: cart.shippingAddress.postalCode,
//             billing_state: cart.shippingAddress.state || 'Test State',
//             billing_country: cart.shippingAddress.country,
//             billing_email: 'patkarsaahas@gmail.com',
//             billing_phone: '1234567890',
//             shipping_is_billing: true,
//             order_items: cart.cartItems.map((item) => ({
//               name: item.name,
//               sku: item.product.toString(),
//               units: item.qty,
//               selling_price: item.price,
//             })),
//             payment_method: cart.paymentMethod === 'paypal' ? 'Prepaid' : 'COD',
//             sub_total: cart.itemsPrice,
//             length: 10,
//             breadth: 10,
//             height: 10,
//             weight: 0.5,
//           };
//           console.log(shiprocketOrder);
  
//           // Create order in Shiprocket
//           try {
//             const shiprocketResponse = await axios.post(
//               'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc',
//               shiprocketOrder,
//               {
//                 headers: {
//                   'Content-Type': 'application/json',
//                   Authorization: `Bearer ${token}`,
//                 },
//               }
//             );
  
//             console.log('Shiprocket Order Created:', shiprocketResponse.data);
//           } catch (shiprocketError) {
//             console.error('Failed to create order in Shiprocket:', shiprocketError.response.data);
//           }
//         } catch(err){
//           console.error('Error placing order:', err);
//         }
//     };
  
//     useEffect(() => {
//       if (success) {
//         if (cart.paymentMethod === 'paypal') {
//           navigate(`/order/${order._id}`);
//         } else {
//           navigate(`/razorpay/order/${order._id}`);
//         }
//       }
//     }, [success, navigate, order, cart]);
  
//     return (
//       <Flex w="full" direction="column" py="5">
//         <CheckoutSteps step1 step2 step3 step4 />
  
//         <Grid templateColumns="3fr 2fr" gap="20">
//           {/* Column 1 */}
//           <Flex direction="column">
//             {/* Shipping */}
//             <Box borderBottom="1px" py="6" borderColor="gray.300">
//               <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
//                 Shipping
//               </Heading>
//               <Text>
//                 <strong>Address: </strong>
//                 {cart.shippingAddress.address}, {cart.shippingAddress.city},{' '}
//                 {cart.shippingAddress.postalCode}, {cart.shippingAddress.country}
//               </Text>
//             </Box>
  
//             {/* Payment Method */}
//             <Box borderBottom="1px" py="6" borderColor="gray.300">
//               <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
//                 Payment Method
//               </Heading>
//               <Text>
//                 <strong>Method: </strong>
//                 {cart.paymentMethod.toUpperCase()}
//               </Text>
//             </Box>
  
//             {/* Order Items */}
//             <Box borderBottom="1px" py="6" borderColor="gray.300">
//               <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
//                 Order Items
//               </Heading>
//               <Box>
//                 {cart.cartItems.length === 0 ? (
//                   <Message>Your cart is empty</Message>
//                 ) : (
//                   <Box py="2">
//                     {cart.cartItems.map((item, idx) => (
//                       <Flex
//                         key={idx}
//                         alignItems="center"
//                         justifyContent="space-between"
//                       >
//                         <Flex py="2" alignItems="center">
//                           <Image
//                             src={item.image}
//                             alt={item.name}
//                             w="12"
//                             h="12"
//                             objectFit="cover"
//                             mr="6"
//                           />
//                           <Link
//                             fontWeight="bold"
//                             fontSize="xl"
//                             as={RouterLink}
//                             to={`/products/${item.product}`}
//                           >
//                             {item.name}
//                           </Link>
//                         </Flex>
  
//                         <Text fontSize="lg" fontWeight="semibold">
//                           {item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
//                         </Text>
//                       </Flex>
//                     ))}
//                   </Box>
//                 )}
//               </Box>
//             </Box>
  
//             {/* Courier Selection */}
            
  
//           </Flex>
  
//           {/* Column 2 */}
//           <Flex
//             direction="column"
//             bgColor="white"
//             justifyContent="space-between"
//             py="8"
//             px="8"
//             shadow="md"
//             rounded="lg"
//             borderColor="gray.300"
//           >
//             <Box>
//               <Heading mb="6" as="h2" fontSize="3xl" fontWeight="bold">
//                 Order Summary
//               </Heading>
  
//               {/* Items Price */}
//               <Flex
//                 borderBottom="1px"
//                 py="2"
//                 borderColor="gray.200"
//                 alignItems="center"
//                 justifyContent="space-between"
//               >
//                 <Text fontSize="xl">Items</Text>
//                 <Text fontWeight="bold" fontSize="xl">
//                   ₹{cart.itemsPrice}
//                 </Text>
//               </Flex>
  
//               {/* Shipping Price */}
//               <Flex
//                 borderBottom="1px"
//                 py="2"
//                 borderColor="gray.200"
//                 alignItems="center"
//                 justifyContent="space-between"
//               >
//                 <Text fontSize="xl">Shipping</Text>
//                 <Text fontWeight="bold" fontSize="xl">
//                   ₹{cart.shippingPrice}
//                 </Text>
//               </Flex>
  
//               {/* Tax Price */}
//               <Flex
//                 borderBottom="1px"
//                 py="2"
//                 borderColor="gray.200"
//                 alignItems="center"
//                 justifyContent="space-between"
//               >
//                 <Text fontSize="xl">Tax</Text>
//                 <Text fontWeight="bold" fontSize="xl">
//                   ₹{cart.taxPrice}
//                 </Text>
//               </Flex>
  
//               {/* Total Price */}
//               <Flex
//                 borderBottom="1px"
//                 py="2"
//                 borderColor="gray.200"
//                 alignItems="center"
//                 justifyContent="space-between"
//               >
//                 <Text fontSize="xl">Total</Text>
//                 <Text fontWeight="bold" fontSize="xl">
//                   ₹{cart.totalPrice}
//                 </Text>
//               </Flex>
//             </Box>
//             <Button
//               size="lg"
//               textTransform="uppercase"
//               colorScheme="yellow"
//               type="button"
//               w="full"
//               onClick={placeOrderHandler}
//               disabled={cart.cartItems.length === 0}
//             >
//               Place Order
//             </Button>
//             {/* {isServiceable === false && (
//               <Message type="error">Address not serviceable</Message>
//             )} */}
//           </Flex>
//         </Grid>
//       </Flex>
//     );
//   };
  
//   export default PlaceOrderScreesn;


// export const createOrder = (order) => async (dispatch, getState) => {
// 	try {
// 		dispatch({ type: ORDER_CREATE_REQUEST });

// 		const {
// 			userLogin: { userInfo },
// 		} = getState();

// 		const config = {
// 			headers: {
// 				Authorization: `Bearer ${userInfo.token}`,
// 				'Content-Type': 'application/json',
// 			},
// 		};

// 		const { data } = await axios.post(`/api/orders`, order, config);
// 		console.log('Order created successfully:', data);
		

// 		dispatch({ type: ORDER_CREATE_SUCCESS, payload: data });
// 	} catch (err) {
// 		console.error('Error creating order:', err);
// 		dispatch({
// 			type: ORDER_CREATE_FAIL,
// 			payload:
// 				err.response && err.response.data.message
// 					? err.response.data.message
// 					: err.message,
// 		});
// 	}
// };




// import {
//     Box,
//     Button,
//     Flex,
//     Grid,
//     Heading,
//     Image,
//     Link,
//     Text,
//     // RadioGroup,
//     // Radio,
//     // Stack,
//   } from '@chakra-ui/react';
//   import { useEffect } from 'react';
//   import { useDispatch, useSelector } from 'react-redux';
//   import { Link as RouterLink, useNavigate } from 'react-router-dom';
//   import { createOrder, updateOrderWithShiprocketId } from '../actions/orderActions';
//   import CheckoutSteps from '../components/CheckoutSteps';
//   import Message from '../components/Message';
//   import axios from 'axios';
//   import { fetchAvailableCouriers } from '../actions/courierActions';
//   import { GetToken } from '../shiprocket';
//   // import { GetToken, CourierServiceability } from '../shiprocket';
  
//   const PlaceOrderScreen = () => {
//     const dispatch = useDispatch();
//     const navigate = useNavigate();
//     // const [isServiceable, setIsServiceable] = useState(null);
  
//     const cart = useSelector((state) => state.cart);
  
//     cart.itemsPrice = cart.cartItems.reduce(
//       (acc, currVal) => acc + currVal.price * +currVal.qty,
//       0
//     );
//     cart.shippingPrice = cart.cartItems < 10000 ? 10000 : 0;
//     cart.taxPrice = (28 * cart.itemsPrice) / 100;
//     cart.totalPrice = cart.itemsPrice + cart.shippingPrice + cart.taxPrice;
  
//     const orderCreate = useSelector((state) => state.orderCreate);
//     const { order, success } = orderCreate;
  
//     const placeOrderHandler = async () => {
//       try {
//           // setAvailableCouriers(availableCouriers);
  
//           const params = {
//             pickup_postcode: '400068',
//             delivery_postcode: cart.shippingAddress.postalCode,
//             weight:2,
//             cod:1,
//           }
//           dispatch(fetchAvailableCouriers(params));
//           const token = await GetToken({
//             email: 'patkarsaahas@gmail.com',
//             password: 'tempUse@486',
//           });
//           const createdOrder = await dispatch(
//             createOrder({
//               orderItems: cart.cartItems,
//               shippingAddress: cart.shippingAddress,
//               paymentMethod: cart.paymentMethod,
//               itemsPrice: cart.itemsPrice,
//               shippingPrice: cart.shippingPrice,
//               taxPrice: cart.taxPrice,
//               totalPrice: cart.totalPrice,
//             })
//           );
      
//           const orderId = createdOrder._id;
          
//           const shiprocketOrder = {
//             order_id: orderId,
//             order_date: new Date().toISOString(),
//             pickup_location: 'Borivali',
//             billing_customer_name: cart.shippingAddress.name || 'Test Name',
//             billing_last_name: '',
//             billing_address: cart.shippingAddress.address,
//             billing_city: cart.shippingAddress.city,
//             billing_pincode: cart.shippingAddress.postalCode,
//             billing_state: cart.shippingAddress.state || 'Test State',
//             billing_country: cart.shippingAddress.country,
//             billing_email: 'patkarsaahas@gmail.com',
//             billing_phone: '1234567890',
//             shipping_is_billing: true,
//             order_items: cart.cartItems.map((item) => ({
//               name: item.name,
//               sku: item.product.toString(),
//               units: item.qty,
//               selling_price: item.price,
//             })),
//             payment_method: cart.paymentMethod === 'paypal' ? 'Prepaid' : 'COD',
//             sub_total: cart.itemsPrice,
//             length: 10,
//             breadth: 10,
//             height: 10,
//             weight: 0.5,
//           };
//           console.log(shiprocketOrder);
  
//           // Create order in Shiprocket
//           try {
//             const shiprocketResponse = await axios.post(
//               'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc',
//               shiprocketOrder,
//               {
//                 headers: {
//                   'Content-Type': 'application/json',
//                   Authorization: `Bearer ${token}`,
//                 },
//               }
//             );
//             const shiprocketOrderId = shiprocketResponse.data.order_id;
//             const shiprocketShipmentId = shiprocketResponse.data.shipment_id;
//             console.log('Shiprocket Order Created:', shiprocketResponse.data);
  
//             await dispatch(updateOrderWithShiprocketId(orderId, shiprocketOrderId, shiprocketShipmentId))
//           } catch (shiprocketError) {
//             console.error('Failed to create order in Shiprocket:', shiprocketError.response.data);
//           }
//         } catch(err){
//           console.error('Error placing order:', err);
//         }
//     };
  
//     useEffect(() => {
//       if (success) {
//         if (cart.paymentMethod === 'paypal') {
//           navigate(`/order/${order._id}`);
//         } else {
//           navigate(`/razorpay/order/${order._id}`);
//         }
//       }
//     }, [success, navigate, order, cart]);
  
//     return (
//       <Flex w="full" direction="column" py="5">
//         <CheckoutSteps step1 step2 step3 step4 />
  
//         <Grid templateColumns="3fr 2fr" gap="20">
//           {/* Column 1 */}
//           <Flex direction="column">
//             {/* Shipping */}
//             <Box borderBottom="1px" py="6" borderColor="gray.300">
//               <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
//                 Shipping
//               </Heading>
//               <Text>
//                 <strong>Address: </strong>
//                 {cart.shippingAddress.address}, {cart.shippingAddress.city},{' '}
//                 {cart.shippingAddress.postalCode}, {cart.shippingAddress.country}
//               </Text>
//             </Box>
  
//             {/* Payment Method */}
//             <Box borderBottom="1px" py="6" borderColor="gray.300">
//               <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
//                 Payment Method
//               </Heading>
//               <Text>
//                 <strong>Method: </strong>
//                 {cart.paymentMethod.toUpperCase()}
//               </Text>
//             </Box>
  
//             {/* Order Items */}
//             <Box borderBottom="1px" py="6" borderColor="gray.300">
//               <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
//                 Order Items
//               </Heading>
//               <Box>
//                 {cart.cartItems.length === 0 ? (
//                   <Message>Your cart is empty</Message>
//                 ) : (
//                   <Box py="2">
//                     {cart.cartItems.map((item, idx) => (
//                       <Flex
//                         key={idx}
//                         alignItems="center"
//                         justifyContent="space-between"
//                       >
//                         <Flex py="2" alignItems="center">
//                           <Image
//                             src={item.image}
//                             alt={item.name}
//                             w="12"
//                             h="12"
//                             objectFit="cover"
//                             mr="6"
//                           />
//                           <Link
//                             fontWeight="bold"
//                             fontSize="xl"
//                             as={RouterLink}
//                             to={`/products/${item.product}`}
//                           >
//                             {item.name}
//                           </Link>
//                         </Flex>
  
//                         <Text fontSize="lg" fontWeight="semibold">
//                           {item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
//                         </Text>
//                       </Flex>
//                     ))}
//                   </Box>
//                 )}
//               </Box>
//             </Box>
  
//             {/* Courier Selection */}
            
  
//           </Flex>
  
//           {/* Column 2 */}
//           <Flex
//             direction="column"
//             bgColor="white"
//             justifyContent="space-between"
//             py="8"
//             px="8"
//             shadow="md"
//             rounded="lg"
//             borderColor="gray.300"
//           >
//             <Box>
//               <Heading mb="6" as="h2" fontSize="3xl" fontWeight="bold">
//                 Order Summary
//               </Heading>
  
//               {/* Items Price */}
//               <Flex
//                 borderBottom="1px"
//                 py="2"
//                 borderColor="gray.200"
//                 alignItems="center"
//                 justifyContent="space-between"
//               >
//                 <Text fontSize="xl">Items</Text>
//                 <Text fontWeight="bold" fontSize="xl">
//                   ₹{cart.itemsPrice}
//                 </Text>
//               </Flex>
  
//               {/* Shipping Price */}
//               <Flex
//                 borderBottom="1px"
//                 py="2"
//                 borderColor="gray.200"
//                 alignItems="center"
//                 justifyContent="space-between"
//               >
//                 <Text fontSize="xl">Shipping</Text>
//                 <Text fontWeight="bold" fontSize="xl">
//                   ₹{cart.shippingPrice}
//                 </Text>
//               </Flex>
  
//               {/* Tax Price */}
//               <Flex
//                 borderBottom="1px"
//                 py="2"
//                 borderColor="gray.200"
//                 alignItems="center"
//                 justifyContent="space-between"
//               >
//                 <Text fontSize="xl">Tax</Text>
//                 <Text fontWeight="bold" fontSize="xl">
//                   ₹{cart.taxPrice}
//                 </Text>
//               </Flex>
  
//               {/* Total Price */}
//               <Flex
//                 borderBottom="1px"
//                 py="2"
//                 borderColor="gray.200"
//                 alignItems="center"
//                 justifyContent="space-between"
//               >
//                 <Text fontSize="xl">Total</Text>
//                 <Text fontWeight="bold" fontSize="xl">
//                   ₹{cart.totalPrice}
//                 </Text>
//               </Flex>
//             </Box>
//             <Button
//               size="lg"
//               textTransform="uppercase"
//               colorScheme="yellow"
//               type="button"
//               w="full"
//               onClick={placeOrderHandler}
//               disabled={cart.cartItems.length === 0}
//             >
//               Place Order
//             </Button>
//             {/* {isServiceable === false && (
//               <Message type="error">Address not serviceable</Message>
//             )} */}
//           </Flex>
//         </Grid>
//       </Flex>
//     );
//   };
  
//   export default PlaceOrderScreen;


// import { Box, Flex, Grid, Heading, Image, Link, Text, Button, Select } from '@chakra-ui/react';
// import { PayPalButtons, PayPalScriptProvider } from '@paypal/react-paypal-js';
// import { useEffect, useMemo, useState } from 'react';
// import { useDispatch, useSelector } from 'react-redux';
// import { Link as RouterLink, useParams } from 'react-router-dom';
// import { deliverOrder, getOrderDetails, payOrder } from '../actions/orderActions';
// import Loader from '../components/Loader';
// import Message from '../components/Message';
// import { ORDER_DELIVER_RESET, ORDER_PAY_RESET } from '../constants/orderConstants';
// import { fetchAvailableCouriers } from '../actions/courierActions';
// import { GetToken } from '../shiprocket';
// import axios from 'axios';

// const OrderScreen = () => {
// 	const dispatch = useDispatch();
// 	const { id: orderId } = useParams();
// 	const [selectedCourier, setSelectedCourier] = useState(null);
// 	const [estimatedDelivery, setEstimatedDelivery] = useState('');

// 	const orderDetails = useSelector((state) => state.orderDetails);
// 	const { order, loading, error } = orderDetails;

// 	const availableCouriersState = useSelector((state) => state.availableCouriers);
// 	const { loading: loadingCouriers, error: errorCouriers, availableCouriers } = availableCouriersState;

// 	const orderPay = useSelector((state) => state.orderPay);
// 	const { loading: loadingPay, success: successPay } = orderPay;

// 	const orderDeliver = useSelector((state) => state.orderDeliver);
// 	const { loading: loadingDeliver, success: successDeliver } = orderDeliver;

// 	const userLogin = useSelector((state) => state.userLogin);
// 	const { userInfo } = userLogin;

// 	const handleCourierChange = (e) => {
// 		const selectedCourier = availableCouriers.find(courier => courier.courier_name === e.target.value);
// 		setSelectedCourier(selectedCourier);
// 		if (selectedCourier) {
// 			setEstimatedDelivery(selectedCourier.etd); // Assuming the ETD is part of the courier object
// 			localStorage.setItem('selectedCourier', JSON.stringify(selectedCourier));
// 		}
// 	};

// 	const calculateTotalPrice = useMemo(() => {
// 		return (
// 			order?.itemsPrice +
// 			(selectedCourier ? selectedCourier.rate : order?.shippingPrice) +
// 			order?.taxPrice
// 		);
// 	}, [order, selectedCourier]);

// 	useEffect(() => {
// 		dispatch({ type: ORDER_PAY_RESET });
// 		dispatch({ type: ORDER_DELIVER_RESET });

// 		if (!order || successPay || successDeliver || order._id !== orderId) {
// 			dispatch(getOrderDetails(orderId));
// 		} else if (order.shippingAddress) {
// 			const params = {
// 				pickup_postcode: '400068',
// 				delivery_postcode: order.shippingAddress.postalCode,
// 				weight: 2,
// 				cod: order.paymentMethod === 'COD' ? 1 : 0,
// 			};

// 			const storedCourierInfo = localStorage.getItem('courierInfo');
// 			const storedSelectedCourier = localStorage.getItem('selectedCourier');

// 			if (storedCourierInfo) {
// 				const storedCouriers = JSON.parse(storedCourierInfo).data.available_courier_companies;
// 				dispatch({
// 					type: 'FETCH_AVAILABLE_COURIERS_SUCCESS',
// 					payload: storedCouriers,
// 				});
// 			} else {
// 				dispatch(fetchAvailableCouriers(params));
// 			}

// 			if (storedSelectedCourier) {
// 				const selectedCourier = JSON.parse(storedSelectedCourier);
// 				setSelectedCourier(selectedCourier);
// 				setEstimatedDelivery(selectedCourier.etd);
// 			}
// 		}
// 		else {
// 			console.log('Order Details', order);
// 		}
// 	}, [dispatch, orderId, order, successPay, successDeliver]);

// 	console.log('Order Details', order);

// 	const successPaymentHandler = async (paymentResult) => {
// 		dispatch(payOrder(orderId, paymentResult));

// 		try {
// 			const token = await GetToken({
// 				email: 'patkarsaahas@gmail.com',
// 				password: 'tempUse@486',
// 			});

// 			const confirmShipmentPayload = {
// 				shipment_id: [order.shiprocketShipmentId],
// 			};
// 			console.log(confirmShipmentPayload.shipment_id);

// 			const confirmShipmentResponse = await axios.post(
// 				'https://apiv2.shiprocket.in/v1/external/courier/generate/pickup',
// 				confirmShipmentPayload,
// 				{
// 					headers: {
// 						'Content-Type': 'application/json',
// 						Authorization: `Bearer ${token}`,
// 					},
// 				}
// 			);

// 			console.log('Shipment Confirmed:', confirmShipmentResponse.data);
// 		} catch (error) {
// 			console.error('Error confirming shipment:', error.response ? error.response.data : error.message);
// 		}
// 	};

// 	const deliverHandler = () => dispatch(deliverOrder(order));

// 	console.log("Available Couriers State:", availableCouriersState);
// 	return loading ? (
// 		<Loader />
// 	) : error ? (
// 		<Message type='error'>{error}</Message>
// 	) : (
// 		<>
// 			<h1>Order {order._id}</h1>
// 			{order.shiprocketShipmentId && (
// 				<div>Shipment ID: {order.shiprocketShipmentId}</div>
// 			)}
// 			<Flex w='full' py='5' direction='column'>
// 				<Grid templateColumns='3fr 2fr' gap='20'>
// 					{/* Column 1 */}
// 					<Flex direction='column'>
// 						{/* Shipping */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Shipping
// 							</Heading>
// 							<Text>
// 								Name: <strong>{order.user.name}</strong>
// 							</Text>
// 							<Text>
// 								Email:{' '}
// 								<strong>
// 									<a href={`mailto:${order.user.email}`}>{order.user.email}</a>
// 								</strong>
// 							</Text>
// 							<Text>
// 								<strong>Address: </strong>
// 								{order.shippingAddress.address}, {order.shippingAddress.city},{' '}
// 								{order.shippingAddress.postalCode},{' '}
// 								{order.shippingAddress.country}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isDelivered ? (
// 									<Message type='success'>
// 										Delivered on {order.deliveredAt}
// 									</Message>
// 								) : (
// 									<Message type='warning'>Not Delivered</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Payment Method */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Payment Method
// 							</Heading>
// 							<Text>
// 								<strong>Method: </strong>
// 								{order.paymentMethod?.toUpperCase()}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isPaid ? (
// 									<Message type='success'>Paid on {order.paidAt}</Message>
// 								) : (
// 									<Message type='warning'>Not Paid</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Order Items */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Order Items
// 							</Heading>
// 							<Box>
// 								{order.orderItems.length === 0 ? (
// 									<Message>No Order Info</Message>
// 								) : (
// 									<Box py='2'>
// 										{order.orderItems.map((item, idx) => (
// 											<Flex
// 												key={idx}
// 												alignItems='center'
// 												justifyContent='space-between'>
// 												<Flex py='2' alignItems='center'>
// 													<Image
// 														src={item.image}
// 														alt={item.name}
// 														w='12'
// 														h='12'
// 														objectFit='cover'
// 														mr='6'
// 													/>
// 													<Link
// 														fontWeight='bold'
// 														fontSize='xl'
// 														as={RouterLink}
// 														to={`/products/${item.product}`}>
// 														{item.name}
// 													</Link>
// 												</Flex>

// 												<Text fontSize='lg' fontWeight='semibold'>
// 													{item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
// 												</Text>
// 											</Flex>
// 										))}
// 									</Box>
// 								)}
// 							</Box>
// 						</Box>

// 						{/* Available Couriers */}
// 						{
// 							!order.isPaid ? (

// 								<Box borderBottom="1px" py="6" borderColor="gray.300">
// 									<Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
// 										Available Couriers
// 									</Heading>
// 									{loadingCouriers ? (
// 										<Loader />
// 									) : errorCouriers ? (
// 										<Message type="error">{errorCouriers}</Message>
// 									) : availableCouriers && availableCouriers.length > 0 ? (
// 										<Box>
// 											<Select
// 												placeholder="Select a courier"
// 												value={selectedCourier?.courier_name || ''}
// 												onChange={handleCourierChange}
// 											>
// 												{availableCouriers.map((courier, index) => (
// 													<option key={index} value={courier.courier_name}>
// 														{courier.courier_name + ' ' + courier.rate}
// 													</option>
// 												))}
// 											</Select>
// 										</Box>
// 									) : (
// 										<Message type="info">No couriers available.</Message>
// 									)}
// 								</Box>
// 							) : (
// 								<Box mt='4'>
// 									<Text fontSize='xl'>
// 										<strong>Estimated Delivery Time: </strong>
// 										{estimatedDelivery}
// 									</Text>
// 								</Box>

// 							)}

// 					</Flex>

// 					{/* Column 2 */}
// 					<Flex
// 						direction='column'
// 						bgColor='white'
// 						justifyContent='space-between'
// 						py='8'
// 						px='8'
// 						shadow='md'
// 						rounded='lg'
// 						borderColor='gray.300'>
// 						<Box>
// 							<Heading mb='6' as='h2' fontSize='3xl' fontWeight='bold'>
// 								Order Summary
// 							</Heading>

// 							{/* Items Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Items</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.itemsPrice}
// 								</Text>
// 							</Flex>

// 							{/* Shipping Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Shipping</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{selectedCourier ? selectedCourier.rate : order.shippingPrice}
// 								</Text>
// 							</Flex>

// 							{/* Tax Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Tax</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.taxPrice}
// 								</Text>
// 							</Flex>

// 							{/* Total Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Total</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{calculateTotalPrice}
// 								</Text>
// 							</Flex>


// 						</Box>

// 						{/* PAYMENT BUTTON */}
// 						{!order.isPaid && (
// 							<Box>
// 								{loadingPay ? (
// 									<Loader />
// 								) : (
// 									<PayPalScriptProvider
// 										options={{
// 											clientId:
// 												'AUovBO90owBawRxVyoBse0wH_X0K-k_NbScKdEK1ULHjvn3QI2QJgVnoLb11FFYyG69iFdDcNhcGIkjL',
// 											components: 'buttons',
// 										}}>
// 										<PayPalButtons
// 											createOrder={(data, actions) => {
// 												const totalPrice = calculateTotalPrice.toFixed(2);
// 												return actions.order.create({
// 													purchase_units: [
// 														{
// 															amount: {
// 																value: totalPrice,
// 															},
// 														},
// 													],
// 												});
// 											}}
// 											onApprove={(data, actions) => {
// 												return actions.order.capture().then((details) => {
// 													console.log(details);
// 													const paymentResult = {
// 														id: details.id,
// 														status: details.status,
// 														update_time: details.update_time,
// 														email_address: details.email_address,
// 													};
// 													successPaymentHandler(paymentResult);
// 												});
// 											}}
// 										/>
// 									</PayPalScriptProvider>
// 								)}
// 							</Box>
// 						)}

// 						{/* Order Deliver Button */}
// 						{loadingDeliver && <Loader />}
// 						{userInfo && userInfo.isAdmin && order.isPaid && !order.isDelivered && (
// 							<Button type='button' colorScheme='teal' onClick={deliverHandler}>Mark as Delivered</Button>
// 						)}


// 					</Flex>
// 				</Grid>
// 			</Flex>
// 		</>
// 	);
// };

// export default OrderScreen;


// const successPaymentHandler = async (paymentResult) => {
//     dispatch(payOrder(orderId, paymentResult));

//     try {
//         const token = await GetToken({
//             email: 'patkarsaahas@gmail.com',
//             password: 'tempUse@486',
//         });

//         // Assuming you have already obtained `courier_id` and `shipment_id` in `order.shiprocketShipmentId`

//         // 1. Assign AWB (Airway Bill)
//         const assignAWBPayload = {
//             courier_id: order.courier_id,
//             shipment_id: order.shiprocketShipmentId,
//         };

//         const assignAWBResponse = await axios.post(
//             'https://apiv2.shiprocket.in/v1/external/courier/assign/awb',
//             assignAWBPayload,
//             {
//                 headers: {
//                     'Content-Type': 'application/json',
//                     Authorization: `Bearer ${token}`,
//                 },
//             }
//         );

//         console.log('AWB Assigned:', assignAWBResponse.data);

//         // 2. Confirm Shipment
//         const confirmShipmentPayload = {
//             shipment_id: [order.shiprocketShipmentId],
//         };

//         const confirmShipmentResponse = await axios.post(
//             'https://apiv2.shiprocket.in/v1/external/courier/generate/pickup',
//             confirmShipmentPayload,
//             {
//                 headers: {
//                     'Content-Type': 'application/json',
//                     Authorization: `Bearer ${token}`,
//                 },
//             }
//         );

//         console.log('Shipment Confirmed:', confirmShipmentResponse.data);

//     } catch (error) {
//         console.error('Error handling payment or confirming shipment:', error.response ? error.response.data : error.message);
//     }
// };



// import { Box, Flex, Grid, Heading, Image, Link, Text, Button, Select } from '@chakra-ui/react';
// import { PayPalButtons, PayPalScriptProvider } from '@paypal/react-paypal-js';
// import { useEffect, useMemo, useState } from 'react';
// import { useDispatch, useSelector } from 'react-redux';
// import { Link as RouterLink, useParams } from 'react-router-dom';
// import { deliverOrder, getOrderDetails, payOrder } from '../actions/orderActions';
// import Loader from '../components/Loader';
// import Message from '../components/Message';
// import { ORDER_DELIVER_RESET, ORDER_PAY_RESET } from '../constants/orderConstants';
// import { fetchAvailableCouriers } from '../actions/courierActions';
// import { GetToken } from '../shiprocket';
// import axios from 'axios';

// const OrderScreen = () => {
// 	const dispatch = useDispatch();
// 	const { id: orderId } = useParams();
// 	const [selectedCourier, setSelectedCourier] = useState(null);
// 	const [estimatedDelivery, setEstimatedDelivery] = useState('');

// 	const orderDetails = useSelector((state) => state.orderDetails);
// 	const { order, loading, error } = orderDetails;

// 	const availableCouriersState = useSelector((state) => state.availableCouriers);
// 	const { loading: loadingCouriers, error: errorCouriers, availableCouriers } = availableCouriersState;

// 	const orderPay = useSelector((state) => state.orderPay);
// 	const { loading: loadingPay, success: successPay } = orderPay;

// 	const orderDeliver = useSelector((state) => state.orderDeliver);
// 	const { loading: loadingDeliver, success: successDeliver } = orderDeliver;

// 	const userLogin = useSelector((state) => state.userLogin);
// 	const { userInfo } = userLogin;

// 	const handleCourierChange = (e) => {
// 		const selectedCourier = availableCouriers.find(courier => courier.courier_name === e.target.value);
// 		setSelectedCourier(selectedCourier);
// 		if (selectedCourier) {
// 			setEstimatedDelivery(selectedCourier.etd); // Assuming the ETD is part of the courier object
// 			localStorage.setItem('selectedCourier', JSON.stringify(selectedCourier));
// 		}
// 	};

// 	const calculateTotalPrice = useMemo(() => {
// 		return (
// 			order?.itemsPrice +
// 			(selectedCourier ? selectedCourier.rate : order?.shippingPrice) +
// 			order?.taxPrice
// 		);
// 	}, [order, selectedCourier]);

// 	useEffect(() => {
// 		dispatch({ type: ORDER_PAY_RESET });
// 		dispatch({ type: ORDER_DELIVER_RESET });

// 		if (!order || successPay || successDeliver || order._id !== orderId) {
// 			dispatch(getOrderDetails(orderId));
// 		} else if (order.shippingAddress) {
// 			const params = {
// 				pickup_postcode: '400068',
// 				delivery_postcode: order.shippingAddress.postalCode,
// 				weight: 2,
// 				cod: order.paymentMethod === 'COD' ? 1 : 0,
// 			};

// 			const storedCourierInfo = localStorage.getItem('courierInfo');
// 			const storedSelectedCourier = localStorage.getItem('selectedCourier');

// 			if (storedCourierInfo) {
// 				const storedCouriers = JSON.parse(storedCourierInfo).data.available_courier_companies;
// 				dispatch({
// 					type: 'FETCH_AVAILABLE_COURIERS_SUCCESS',
// 					payload: storedCouriers,
// 				});
// 			} else {
// 				dispatch(fetchAvailableCouriers(params));
// 			}

// 			if (storedSelectedCourier) {
// 				const selectedCourier = JSON.parse(storedSelectedCourier);
// 				setSelectedCourier(selectedCourier);
// 				setEstimatedDelivery(selectedCourier.etd);
// 			}
// 		}
// 		else {
// 			console.log('Order Details', order);
// 		}
// 	}, [dispatch, orderId, order, successPay, successDeliver]);

// 	console.log('Order Details', order);

// 	const successPaymentHandler = async (paymentResult) => {
// 		dispatch(payOrder(orderId, paymentResult));

// 		try {
// 			const token = await GetToken({
// 				email: 'patkarsaahas@gmail.com',
// 				password: 'tempUse@486',
// 			});

// 			const assignAWBPayload = {
// 				courier_id: order.courier_id,
// 				shipment_id: order.shiprocketShipmentId,
// 			};

// 			const assignAWBResponse = await axios.post(
// 				'https://apiv2.shiprocket.in/v1/external/courier/assign/awb',
// 				assignAWBPayload,
// 				{
// 					headers: {
// 						'Content-Type': 'application/json',
// 						Authorization: `Bearer ${token}`,
// 					},
// 				}
// 			);

// 			console.log('AWB Assigned:', assignAWBResponse.data);

// 			const confirmShipmentPayload = {
// 				shipment_id: [order.shiprocketShipmentId],
// 			};
// 			console.log(confirmShipmentPayload.shipment_id);

// 			const confirmShipmentResponse = await axios.post(
// 				'https://apiv2.shiprocket.in/v1/external/courier/generate/pickup',
// 				confirmShipmentPayload,
// 				{
// 					headers: {
// 						'Content-Type': 'application/json',
// 						Authorization: `Bearer ${token}`,
// 					},
// 				}
// 			);

// 			console.log('Shipment Confirmed:', confirmShipmentResponse.data);
// 		} catch (error) {
// 			console.error('Error confirming shipment:', error.response ? error.response.data : error.message);
// 		}
// 	};

// 	const deliverHandler = () => dispatch(deliverOrder(order));

// 	console.log("Available Couriers State:", availableCouriersState);
// 	return loading ? (
// 		<Loader />
// 	) : error ? (
// 		<Message type='error'>{error}</Message>
// 	) : (
// 		<>
// 			<h1>Order ID:  {order._id}</h1>
// 			{order.shiprocketShipmentId && (
// 				<div>Shipment ID: {order.shiprocketShipmentId}</div>
// 			)}
// 			<Flex w='full' py='5' direction='column'>
// 				<Grid templateColumns='3fr 2fr' gap='20'>
// 					{/* Column 1 */}
// 					<Flex direction='column'>
// 						{/* Shipping */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Shipping
// 							</Heading>
// 							<Text>
// 								Name: <strong>{order.user.name}</strong>
// 							</Text>
// 							<Text>
// 								Email:{' '}
// 								<strong>
// 									<a href={`mailto:${order.user.email}`}>{order.user.email}</a>
// 								</strong>
// 							</Text>
// 							<Text>
// 								<strong>Address: </strong>
// 								{order.shippingAddress.address}, {order.shippingAddress.city},{' '}
// 								{order.shippingAddress.postalCode},{' '}
// 								{order.shippingAddress.country}
// 							</Text>
// 							<Text>
// 							<strong>Phone No: </strong>
// 							{order.shippingAddress.phone}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isDelivered ? (
// 									<Message type='success'>
// 										Delivered on {order.deliveredAt}
// 									</Message>
// 								) : (
// 									<Message type='warning'>Not Delivered</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Payment Method */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Payment Method
// 							</Heading>
// 							<Text>
// 								<strong>Method: </strong>
// 								{order.paymentMethod?.toUpperCase()}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isPaid ? (
// 									<Message type='success'>Paid on {order.paidAt}</Message>
// 								) : (
// 									<Message type='warning'>Not Paid</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Order Items */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Order Items
// 							</Heading>
// 							<Box>
// 								{order.orderItems.length === 0 ? (
// 									<Message>No Order Info</Message>
// 								) : (
// 									<Box py='2'>
// 										{order.orderItems.map((item, idx) => (
// 											<Flex
// 												key={idx}
// 												alignItems='center'
// 												justifyContent='space-between'>
// 												<Flex py='2' alignItems='center'>
// 													<Image
// 														src={item.image}
// 														alt={item.name}
// 														w='12'
// 														h='12'
// 														objectFit='cover'
// 														mr='6'
// 													/>
// 													<Link
// 														fontWeight='bold'
// 														fontSize='xl'
// 														as={RouterLink}
// 														to={`/products/${item.product}`}>
// 														{item.name}
// 													</Link>
// 												</Flex>

// 												<Text fontSize='lg' fontWeight='semibold'>
// 													{item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
// 												</Text>
// 											</Flex>
// 										))}
// 									</Box>
// 								)}
// 							</Box>
// 						</Box>

// 						{/* Available Couriers */}
// 						{
// 							!order.isPaid ? (

// 								<Box borderBottom="1px" py="6" borderColor="gray.300">
// 									<Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
// 										Available Couriers
// 									</Heading>
// 									{loadingCouriers ? (
// 										<Loader />
// 									) : errorCouriers ? (
// 										<Message type="error">{errorCouriers}</Message>
// 									) : availableCouriers && availableCouriers.length > 0 ? (
// 										<Box>
// 											<Select
// 												placeholder="Select a courier"
// 												value={selectedCourier?.courier_name || ''}
// 												onChange={handleCourierChange}
// 											>
// 												{availableCouriers.map((courier, index) => (
// 													<option key={index} value={courier.courier_name}>
// 														{courier.courier_name + ' ' + courier.rate}
// 													</option>
// 												))}
// 											</Select>
// 										</Box>
// 									) : (
// 										<Message type="info">No couriers available.</Message>
// 									)}
// 								</Box>
// 							) : (
// 								<Box mt='4'>
// 									<Text fontSize='xl'>
// 										<strong>Estimated Delivery Time: </strong>
// 										{estimatedDelivery}
// 									</Text>
// 								</Box>

// 							)}

// 					</Flex>

// 					{/* Column 2 */}
// 					<Flex
// 						direction='column'
// 						bgColor='white'
// 						justifyContent='space-between'
// 						py='8'
// 						px='8'
// 						shadow='md'
// 						rounded='lg'
// 						borderColor='gray.300'>
// 						<Box>
// 							<Heading mb='6' as='h2' fontSize='3xl' fontWeight='bold'>
// 								Order Summary
// 							</Heading>

// 							{/* Items Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Items</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.itemsPrice}
// 								</Text>
// 							</Flex>

// 							{/* Shipping Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Shipping</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{selectedCourier ? selectedCourier.rate : order.shippingPrice}
// 								</Text>
// 							</Flex>

// 							{/* Tax Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Tax</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.taxPrice}
// 								</Text>
// 							</Flex>

// 							{/* Total Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Total</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{calculateTotalPrice}
// 								</Text>
// 							</Flex>


// 						</Box>

// 						{/* PAYMENT BUTTON */}
// 						{!order.isPaid && (
// 							<Box>
// 								{loadingPay ? (
// 									<Loader />
// 								) : (
// 									<PayPalScriptProvider
// 										options={{
// 											clientId:
// 												'AUovBO90owBawRxVyoBse0wH_X0K-k_NbScKdEK1ULHjvn3QI2QJgVnoLb11FFYyG69iFdDcNhcGIkjL',
// 											components: 'buttons',
// 										}}>
// 										<PayPalButtons
// 											createOrder={(data, actions) => {
// 												const totalPrice = calculateTotalPrice.toFixed(2);
// 												return actions.order.create({
// 													purchase_units: [
// 														{
// 															amount: {
// 																value: totalPrice,
// 															},
// 														},
// 													],
// 												});
// 											}}
// 											onApprove={(data, actions) => {
// 												return actions.order.capture().then((details) => {
// 													console.log(details);
// 													const paymentResult = {
// 														id: details.id,
// 														status: details.status,
// 														update_time: details.update_time,
// 														email_address: details.email_address,
// 													};
// 													successPaymentHandler(paymentResult);
// 												});
// 											}}
// 										/>
// 									</PayPalScriptProvider>
// 								)}
// 							</Box>
// 						)}

// 						{/* Order Deliver Button */}
// 						{loadingDeliver && <Loader />}
// 						{userInfo && userInfo.isAdmin && order.isPaid && !order.isDelivered && (
// 							<Button type='button' colorScheme='teal' onClick={deliverHandler}>Mark as Delivered</Button>
// 						)}


// 					</Flex>
// 				</Grid>
// 			</Flex>
// 		</>
// 	);
// };

// export default OrderScreen;


// import {
//     Button,
//     Flex,
//     Grid,
//     Heading,
//     Image,
//     Text,
//     Select,
//     Box,
//     FormControl,
//     FormLabel,
//     Textarea,
//   } from "@chakra-ui/react";
//   import { Link as RouterLink, useParams, useNavigate } from "react-router-dom";
//   import Rating from "../components/Rating";
//   import { useEffect, useState } from "react";
//   import { useDispatch, useSelector } from "react-redux";
//   import {
//     createProductReview,
//     listProductDetails,
//   } from "../actions/productActions";
//   import Loader from "../components/Loader";
//   import Message from "../components/Message";
//   import { PRODUCT_REVIEW_CREATE_RESET } from "../constants/productConstants";
  
//   const ProductScreen = () => {
//     const { id } = useParams();
//     const dispatch = useDispatch();
//     const navigate = useNavigate();
  
//     const [qty, setqty] = useState(1);
//     const [rating, setRating] = useState(1);
//     const [comment, setComment] = useState("");
  
//     const productDetails = useSelector((state) => state.productDetails);
//     const { loading, error, product } = productDetails;
  
//     const userLogin = useSelector((state) => state.userLogin);
//     const { userInfo } = userLogin;
  
//     const productReviewCreate = useSelector((state) => state.productReviewCreate);
//     const { success: successProductReview, error: errorProductReview } =
//       productReviewCreate;
  
//     useEffect(() => {
//       if (successProductReview) {
//         alert("Review submitted");
//         setRating(1);
//         setComment("");
//         dispatch({ type: PRODUCT_REVIEW_CREATE_RESET });
//       }
//       dispatch(listProductDetails(id));
//     }, [id, dispatch, successProductReview]);
  
//     const addToCarHandler = () => {
//       navigate(`/cart/${id}?qty=${qty}`);
//     };
  
//     const submitHandler = (e) => {
//       e.preventDefault();
//       dispatch(createProductReview(id, { rating, comment }));
//     };
  
//     return (
//       <>
//         <Flex mb="5">
//           <Button as={RouterLink} to="/" colorScheme="gray">
//             Go Back
//           </Button>
//         </Flex>
  
//         {loading ? (
//           <Loader />
//         ) : error ? (
//           <Message type="error">{error}</Message>
//         ) : (
//           <>
//             <Grid templateColumns={{ base: '1fr', md: '1fr 1fr' }} gap="10">
//               {/* {Column 1} */}
//               <Image src={product.image} alt={product.name} borderRadius="md" />
  
//               {/* {Column 2} */}
//               <Flex direction="column">
//                 <Heading as="h5" fontSize="base" color="gray.500">
//                   {product.brand}
//                 </Heading>
  
//                 <Heading as="h2" fontSize="4xl" mb="4">
//                   {product.name}
//                 </Heading>
  
//                 <Rating
//                   value={product.rating}
//                   text={`${product.numReviews} reviews`}
//                 />
  
//                 <Heading
//                   as="h5"
//                   fontSize="4xl"
//                   fontWeight="bold"
//                   color="teal.600"
//                   my="5"
//                 >
//                   ₹{product.price}
//                 </Heading>
  
//                 <Text>{product.description}</Text>
//               </Flex>
  
//               {/* {Column 3} */}
  
//               <Flex direction="column">
//                 <Flex justifyContent="space-between" py="2">
//                   <Text>Price: </Text>
//                   <Text fontWeight="bold">₹{product.price}</Text>
//                 </Flex>
  
//                 <Flex justifyContent="space-between" py="2">
//                   <Text>Status: </Text>
//                   <Text fontWeight="bold">
//                     {product.countInStock > 0 ? "In Stock" : "Not available"}
//                   </Text>
//                 </Flex>
  
//                 {product.countInStock > 0 && (
//                   <Flex justifyContent="space-between" py="2">
//                     <Text>Qty: </Text>
//                     <Select
//                       value={qty}
//                       onChange={(e) => setqty(e.target.value)}
//                       width="30%"
//                     >
//                       {[...Array(product.countInStock).keys()].map((i) => (
//                         <option value={i + 1}>{i + 1}</option>
//                       ))}
//                     </Select>
//                   </Flex>
//                 )}
  
//                 <Button
//                   bg="gray.800"
//                   colorScheme="teal"
//                   textTransform="uppercase"
//                   letterSpacing="wide"
//                   onClick={addToCarHandler}
//                   isDisabled={product.countInStock === 0}
//                 >
//                   Add to cart
//                 </Button>
//               </Flex>
//             </Grid>
  
//             {/* Review Form */}
//             <Box
//               p="10"
//               bgColor="white"
//               rounded="md"
//               mt="10"
//               borderColor="gray.300"
//             >
//               <Heading as="h3" size="lg" mb="6">
//                 Write a review
//               </Heading>
  
//               {product?.reviews?.length === 0 && <Message>No Reviews</Message>}
  
//               {product?.reviews?.length !== 0 && (
//                 <Box p="4" bgColor="white" rounded="md" mb="1" mt="5">
//                   {product?.reviews?.map((review) => (
//                     <Flex direction="column" key={review._id} mb="5">
//                       <Flex justifyContent="space-between">
//                         <Text fontSize="lg">
//                           <strong>{review.name}</strong>
//                         </Text>
//                         <Rating value={review.rating} />
//                       </Flex>
//                       <Text mt="2">{review.comment}</Text>
//                     </Flex>
//                   ))}
//                 </Box>
//               )}
  
//               {errorProductReview && (
//                 <Message type="error">{errorProductReview}</Message>
//               )}
  
//               {userInfo ? (
//                 <form onSubmit={submitHandler}>
//                   <FormControl id="rating" mb="3">
//                     <FormLabel>Rating</FormLabel>
//                     <Select
//                       placeholder="Select Option"
//                       value={rating}
//                       onChange={(e) => setRating(e.target.value)}
//                     >
//                       <option>Select...</option>
//                       <option value="1">1 - Poor</option>
//                       <option value="2">2 - Okay</option>
//                       <option value="3">3 - Good</option>
//                       <option value="4">4 - Very Good</option>
//                       <option value="5">5 - Excellent</option>
//                     </Select>
//                   </FormControl>
  
//                   <FormControl id="comment" mb="3">
//                     <FormLabel>Comment</FormLabel>
//                     <Textarea
//                       value={comment}
//                       onChange={(e) => setComment(e.target.value)}
//                     ></Textarea>
//                   </FormControl>
  
//                   <Button colorScheme="teal" type="submit">
//                     Post Review
//                   </Button>
//                 </form>
//               ) : (
//                 <Message>Please login to write a review</Message>
//               )}
//             </Box>
//           </>
//         )}
//       </>
//     );
//   };
  
//   export default ProductScreen;
  

  // const handlePayment = useCallback(async (totalPrice) => {
  //   const params = {

  //   }
  //   const order = await createRazorPayOrder(params);

  //   const options = {
  //     key: "rzp_test_scVhdCqnABHXEi",
  //     amount: totalPrice,
  //     currency: "INR",
  //     name: "Acme Corp",
  //     description: "Test Transaction",
  //     image: "https://example.com/your_logo",
  //     order_id: order.id,
  //     handler: (res) => {
  //       console.log(res);

  //       const paymentResult = {
  //         id: res.razorpay_payment_id,
  //         status: res.payment_status,
  //         update_time: new Date().toString(),
  //         email_address: res.email || '',
  //       };

  //       dispatch(payRazorOrder(orderId, paymentResult))
  //     },
  //     prefill: {
  //       name: "Piyush Garg",
  //       email: "youremail@example.com",
  //       contact: "9999999999",
  //     },
  //     notes: {
  //       address: "Razorpay Corporate Office",
  //     },
  //     theme: {
  //       color: "#3399cc",
  //     },
  //   };

  //   const rzpay = new Razorpay(options);
  //   rzpay.open();
  // }, [Razorpay, dispatch, orderId]);