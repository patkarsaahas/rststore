import React, { useCallback, useState } from 'react'
import Button from './Button'

const CallBack = () => {
    const [count, setCount] = useState(0);
    const handleClick = useCallback(() => {
        setCount(count + 1);
      }, [count]);
    
  return (
    <div>
        <p>Count: {count}</p>

        <Button onClick={handleClick}>Increment</Button>
    </div>
  )
}

export default CallBack