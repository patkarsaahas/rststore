import React from 'react'

const Button = ({onClick, children}) => {
    console.log('Button is rendered');
  return (
    <button onClick={onClick}>{children}</button>
  
  )
}

export default Button