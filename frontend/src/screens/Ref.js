import React, { useRef } from 'react'

const Ref = () => {
    const inputRef = useRef(null);

    const handleButtonClick = () => {
        // Focus the input element
        console.log(inputRef.current);
        inputRef.current.focus();
    };
    return (
        <div>
            {/* Input element with ref */}
            <input ref={inputRef} type="text"/>
            {/* Button to focus the input element */}
            <button onClick={handleButtonClick}>Focus the input</button>
        </div>
    )
}

export default Ref