import React from 'react';

// Define the initial theme values
const defaultTheme = {
  background: 'white',
  foreground: 'black'
};

// Create a new context object
const ThemeContext = React.createContext(defaultTheme);

// Export the context object
export default ThemeContext;