import React from 'react'
import { Button, Flex, FormControl, FormLabel, Heading, Input, Select, Spacer } from '@chakra-ui/react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { saveShippingAddress } from '../actions/cartActions'
import CheckoutSteps from '../components/CheckoutSteps'
import FormContainer from '../components/FormContainer'
import countries from '../data/countries'
const ShippingScreen = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const cart = useSelector((state) => state.cart);
    const { shippingAddress } = cart;
    // console.log(cart)

    const [address, setAddress] = useState(shippingAddress.address || '');
    const [city, setCity] = useState(shippingAddress.city || '');
    const [postalCode, setPostalCode] = useState(shippingAddress.postalCode || '');
    const [phone, setPhone] = useState(shippingAddress.phone || '');

    const [country, setCountry] = useState(shippingAddress.country || '');
    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(saveShippingAddress({ address, city, postalCode, phone, country  }));
        navigate('/payment');
    }
    return (
        <>
            <Flex w={'full'} alignItems={'center'} justifyContent={'center'} py={'5'}>
                <FormContainer>
                    <Heading as={'h2'} mb={'8'} fontSize={'3xl'}>
                        Shipping
                    </Heading>
                    <CheckoutSteps step1 step2 />

                    <form onSubmit={submitHandler}>
                        {/* Address */}
                        <FormControl id='address'>
                            <FormLabel htmlFor='address'>
                                Address
                            </FormLabel>
                            <Input id='address' type='text' placeholder='Your Address' value={address} onChange={(e) => setAddress(e.target.value)}></Input>
                        </FormControl>

                        <Spacer h='3'></Spacer>

                        {/* City */}
                        <FormControl id='city'>
                            <FormLabel htmlFor='city'>City</FormLabel>
                            <Input id='city' type='text' placeholder='Your City'
                                value={city} onChange={(e) => setCity(e.target.value)}></Input>
                        </FormControl>

                        <Spacer h='3'></Spacer>

                        {/* Postal Code */}

                        <FormControl id='postalCode'>
                            <FormLabel htmlFor='postalCode'>Postal Code</FormLabel>
                            <Input id='postalCode' type='text' placeholder='Your Postal Code'
                                value={postalCode} onChange={(e) => setPostalCode(e.target.value)}></Input>
                        </FormControl>

                        <Spacer h='3'></Spacer>

                        {/* Phone No */}

                        <FormControl id='phone'>
                            <FormLabel htmlFor='phone'>Phone No</FormLabel>
                            <Input id='phone' type='tel' placeholder='Your Phone No'
                                value={phone} onChange={(e) => setPhone(e.target.value)} required></Input>
                        </FormControl>

                        {/* Country */}
                        <FormControl id='country'>
                            <FormLabel htmlFor='country'>Country</FormLabel>
                            <Select value={country} onChange={(e) => setCountry(e.target.value)} placeholder='Select option'>{countries.map((country) => (
                                <option key={country} value={country}>{country}</option>
                            ))}</Select>
                        </FormControl>
                        <Spacer h='3'></Spacer>

                        <Button type='submit' colorScheme='teal' mt={'4'}>Continue</Button>
                    </form>
                </FormContainer>
            </Flex>
        </>
    )
}

export default ShippingScreen