import React from 'react'
import { Box, Flex, Grid, Heading, Image, Text, Link } from '@chakra-ui/react';
import left_women from '../images/left_women2.webp'
import middle_tshirt from '../images/middle_tshirt.webp'
import right_men from '../images/right_men.webp'
import sale_is_on from '../images/sale_is_on.webp'
import products from '../products';
// import ProductCard from '../components/ProductCard'
import NewProductCard from '../components/NewProductCard';
import { Link as RouterLink } from 'react-router-dom';
const NewHomeScreen = () => {
    return (
        <>
            <Flex alignItems={{ base: 'center', md: 'center' }} justifyContent={{ base: 'center', md: 'center' }} direction={'column'}>
                <Box mb='2'>
                    <Heading as='h1' fontSize='2.5rem'>RST STORE</Heading>
                </Box>
                <Box background='gray.800' color='white' w='342px' h='33.4px' mb='2' textAlign='center' p='1'>There's One for Everyone</Box>
            </Flex>

            <Flex alignItems={'center'} justifyContent={'center'} gap={'12px'} direction={{ base: 'column', md: 'row' }}>
                <Link as={RouterLink} to='/products/women/fashion'>
                    <Box className='left-women' position='relative' overflow='hidden' _hover={{ '.hoverText': { opacity: 1 } }} display={{ base: 'none', md: 'block' }}>
                        <Image src={left_women} alt='women-sect' w={{ base: '100%', md: '372px' }} h='449px' objectFit="cover"></Image>
                        {/* Hover text */}
                        <Box
                            className="hoverText"
                            position="absolute"
                            top="50%"
                            left="50%"
                            transform="translate(-50%, -50%)"
                            color="black"
                            fontSize="2xl"
                            fontWeight="bold"
                            opacity={0} // Initially hidden
                            transition="opacity 0.3s ease-in-out" // Smooth fade-in effect
                        >
                            Women
                        </Box>
                    </Box>
                </Link>

                <Link as={RouterLink} to='/products'>
                    <Box
                        position="relative"
                        w={{ base: '100%', md: '754px' }}
                        h={{ base: 'full', md: '449px' }}
                        overflow="hidden"
                        _hover={{ '.hoverText': { opacity: 1 } }} // Trigger hover effect
                    >
                        <Image
                            src={middle_tshirt}
                            alt='middle-sect'
                            w="100%"
                            h="100%"
                            objectFit="cover"
                        />

                        {/* Hover text */}
                        <Box
                            className="hoverText"
                            position="absolute"
                            top="50%"
                            left="50%"
                            transform="translate(-50%, -50%)"
                            color="white"
                            fontSize="2xl"
                            fontWeight="bold"
                            opacity={0} // Initially hidden
                            transition="opacity 0.3s ease-in-out" // Smooth fade-in effect
                        >
                            Latest Products
                        </Box>
                    </Box>
                </Link>

                <Link as={RouterLink} to='/products/men/fashion'>
                    <Box position='relative' overflow='hidden' _hover={{ '.hoverText': { opacity: 1 } }}>
                        <Image src={right_men} alt='men-sect' w={{ base: '100%', md: '372px' }} h='449px' display={{ base: 'none', md: 'block' }} objectFit='cover'></Image>
                        <Box
                            className="hoverText"
                            position="absolute"
                            top="50%"
                            left="50%"
                            transform="translate(-50%, -50%)"
                            color="black"
                            fontSize="2xl"
                            fontWeight="bold"
                            opacity={0} // Initially hidden
                            transition="opacity 0.3s ease-in-out" // Smooth fade-in effect
                        >
                            Mens
                        </Box>
                    </Box>
                </Link>
            </Flex>

            <Box className='New_Drops' mt='1.5rem'>
                <Heading as='h2' fontSize='2rem' textAlign='center' px='2' py='2' >New Drops</Heading>
                <Grid templateColumns={{ base: '1fr', md: '1fr 1fr 1fr 1fr' }} gap='8' ml='1rem' mr='1rem'>
                    {products.slice(0, 4).map((prod) => (
                        <NewProductCard key={prod._id} product={prod} />
                    ))}
                </Grid>

            </Box>

            <Box className='sale_is_on'>
                <Flex alignItems='center' direction={{ base: 'column', md: 'row' }} justifyContent='center' gap='3rem'>
                    <Box width='full' h='725px' bgGradient='linear(to-t, gray.700, gray.100)'>

                        <Flex alignItems='center' justifyContent='center' height='100%'>
                            <Box textAlign='center' color='black'>
                                <Text fontSize='1rem'>SALE IS ON!</Text>
                                <Text fontSize='7rem'>25% OFF</Text>
                                <Text fontSize='1rem'>25% off sitewide using TEES25 at checkout</Text>
                            </Box>
                        </Flex>
                    </Box>

                    <Box display={{ base: 'none', md: 'block' }}>
                        <Image src={sale_is_on} alt='young-men' w='100%' h='725px'></Image>
                    </Box>
                </Flex>
            </Box>
        </>

    )
}

export default NewHomeScreen