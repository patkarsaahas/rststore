// import React, { useEffect } from 'react';
// import { Box, Flex, Grid, Heading, Image, Link, Text, Button } from '@chakra-ui/react';
// import { useDispatch, useSelector } from 'react-redux';
// import { Link as RouterLink, useParams } from 'react-router-dom';
// import { createOrder, deliverOrder, getOrderDetails } from '../actions/orderActions';
// import Loader from '../components/Loader';
// import Message from '../components/Message';
// import { ORDER_DELIVER_RESET, ORDER_PAY_RESET } from '../constants/orderConstants';
// import useRazorpay from 'react-razorpay';

// const RazorPayScreen = () => {
//   const dispatch = useDispatch();
//   const { id: orderId } = useParams();
//   const [Razorpay] = useRazorpay();

//   const orderDetails = useSelector((state) => state.orderDetails);
//   const { order, loading, error } = orderDetails;

//   const orderPay = useSelector((state) => state.orderPay);
//   const { loading: loadingPay } = orderPay;

//   const orderDeliver = useSelector((state) => state.orderDeliver);
//   const { loading: loadingDeliver } = orderDeliver;

//   const userLogin = useSelector((state) => state.userLogin);
//   const { userInfo } = userLogin;

//   useEffect(() => {
//     dispatch({ type: ORDER_PAY_RESET });
//     dispatch({ type: ORDER_DELIVER_RESET });

//     dispatch(getOrderDetails(orderId));
//   }, [dispatch, orderId]);

//   const handlePayment = async (totalPrice) => {
//     const params = {
//       // Define your parameters here
//     };

//     const order = await createOrder(params);
//     console.log(order);

//     const options = {
//       key: "rzp_test_scVhdCqnABHXEi",
//       amount:  totalPrice * 100,
//       currency: "INR",
//       name: "Acme Corp",
//       description: "Test Transaction",
//       image: "https://example.com/your_logo",
//       order_id: order._id,
//       handler: function (response) {
//         alert(response.razorpay_payment_id);
//         alert(response.razorpay_order_id);
//         alert(response.razorpay_signature);
//       },
//       prefill: {
//         name: "Piyush Garg",
//         email: "youremail@example.com",
//         contact: "9999999999",
//       },
//       notes: {
//         address: "Razorpay Corporate Office",
//       },
//       theme: {
//         color: "#3399cc",
//       },
//     };

//     const rzp1 = new Razorpay(options);

//     rzp1.on("payment.failed", function (response) {
//       alert(response.error.code);
//       alert(response.error.description);
//       alert(response.error.source);
//       alert(response.error.step);
//       alert(response.error.reason);
//       alert(response.error.metadata.order_id);
//       alert(response.error.metadata.payment_id);
//     });

//     rzp1.open();
//   }

//   const deliverHandler = () => dispatch(deliverOrder(order));
//   return loading ? (
//     <Loader />
//   ) : error ? (
//     <Message type='error'>{error}</Message>
//   ) : (
//     <>
//       <Flex w='full' py='5' direction='column'>
//         <Grid templateColumns='3fr 2fr' gap='20'>
//           {/* Column 1 */}
//           <Flex direction='column'>
//             {/* Shipping */}
//             <Box borderBottom='1px' py='6' borderColor='gray.300'>
//               <Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
//                 Shipping
//               </Heading>
//               <Text>
//                 Name: <strong>{order.user.name}</strong>
//               </Text>
//               <Text>
//                 Email:{' '}
//                 <strong>
//                   <a href={`mailto:${order.user.email}`}>{order.user.email}</a>
//                 </strong>
//               </Text>
//               <Text>
//                 <strong>Address: </strong>
//                 {order.shippingAddress.address}, {order.shippingAddress.city},{' '}
//                 {order.shippingAddress.postalCode},{' '}
//                 {order.shippingAddress.country}
//               </Text>
//               <Text mt='4'>
//                 {order.isDelivered ? (
//                   <Message type='success'>
//                     Delivered on {order.deliveredAt}
//                   </Message>
//                 ) : (
//                   <Message type='warning'>Not Delivered</Message>
//                 )}
//               </Text>
//             </Box>

//             {/* Payment Method */}
//             <Box borderBottom='1px' py='6' borderColor='gray.300'>
//               <Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
//                 Payment Method
//               </Heading>
//               <Text>
//                 <strong>Method: </strong>
//                 {order.paymentMethod?.toUpperCase()}
//               </Text>
//               <Text mt='4'>
//                 {order.isPaid ? (
//                   <Message type='success'>Paid on {order.paidAt}</Message>
//                 ) : (
//                   <Message type='warning'>Not Paid</Message>
//                 )}
//               </Text>
//             </Box>

//             {/* Order Items */}
//             <Box borderBottom='1px' py='6' borderColor='gray.300'>
//               <Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
//                 Order Items
//               </Heading>
//               <Box>
//                 {order.orderItems.length === 0 ? (
//                   <Message>No Order Info</Message>
//                 ) : (
//                   <Box py='2'>
//                     {order.orderItems.map((item, idx) => (
//                       <Flex
//                         key={idx}
//                         alignItems='center'
//                         justifyContent='space-between'>
//                         <Flex py='2' alignItems='center'>
//                           <Image
//                             src={item.image}
//                             alt={item.name}
//                             w='12'
//                             h='12'
//                             objectFit='cover'
//                             mr='6'
//                           />
//                           <Link
//                             fontWeight='bold'
//                             fontSize='xl'
//                             as={RouterLink}
//                             to={`/products/${item.product}`}>
//                             {item.name}
//                           </Link>
//                         </Flex>

//                         <Text fontSize='lg' fontWeight='semibold'>
//                           {item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
//                         </Text>
//                       </Flex>
//                     ))}
//                   </Box>
//                 )}
//               </Box>
//             </Box>
//           </Flex>

//           {/* Column 2 */}
//           <Flex
//             direction='column'
//             bgColor='white'
//             justifyContent='space-between'
//             py='8'
//             px='8'
//             shadow='md'
//             rounded='lg'
//             borderColor='gray.300'>
//             <Box>
//               <Heading mb='6' as='h2' fontSize='3xl' fontWeight='bold'>
//                 Order Summary
//               </Heading>

//               {/* Items Price */}
//               <Flex
//                 borderBottom='1px'
//                 py='2'
//                 borderColor='gray.200'
//                 alignitems='center'
//                 justifyContent='space-between'>
//                 <Text fontSize='xl'>Items</Text>
//                 <Text fontWeight='bold' fontSize='xl'>
//                   ₹{order.itemsPrice}
//                 </Text>
//               </Flex>

//               {/* Shipping Price */}
//               <Flex
//                 borderBottom='1px'
//                 py='2'
//                 borderColor='gray.200'
//                 alignitems='center'
//                 justifyContent='space-between'>
//                 <Text fontSize='xl'>Shipping</Text>
//                 <Text fontWeight='bold' fontSize='xl'>
//                   ₹{order.shippingPrice}
//                 </Text>
//               </Flex>

//               {/* Tax Price */}
//               <Flex
//                 borderBottom='1px'
//                 py='2'
//                 borderColor='gray.200'
//                 alignitems='center'
//                 justifyContent='space-between'>
//                 <Text fontSize='xl'>Tax</Text>
//                 <Text fontWeight='bold' fontSize='xl'>
//                   ₹{order.taxPrice}
//                 </Text>
//               </Flex>

//               {/* Total Price */}
//               <Flex
//                 borderBottom='1px'
//                 py='2'
//                 borderColor='gray.200'
//                 alignitems='center'
//                 justifyContent='space-between'>
//                 <Text fontSize='xl'>Total</Text>
//                 <Text fontWeight='bold' fontSize='xl'>
//                   ₹{order.totalPrice}
//                 </Text>
//               </Flex>
//             </Box>

//             {/* PAYMENT BUTTON */}
//             {!order.isPaid && (
//               <Box>
//                 {loadingPay ? (
//                   <Loader />
//                 ) : (
//                   <Box><button onClick={handlePayment(order.totalPrice)}>RazorPay</button></Box>
//                 )}
//               </Box>
//             )}

//             {/* Order Deliver Button */}
//             {loadingDeliver && <Loader />}
//             {userInfo && userInfo.isAdmin && order.isPaid && !order.isDelivered && (
//               <Button type='button' colorScheme='teal' onClick={deliverHandler}>Mark as Delivered</Button>
//             )}
//           </Flex>
//         </Grid>
//       </Flex>
//     </>
//   );
// }

// export default RazorPayScreen

// import { Box, Flex, Grid, Heading, Image, Link, Text, Button } from '@chakra-ui/react';
// import { PayPalButtons, PayPalScriptProvider } from '@paypal/react-paypal-js';
// import { useEffect } from 'react';
// import { useDispatch, useSelector } from 'react-redux';
// import { Link as RouterLink, useParams } from 'react-router-dom';
// import { deliverOrder, getOrderDetails, payOrder } from '../actions/orderActions';
// import Loader from '../components/Loader';
// import Message from '../components/Message';
// import { ORDER_DELIVER_RESET, ORDER_PAY_RESET } from '../constants/orderConstants';

// const OrderScreen = () => {
// 	const dispatch = useDispatch();
// 	const { id: orderId } = useParams();

// 	const orderDetails = useSelector((state) => state.orderDetails);
// 	const { order, loading, error } = orderDetails;

// 	const orderPay = useSelector((state) => state.orderPay);
// 	const { loading: loadingPay, success: successPay } = orderPay;

// 	const orderDeliver = useSelector((state) => state.orderDeliver);
// 	const { loading: loadingDeliver, success: successDeliver } = orderDeliver;

// 	const userLogin = useSelector((state) => state.userLogin);
// 	const { userInfo } = userLogin;

// 	useEffect(() => {
// 		dispatch({ type: ORDER_PAY_RESET });
// 		dispatch({ type: ORDER_DELIVER_RESET });

// 		if (!order.taxPrice || successPay || successDeliver) {
// 			// dispatch({ type: ORDER_PAY_RESET });
// 			// dispatch({ type: ORDER_DELIVER_RESET });

// 			dispatch(getOrderDetails(orderId));
// 		}
// 	}, [dispatch, orderId, order, successPay, successDeliver]);

// 	const successPaymentHandler = (paymentResult) => {
// 		dispatch(payOrder(orderId, paymentResult));
// 	};

// 	const deliverHandler = () => dispatch(deliverOrder(order));
// 	return loading ? (
// 		<Loader />
// 	) : error ? (
// 		<Message type='error'>{error}</Message>
// 	) : (
// 		<>
// 			<Flex w='full' py='5' direction='column'>
// 				<Grid templateColumns='3fr 2fr' gap='20'>
// 					{/* Column 1 */}
// 					<Flex direction='column'>
// 						{/* Shipping */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Shipping
// 							</Heading>
// 							<Text>
// 								Name: <strong>{order.user.name}</strong>
// 							</Text>
// 							<Text>
// 								Email:{' '}
// 								<strong>
// 									<a href={`mailto:${order.user.email}`}>{order.user.email}</a>
// 								</strong>
// 							</Text>
// 							<Text>
// 								<strong>Address: </strong>
// 								{order.shippingAddress.address}, {order.shippingAddress.city},{' '}
// 								{order.shippingAddress.postalCode},{' '}
// 								{order.shippingAddress.country}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isDelivered ? (
// 									<Message type='success'>
// 										Delivered on {order.deliveredAt}
// 									</Message>
// 								) : (
// 									<Message type='warning'>Not Delivered</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Payment Method */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Payment Method
// 							</Heading>
// 							<Text>
// 								<strong>Method: </strong>
// 								{order.paymentMethod?.toUpperCase()}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isPaid ? (
// 									<Message type='success'>Paid on {order.paidAt}</Message>
// 								) : (
// 									<Message type='warning'>Not Paid</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Order Items */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Order Items
// 							</Heading>
// 							<Box>
// 								{order.orderItems.length === 0 ? (
// 									<Message>No Order Info</Message>
// 								) : (
// 									<Box py='2'>
// 										{order.orderItems.map((item, idx) => (
// 											<Flex
// 												key={idx}
// 												alignItems='center'
// 												justifyContent='space-between'>
// 												<Flex py='2' alignItems='center'>
// 													<Image
// 														src={item.image}
// 														alt={item.name}
// 														w='12'
// 														h='12'
// 														objectFit='cover'
// 														mr='6'
// 													/>
// 													<Link
// 														fontWeight='bold'
// 														fontSize='xl'
// 														as={RouterLink}
// 														to={`/products/${item.product}`}>
// 														{item.name}
// 													</Link>
// 												</Flex>

// 												<Text fontSize='lg' fontWeight='semibold'>
// 													{item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
// 												</Text>
// 											</Flex>
// 										))}
// 									</Box>
// 								)}
// 							</Box>
// 						</Box>
// 					</Flex>

// 					{/* Column 2 */}
// 					<Flex
// 						direction='column'
// 						bgColor='white'
// 						justifyContent='space-between'
// 						py='8'
// 						px='8'
// 						shadow='md'
// 						rounded='lg'
// 						borderColor='gray.300'>
// 						<Box>
// 							<Heading mb='6' as='h2' fontSize='3xl' fontWeight='bold'>
// 								Order Summary
// 							</Heading>

// 							{/* Items Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Items</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.itemsPrice}
// 								</Text>
// 							</Flex>

// 							{/* Shipping Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Shipping</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.shippingPrice}
// 								</Text>
// 							</Flex>

// 							{/* Tax Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Tax</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.taxPrice}
// 								</Text>
// 							</Flex>

// 							{/* Total Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Total</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.totalPrice}
// 								</Text>
// 							</Flex>
// 						</Box>

// 						{/* PAYMENT BUTTON */}
// 						{!order.isPaid && (
// 							<Box>
// 								{loadingPay ? (
// 									<Loader />
// 								) : (
// 									<PayPalScriptProvider
// 										options={{
// 											clientId:
// 												'AUovBO90owBawRxVyoBse0wH_X0K-k_NbScKdEK1ULHjvn3QI2QJgVnoLb11FFYyG69iFdDcNhcGIkjL',
// 											components: 'buttons',
// 										}}>
// 										<PayPalButtons
// 											createOrder={(data, actions) => {
// 												return actions.order.create({
// 													purchase_units: [
// 														{
// 															amount: {
// 																value: order.totalPrice,
// 															},
// 														},
// 													],
// 												});
// 											}}
// 											onApprove={(data, actions) => {
// 												return actions.order.capture().then((details) => {
// 													console.log(details);
// 													const paymentResult = {
// 														id: details.id,
// 														status: details.status,
// 														update_time: details.update_time,
// 														email_address: details.email_address,
// 													};
// 													successPaymentHandler(paymentResult);
// 												});
// 											}}
// 										/>
// 									</PayPalScriptProvider>
// 								)}
// 							</Box>
// 						)}

// 						{/* Order Deliver Button */}
// 						{loadingDeliver && <Loader />}
// 						{userInfo && userInfo.isAdmin && order.isPaid && !order.isDelivered && (
// 							<Button type='button' colorScheme='teal' onClick={deliverHandler}>Mark as Delivered</Button>
// 						)}
// 					</Flex>
// 				</Grid>
// 			</Flex>
// 		</>
// 	);
// };

// export default OrderScreen;


// import { useEffect, useState } from 'react';
// import { CourierServiceability, GetToken, Tracking_OrderId } from 'shiprocket-api';
// const [shiprocketToken, setShiprocketToken] = useState(null);
// const [courierServiceabilityResponse, setCourierServiceabilityResponse] = useState(null);
// const [trackingResponse, setTrackingResponse] = useState(null);
// const fetchShiprocketToken = async () => {
//     try {
//       const tokenResponse = await GetToken({
//         email: 'example@email.com',
//         password: 'password',
//       });
//       setShiprocketToken(tokenResponse.token);
//     } catch (error) {
//       console.error('Error fetching Shiprocket token:', error);
//     }
//   };
  
//   const checkCourierServiceability = async () => {
//     try {
//       const response = await CourierServiceability({
//         auth: {
//           email: 'example@email.com',
//           password: 'password',
//         },
//         params: {
//           pickup_postcode: 600000,
//           delivery_postcode: 600005,
//           weight: 2,
//           cod: 1,
//         },
//         token: shiprocketToken,
//       });
//       setCourierServiceabilityResponse(response);
//     } catch (error) {
//       console.error('Error checking courier serviceability:', error);
//     }
//   };
  
//   const trackOrder = async () => {
//     try {
//       const response = await Tracking_OrderId({
//         auth: {
//           email: 'example@email.com',
//           password: 'password',
//         },
//         params: {
//           order_id: '55097',
//         },
//         token: shiprocketToken,
//       });
//       setTrackingResponse(response);
//     } catch (error) {
//       console.error('Error tracking order:', error);
//     }
//   };
  


/**
 * @desc Update order to delivered
 * @router PUT /api/orders/:id
 * @access private/admin
 */

// const updateOrderToDelivered = async(req,res) =>{
// 	const order = await Order.findById(req.params.id);
// 	if(order){
// 		order.isDelivered = true;
// 		order.deliveredAt = Date.now();

// 		const updatedOrder = await order.save();
// 		res.json(updatedOrder);
// 	} else {
// 		res.status(404);
// 		throw new Error('Order not found');
// 	}
// }


// import {
// 	Box,
// 	Button,
// 	Flex,
// 	Grid,
// 	Heading,
// 	Image,
// 	Link,
// 	Text,
// } from '@chakra-ui/react';
// import { useEffect } from 'react';
// import { useDispatch, useSelector } from 'react-redux';
// import { Link as RouterLink, useNavigate } from 'react-router-dom';
// import { createOrder } from '../actions/orderActions';
// import CheckoutSteps from '../components/CheckoutSteps';
// import Message from '../components/Message';

// const PlaceOrderScreen = () => {
// 	const dispatch = useDispatch();
// 	const navigate = useNavigate();

// 	const cart = useSelector((state) => state.cart);
// 	console.log(cart);

// 	cart.itemsPrice = cart.cartItems.reduce(
// 		(acc, currVal) => acc + currVal.price * +currVal.qty,
// 		0
// 	);
// 	cart.shippingPrice = cart.cartItems < 10000 ? 10000 : 0;
// 	cart.taxPrice = (28 * cart.itemsPrice) / 100;
// 	cart.totalPrice = cart.itemsPrice + cart.shippingPrice + cart.taxPrice;

// 	const orderCreate = useSelector((state) => state.orderCreate);
// 	const { order, success } = orderCreate;

// 	const placeOrderHandler = () => {
// 		dispatch(
// 			createOrder({
// 				orderItems: cart.cartItems,
// 				shippingAddress: cart.shippingAddress,
// 				paymentMethod: cart.paymentMethod,
// 				itemsPrice: cart.itemsPrice,
// 				shippingPrice: cart.shippingPrice,
// 				taxPrice: cart.taxPrice,
// 				totalPrice: cart.totalPrice,
// 			})
// 		);
// 	};

// 	useEffect(() => {
// 		// Check if success is true and cart is defined
// 		if (success && cart) {
// 			// Check the payment method
// 			const paymentMethod = cart.paymentMethod || ''; // Ensure cart.paymentMethod is defined

// 			// Navigate based on the payment method
// 			if (paymentMethod === 'paypal') {
// 				navigate(`/order/${order._id}`);
// 			} else {
// 				navigate(`/razorpay/order/${order._id}`);
// 			}
// 		}
// 	}, [success, navigate, order, cart]); // Include cart in the dependency array


// 	return (
// 		<Flex w='full' direction='column' py='5'>
// 			<CheckoutSteps step1 step2 step3 step4 />

// 			<Grid templateColumns='3fr 2fr' gap='20'>
// 				{/* Column 1 */}
// 				<Flex direction='column'>
// 					{/* Shipping */}
// 					<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 						<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 							Shipping
// 						</Heading>
// 						<Text>
// 							<strong>Address: </strong>
// 							{cart.shippingAddress.address}, {cart.shippingAddress.city},{' '}
// 							{cart.shippingAddress.postalCode}, {cart.shippingAddress.country}
// 						</Text>
// 					</Box>

// 					{/* Payment Method */}
// 					<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 						<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 							Payment Method
// 						</Heading>
// 						<Text>
// 							<strong>Method: </strong>
// 							{cart.paymentMethod.toUpperCase()}
// 						</Text>
// 					</Box>

// 					{/* Order Items */}
// 					<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 						<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 							Order Items
// 						</Heading>
// 						<Box>
// 							{cart.cartItems.length === 0 ? (
// 								<Message>Your cart is empty</Message>
// 							) : (
// 								<Box py='2'>
// 									{cart.cartItems.map((item, idx) => (
// 										<Flex
// 											key={idx}
// 											alignItems='center'
// 											justifyContent='space-between'>
// 											<Flex py='2' alignItems='center'>
// 												<Image
// 													src={item.image}
// 													alt={item.name}
// 													w='12'
// 													h='12'
// 													objectFit='cover'
// 													mr='6'
// 												/>
// 												<Link
// 													fontWeight='bold'
// 													fontSize='xl'
// 													as={RouterLink}
// 													to={`/products/${item.product}`}>
// 													{item.name}
// 												</Link>
// 											</Flex>

// 											<Text fontSize='lg' fontWeight='semibold'>
// 												{item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
// 											</Text>
// 										</Flex>
// 									))}
// 								</Box>
// 							)}
// 						</Box>
// 					</Box>
// 				</Flex>

// 				{/* Column 2 */}
// 				<Flex
// 					direction='column'
// 					bgColor='white'
// 					justifyContent='space-between'
// 					py='8'
// 					px='8'
// 					shadow='md'
// 					rounded='lg'
// 					borderColor='gray.300'>
// 					<Box>
// 						<Heading mb='6' as='h2' fontSize='3xl' fontWeight='bold'>
// 							Order Summary
// 						</Heading>

// 						{/* Items Price */}
// 						<Flex
// 							borderBottom='1px'
// 							py='2'
// 							borderColor='gray.200'
// 							alignitems='center'
// 							justifyContent='space-between'>
// 							<Text fontSize='xl'>Items</Text>
// 							<Text fontWeight='bold' fontSize='xl'>
// 								₹{cart.itemsPrice}
// 							</Text>
// 						</Flex>

// 						{/* Shipping Price */}
// 						<Flex
// 							borderBottom='1px'
// 							py='2'
// 							borderColor='gray.200'
// 							alignitems='center'
// 							justifyContent='space-between'>
// 							<Text fontSize='xl'>Shipping</Text>
// 							<Text fontWeight='bold' fontSize='xl'>
// 								₹{cart.shippingPrice}
// 							</Text>
// 						</Flex>

// 						{/* Tax Price */}
// 						<Flex
// 							borderBottom='1px'
// 							py='2'
// 							borderColor='gray.200'
// 							alignitems='center'
// 							justifyContent='space-between'>
// 							<Text fontSize='xl'>Tax</Text>
// 							<Text fontWeight='bold' fontSize='xl'>
// 								₹{cart.taxPrice}
// 							</Text>
// 						</Flex>

// 						{/* Total Price */}
// 						<Flex
// 							borderBottom='1px'
// 							py='2'
// 							borderColor='gray.200'
// 							alignitems='center'
// 							justifyContent='space-between'>
// 							<Text fontSize='xl'>Total</Text>
// 							<Text fontWeight='bold' fontSize='xl'>
// 								₹{cart.totalPrice}
// 							</Text>
// 						</Flex>
// 					</Box>
// 					<Button
// 						size='lg'
// 						textTransform='uppercase'
// 						colorScheme='yellow'
// 						type='button'
// 						w='full'
// 						onClick={placeOrderHandler}
// 						disabled={cart.cartItems === 0}>
// 						Place Order
// 					</Button>
// 				</Flex>
// 			</Grid>
// 		</Flex>
// 	);
// };

// export default PlaceOrderScreen;



/**
 * @desc		Create new order
 * @router	POST /api/orders
 * @access	private
 */
// const createOrder = async (req, res) => {
// 	const {
// 		orderItems,
// 		shippingAddress,
// 		paymentMethod,
// 		itemsPrice,
// 		taxPrice,
// 		shippingPrice,
// 		totalPrice,
// 		// shiprocketOrderId,
// 	} = req.body;

// 	if (orderItems && orderItems.length === 0) {
// 		res.status(400);
// 		throw new Error('No order items');
// 	} else {
// 		const order = new Order({
// 			orderItems,
// 			user: req.user._id,
// 			shippingAddress,
// 			paymentMethod,
// 			itemsPrice,
// 			taxPrice,
// 			shippingPrice,
// 			totalPrice,
// 			// shiprocketOrderId,
// 		});

// 		const createdOrder = await order.save();
// 		res.status(201).json(createdOrder);
// 	}
// };



// const placeOrderHandler = async () => {
//     try {
//         const token = await GetToken({
//             email: 'patkarsaahas@gmail.com',
//             password: 'tempUse@486',
//         });

//         console.log('Token:', token); // Log the token to ensure it is retrieved

//         const serviceabilityResponse = await CourierServiceability({
//             auth: {
//                 token,
//             },
//             params: {
//                 pickup_postcode: '400068', // Example known serviceable pickup postal code
//                 delivery_postcode: cart.shippingAddress.postalCode, // Use the postal code from the cart
//                 weight: 2,
//                 cod: 1,
//             },
//         });

//         console.log('Serviceability Response:', serviceabilityResponse); // Log the full response

//         // Check if the serviceability response is a valid date
//         const isDate = (date) => !isNaN(Date.parse(date));

//         if (isDate(serviceabilityResponse)) {
//             setIsServiceable(true);

//             // Dispatch the create order action
//             const createdOrder = dispatch(
//                 createOrder({
//                     orderItems: cart.cartItems,
//                     shippingAddress: cart.shippingAddress,
//                     paymentMethod: cart.paymentMethod,
//                     itemsPrice: cart.itemsPrice,
//                     shippingPrice: cart.shippingPrice,
//                     taxPrice: cart.taxPrice,
//                     totalPrice: cart.totalPrice,
//                 })
//             );

            
//             // Extract the created order details
//             // const orderId = createdOrder._id;
//             console.log('Order Created in DB:',createdOrder._id);

//             if(!createdOrder._id){
//                 throw new Error('Order creation failed. id missing');
//             }

//             // Prepare Shiprocket order payload
//             const shiprocketOrder = {
//                 order_id: createdOrder._id,
//                 order_date: new Date().toISOString(),
//                 pickup_location: 'Borivali', // Replace with actual pickup location
//                 billing_customer_name: cart.shippingAddress.name || 'Test Name', // Placeholder or actual name
//                 billing_last_name: '',
//                 billing_address: cart.shippingAddress.address,
//                 billing_city: cart.shippingAddress.city,
//                 billing_pincode: cart.shippingAddress.postalCode,
//                 billing_state: cart.shippingAddress.state || 'Test State', // Placeholder or actual state
//                 billing_country: cart.shippingAddress.country,
//                 billing_email: 'patkarsaahas@gmail.com', // Placeholder or actual email
//                 billing_phone: '1234567890', // Placeholder or actual phone
//                 shipping_is_billing: true,
//                 order_items: cart.cartItems.map(item => ({
//                     name: item.name,
//                     sku: item.product.toString(),
//                     units: item.qty,
//                     selling_price: item.price,
//                 })),
//                 payment_method: cart.paymentMethod === 'paypal' ? 'Prepaid' : 'COD',
//                 sub_total: cart.itemsPrice,
//                 length: 10,
//                 breadth: 10,
//                 height: 10,
//                 weight: 0.5,
//             };

//             // Create order in Shiprocket
//             try {
//                 const shiprocketResponse = await axios.post(
//                     'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc',
//                     shiprocketOrder,
//                     {
//                         headers: {
//                             'Content-Type': 'application/json',
//                             Authorization: `Bearer ${token}`,
//                         },
//                     }
//                 );

//                 console.log('Shiprocket Order Created:', shiprocketResponse.data);
//             } catch (shiprocketError) {
//                 console.error('Failed to create order in Shiprocket:', shiprocketError.response.data);
//                 // Optionally, update the created order in DB to indicate Shiprocket order creation failure
//             }

//         } else {
//             setIsServiceable(false);
//             console.error('Address not serviceable:', serviceabilityResponse);
//         }
//     } catch (error) {
//         console.error('Error placing order:', error);
//         setIsServiceable(false);
//     }
// };


// import {
// 	Box,
// 	Button,
// 	Flex,
// 	Grid,
// 	Heading,
// 	Image,
// 	Link,
// 	Text,
// } from '@chakra-ui/react';
// import { useEffect, useState } from 'react';
// import { useDispatch, useSelector } from 'react-redux';
// import { Link as RouterLink, useNavigate } from 'react-router-dom';
// import { createOrder } from '../actions/orderActions';
// import CheckoutSteps from '../components/CheckoutSteps';
// import Message from '../components/Message';
// import { CourierServiceability, GetToken } from 'shiprocket-api';
// import axios from 'axios';

// const PlaceOrderScreen = () => {
// 	const dispatch = useDispatch();
// 	const navigate = useNavigate();
// 	const [isServiceable, setIsServiceable] = useState(null);

// 	const cart = useSelector((state) => state.cart);

// 	cart.itemsPrice = cart.cartItems.reduce(
// 		(acc, currVal) => acc + currVal.price * +currVal.qty,
// 		0
// 	);
// 	cart.shippingPrice = cart.cartItems < 10000 ? 10000 : 0;
// 	cart.taxPrice = (28 * cart.itemsPrice) / 100;
// 	cart.totalPrice = cart.itemsPrice + cart.shippingPrice + cart.taxPrice;

// 	const orderCreate = useSelector((state) => state.orderCreate);
// 	const { order, success } = orderCreate;

// 	const placeOrderHandler = async () => {
// 		try {
// 			const token = await GetToken({
// 				email: 'patkarsaahas@gmail.com',
// 				password: 'tempUse@486',
// 			});

// 			console.log('Token:', token);

// 			const serviceabilityResponse = await CourierServiceability({
// 				auth: { token },
// 				params: {
// 					pickup_postcode: '400068',
// 					delivery_postcode: cart.shippingAddress.postalCode,
// 					weight: 2,
// 					cod: 1,
// 				},
// 			});

// 			console.log('Serviceability Response:', serviceabilityResponse);

// 			const isDate = (date) => !isNaN(Date.parse(date));

// 			if (isDate(serviceabilityResponse)) {
// 				setIsServiceable(true);

// 				dispatch(
// 					createOrder({
// 						orderItems: cart.cartItems,
// 						shippingAddress: cart.shippingAddress,
// 						paymentMethod: cart.paymentMethod,
// 						itemsPrice: cart.itemsPrice,
// 						shippingPrice: cart.shippingPrice,
// 						taxPrice: cart.taxPrice,
// 						totalPrice: cart.totalPrice,
// 					})
// 				);

// 				const shiprocketOrder = {
// 					order_id: Math.random() * 1000,
// 					order_date: new Date().toISOString(),
// 					pickup_location: 'Borivali', // Replace with actual pickup location
// 					billing_customer_name: cart.shippingAddress.name || 'Test Name', // Placeholder or actual name
// 					billing_last_name: '',
// 					billing_address: cart.shippingAddress.address,
// 					billing_city: cart.shippingAddress.city,
// 					billing_pincode: cart.shippingAddress.postalCode,
// 					billing_state: cart.shippingAddress.state || 'Test State', // Placeholder or actual state
// 					billing_country: cart.shippingAddress.country,
// 					billing_email: 'patkarsaahas@gmail.com', // Placeholder or actual email
// 					billing_phone: '1234567890', // Placeholder or actual phone
// 					shipping_is_billing: true,
// 					order_items: cart.cartItems.map(item => ({
// 						name: item.name,
// 						sku: item.product.toString(),
// 						units: item.qty,
// 						selling_price: item.price,
// 					})),
// 					payment_method: cart.paymentMethod === 'paypal' ? 'Prepaid' : 'COD',
// 					sub_total: cart.itemsPrice,
// 					length: 10,
// 					breadth: 10,
// 					height: 10,
// 					weight: 0.5,
// 				};
// 				console.log(shiprocketOrder);

// 				// Create order in Shiprocket
// 				try {
// 					const shiprocketResponse = await axios.post(
// 						'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc',
// 						shiprocketOrder,
// 						{
// 							headers: {
// 								'Content-Type': 'application/json',
// 								Authorization: `Bearer ${token}`,
// 							},
// 						}
// 					);

// 					console.log('Shiprocket Order Created:', shiprocketResponse.data);
// 				} catch (shiprocketError) {
// 					console.error('Failed to create order in Shiprocket:', shiprocketError.response.data);
// 					// Optionally, update the created order in DB to indicate Shiprocket order creation failure
// 				}

// 			} else {
// 				setIsServiceable(false);
// 				console.error('Address not serviceable:', serviceabilityResponse);
// 			}
// 		} catch (error) {
// 			console.error('Error checking serviceability:', error);
// 			setIsServiceable(false);
// 		}
// 	};



// 	useEffect(() => {
// 		if (success) {
// 			if (cart.paymentMethod === 'paypal') {
// 				navigate(`/order/${order._id}`);
// 			} else {
// 				navigate(`/razorpay/order/${order._id}`);
// 			}
// 		}
// 	}, [success, navigate, order, cart]);

// 	return (
// 		<Flex w='full' direction='column' py='5'>
// 			<CheckoutSteps step1 step2 step3 step4 />

// 			<Grid templateColumns='3fr 2fr' gap='20'>
// 				{/* Column 1 */}
// 				<Flex direction='column'>
// 					{/* Shipping */}
// 					<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 						<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 							Shipping
// 						</Heading>
// 						<Text>
// 							<strong>Address: </strong>
// 							{cart.shippingAddress.address}, {cart.shippingAddress.city},{' '}
// 							{cart.shippingAddress.postalCode}, {cart.shippingAddress.country}
// 						</Text>
// 					</Box>

// 					{/* Payment Method */}
// 					<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 						<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 							Payment Method
// 						</Heading>
// 						<Text>
// 							<strong>Method: </strong>
// 							{cart.paymentMethod.toUpperCase()}
// 						</Text>
// 					</Box>

// 					{/* Order Items */}
// 					<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 						<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 							Order Items
// 						</Heading>
// 						<Box>
// 							{cart.cartItems.length === 0 ? (
// 								<Message>Your cart is empty</Message>
// 							) : (
// 								<Box py='2'>
// 									{cart.cartItems.map((item, idx) => (
// 										<Flex
// 											key={idx}
// 											alignItems='center'
// 											justifyContent='space-between'>
// 											<Flex py='2' alignItems='center'>
// 												<Image
// 													src={item.image}
// 													alt={item.name}
// 													w='12'
// 													h='12'
// 													objectFit='cover'
// 													mr='6'
// 												/>
// 												<Link
// 													fontWeight='bold'
// 													fontSize='xl'
// 													as={RouterLink}
// 													to={`/products/${item.product}`}>
// 													{item.name}
// 												</Link>
// 											</Flex>

// 											<Text fontSize='lg' fontWeight='semibold'>
// 												{item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
// 											</Text>
// 										</Flex>
// 									))}
// 								</Box>
// 							)}
// 						</Box>
// 					</Box>
// 				</Flex>

// 				{/* Column 2 */}
// 				<Flex
// 					direction='column'
// 					bgColor='white'
// 					justifyContent='space-between'
// 					py='8'
// 					px='8'
// 					shadow='md'
// 					rounded='lg'
// 					borderColor='gray.300'>
// 					<Box>
// 						<Heading mb='6' as='h2' fontSize='3xl' fontWeight='bold'>
// 							Order Summary
// 						</Heading>

// 						{/* Items Price */}
// 						<Flex
// 							borderBottom='1px'
// 							py='2'
// 							borderColor='gray.200'
// 							alignitems='center'
// 							justifyContent='space-between'>
// 							<Text fontSize='xl'>Items</Text>
// 							<Text fontWeight='bold' fontSize='xl'>
// 								₹{cart.itemsPrice}
// 							</Text>
// 						</Flex>

// 						{/* Shipping Price */}
// 						<Flex
// 							borderBottom='1px'
// 							py='2'
// 							borderColor='gray.200'
// 							alignitems='center'
// 							justifyContent='space-between'>
// 							<Text fontSize='xl'>Shipping</Text>
// 							<Text fontWeight='bold' fontSize='xl'>
// 								₹{cart.shippingPrice}
// 							</Text>
// 						</Flex>

// 						{/* Tax Price */}
// 						<Flex
// 							borderBottom='1px'
// 							py='2'
// 							borderColor='gray.200'
// 							alignitems='center'
// 							justifyContent='space-between'>
// 							<Text fontSize='xl'>Tax</Text>
// 							<Text fontWeight='bold' fontSize='xl'>
// 								₹{cart.taxPrice}
// 							</Text>
// 						</Flex>

// 						{/* Total Price */}
// 						<Flex
// 							borderBottom='1px'
// 							py='2'
// 							borderColor='gray.200'
// 							alignitems='center'
// 							justifyContent='space-between'>
// 							<Text fontSize='xl'>Total</Text>
// 							<Text fontWeight='bold' fontSize='xl'>
// 								₹{cart.totalPrice}
// 							</Text>
// 						</Flex>
// 					</Box>
// 					<Button
// 						size='lg'
// 						textTransform='uppercase'
// 						colorScheme='yellow'
// 						type='button'
// 						w='full'
// 						onClick={placeOrderHandler}
// 						disabled={cart.cartItems.length === 0}>
// 						Place Order
// 					</Button>
// 					{isServiceable && availableCouriers.length > 0 && (
// 						<div>
// 							<h2>Select Courier Service</h2>
// 							<ul>
// 								{availableCouriers.map((courier) => (
// 									<li key={courier.courier_company_id}>
// 										<input
// 											type="radio"
// 											id={courier.courier_company_id}
// 											name="courier"
// 											value={courier.courier_company_id}
// 											onChange={() => setSelectedCourier(courier)}
// 										/>
// 										<label htmlFor={courier.courier_company_id}>
// 											{courier.courier_name} (Pickup Date: {courier.pickup_date}, Delivery Date: {courier.estimated_delivery_date})
// 										</label>
// 									</li>
// 								))}
// 							</ul>
// 						</div>
// 					)}
// 				</Flex>
// 			</Grid>
// 		</Flex>
// 	);
// };

// export default PlaceOrderScreen;



// const placeOrderHandler = async () => {
//     try {
//         const token = await GetToken({
//             email: 'patkarsaahas@gmail.com',
//             password: 'tempUse@486',
//         });

//         console.log('Token:', token);

//         const serviceabilityResponse = await CourierServiceability({
//             auth: { token },
//             params: {
//                 pickup_postcode: '400068',
//                 delivery_postcode: cart.shippingAddress.postalCode,
//                 weight: 2,
//                 cod: 1,
//             },
//         });

//         console.log('Serviceability Response:', serviceabilityResponse);

//         const isDate = (date) => !isNaN(Date.parse(date));

//         if (isDate(serviceabilityResponse)) {
//             setIsServiceable(true);

//             dispatch(
//                 createOrder({
//                     orderItems: cart.cartItems,
//                     shippingAddress: cart.shippingAddress,
//                     paymentMethod: cart.paymentMethod,
//                     itemsPrice: cart.itemsPrice,
//                     shippingPrice: cart.shippingPrice,
//                     taxPrice: cart.taxPrice,
//                     totalPrice: cart.totalPrice,
//                 })
//             );

//             const shiprocketOrder = {
//                 order_id: Math.random()*1000,
//                 order_date: new Date().toISOString(),
//                 pickup_location: 'Borivali', // Replace with actual pickup location
//                 billing_customer_name: cart.shippingAddress.name || 'Test Name', // Placeholder or actual name
//                 billing_last_name: '',
//                 billing_address: cart.shippingAddress.address,
//                 billing_city: cart.shippingAddress.city,
//                 billing_pincode: cart.shippingAddress.postalCode,
//                 billing_state: cart.shippingAddress.state || 'Test State', // Placeholder or actual state
//                 billing_country: cart.shippingAddress.country,
//                 billing_email: 'patkarsaahas@gmail.com', // Placeholder or actual email
//                 billing_phone: '1234567890', // Placeholder or actual phone
//                 shipping_is_billing: true,
//                 order_items: cart.cartItems.map(item => ({
//                     name: item.name,
//                     sku: item.product.toString(),
//                     units: item.qty,
//                     selling_price: item.price,
//                 })),
//                 payment_method: cart.paymentMethod === 'paypal' ? 'Prepaid' : 'COD',
//                 sub_total: cart.itemsPrice,
//                 length: 10,
//                 breadth: 10,
//                 height: 10,
//                 weight: 0.5,
//             };
//             console.log(shiprocketOrder);

//             // Create order in Shiprocket
//             try {
//                 const shiprocketResponse = await axios.post(
//                     'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc',
//                     shiprocketOrder,
//                     {
//                         headers: {
//                             'Content-Type': 'application/json',
//                             Authorization: `Bearer ${token}`,
//                         },
//                     }
//                 );

//                 console.log('Shiprocket Order Created:', shiprocketResponse.data);
//             } catch (shiprocketError) {
//                 console.error('Failed to create order in Shiprocket:', shiprocketError.response.data);
//                 // Optionally, update the created order in DB to indicate Shiprocket order creation failure
//             }

//         } else {
//             setIsServiceable(false);
//             console.error('Address not serviceable:', serviceabilityResponse);
//         }
//     } catch (error) {
//         console.error('Error checking serviceability:', error);
//         setIsServiceable(false);
//     }
// };


  

// import {
//     Box,
//     Button,
//     Flex,
//     Grid,
//     Heading,
//     Image,
//     Link,
//     Text,
//     // RadioGroup,
//     // Radio,
//     // Stack,
//   } from '@chakra-ui/react';
//   import { useEffect, useState } from 'react';
//   import { useDispatch, useSelector } from 'react-redux';
//   import { Link as RouterLink, useNavigate } from 'react-router-dom';
//   import { createOrder } from '../actions/orderActions';
//   import CheckoutSteps from '../components/CheckoutSteps';
//   import Message from '../components/Message';
//   import axios from 'axios';
//   import { GetToken, CourierServiceability } from '../shiprocket';
  
//   const PlaceOrderScreen = () => {
//     const dispatch = useDispatch();
//     const navigate = useNavigate();
//     const [isServiceable, setIsServiceable] = useState(null);
//     // const [availableCouriers, setAvailableCouriers] = useState([]);
//     // const [selectedCourier, setSelectedCourier] = useState(null);
  
//     const cart = useSelector((state) => state.cart);
  
//     cart.itemsPrice = cart.cartItems.reduce(
//       (acc, currVal) => acc + currVal.price * +currVal.qty,
//       0
//     );
//     cart.shippingPrice = cart.cartItems < 10000 ? 10000 : 0;
//     cart.taxPrice = (28 * cart.itemsPrice) / 100;
//     cart.totalPrice = cart.itemsPrice + cart.shippingPrice + cart.taxPrice;
  
//     const orderCreate = useSelector((state) => state.orderCreate);
//     const { order, success } = orderCreate;
  
//     const placeOrderHandler = async () => {
//       try {
//         const token = await GetToken({
//           email: 'patkarsaahas@gmail.com',
//           password: 'tempUse@486',
//         });
  
//         console.log('Token:', token);
  
//         const serviceabilityResponse = await CourierServiceability({
//           auth: { token },
//           params: {
//             pickup_postcode: '400068',
//             delivery_postcode: cart.shippingAddress.postalCode,
//             weight: 2,
//             cod: 1,
//           },
//         });
  
//         console.log('Serviceability Response:', serviceabilityResponse);
  
//         if (serviceabilityResponse.error) {
//           console.error('Serviceability Error:', serviceabilityResponse.error);
//           setIsServiceable(false);
//           return;
//         }
  
//         console.log('Serviceability Response:', serviceabilityResponse);
  
//           const isDate = (date) => !isNaN(Date.parse(date));
  
//           if (isDate(serviceabilityResponse)) {
//               setIsServiceable(true);
  
//           dispatch(
//             createOrder({
//               orderItems: cart.cartItems,
//               shippingAddress: cart.shippingAddress,
//               paymentMethod: cart.paymentMethod,
//               itemsPrice: cart.itemsPrice,
//               shippingPrice: cart.shippingPrice,
//               taxPrice: cart.taxPrice,
//               totalPrice: cart.totalPrice,
//             })
//           );
  
//           const shiprocketOrder = {
//             order_id: Math.random() * 1000,
//             order_date: new Date().toISOString(),
//             pickup_location: 'Borivali',
//             billing_customer_name: cart.shippingAddress.name || 'Test Name',
//             billing_last_name: '',
//             billing_address: cart.shippingAddress.address,
//             billing_city: cart.shippingAddress.city,
//             billing_pincode: cart.shippingAddress.postalCode,
//             billing_state: cart.shippingAddress.state || 'Test State',
//             billing_country: cart.shippingAddress.country,
//             billing_email: 'patkarsaahas@gmail.com',
//             billing_phone: '1234567890',
//             shipping_is_billing: true,
//             order_items: cart.cartItems.map((item) => ({
//               name: item.name,
//               sku: item.product.toString(),
//               units: item.qty,
//               selling_price: item.price,
//             })),
//             payment_method: cart.paymentMethod === 'paypal' ? 'Prepaid' : 'COD',
//             sub_total: cart.itemsPrice,
//             length: 10,
//             breadth: 10,
//             height: 10,
//             weight: 0.5,
//           };
//           console.log(shiprocketOrder);
  
//           // Create order in Shiprocket
//           try {
//             const shiprocketResponse = await axios.post(
//               'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc',
//               shiprocketOrder,
//               {
//                 headers: {
//                   'Content-Type': 'application/json',
//                   Authorization: `Bearer ${token}`,
//                 },
//               }
//             );
  
//             console.log('Shiprocket Order Created:', shiprocketResponse.data);
//           } catch (shiprocketError) {
//             console.error('Failed to create order in Shiprocket:', shiprocketError.response.data);
//           }
//         } else {
//           setIsServiceable(false);
//           console.error('No courier companies available for this address.');
//         }
//       } catch (error) {
//         console.error('Error checking serviceability:', error);
//         setIsServiceable(false);
//       }
//     };
  
//     useEffect(() => {
//       if (success) {
//         if (cart.paymentMethod === 'paypal') {
//           navigate(`/order/${order._id}`);
//         } else {
//           navigate(`/razorpay/order/${order._id}`);
//         }
//       }
//     }, [success, navigate, order, cart]);
  
//     return (
//       <Flex w="full" direction="column" py="5">
//         <CheckoutSteps step1 step2 step3 step4 />
  
//         <Grid templateColumns="3fr 2fr" gap="20">
//           {/* Column 1 */}
//           <Flex direction="column">
//             {/* Shipping */}
//             <Box borderBottom="1px" py="6" borderColor="gray.300">
//               <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
//                 Shipping
//               </Heading>
//               <Text>
//                 <strong>Address: </strong>
//                 {cart.shippingAddress.address}, {cart.shippingAddress.city},{' '}
//                 {cart.shippingAddress.postalCode}, {cart.shippingAddress.country}
//               </Text>
//             </Box>
  
//             {/* Payment Method */}
//             <Box borderBottom="1px" py="6" borderColor="gray.300">
//               <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
//                 Payment Method
//               </Heading>
//               <Text>
//                 <strong>Method: </strong>
//                 {cart.paymentMethod.toUpperCase()}
//               </Text>
//             </Box>
  
//             {/* Order Items */}
//             <Box borderBottom="1px" py="6" borderColor="gray.300">
//               <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
//                 Order Items
//               </Heading>
//               <Box>
//                 {cart.cartItems.length === 0 ? (
//                   <Message>Your cart is empty</Message>
//                 ) : (
//                   <Box py="2">
//                     {cart.cartItems.map((item, idx) => (
//                       <Flex
//                         key={idx}
//                         alignItems="center"
//                         justifyContent="space-between"
//                       >
//                         <Flex py="2" alignItems="center">
//                           <Image
//                             src={item.image}
//                             alt={item.name}
//                             w="12"
//                             h="12"
//                             objectFit="cover"
//                             mr="6"
//                           />
//                           <Link
//                             fontWeight="bold"
//                             fontSize="xl"
//                             as={RouterLink}
//                             to={`/products/${item.product}`}
//                           >
//                             {item.name}
//                           </Link>
//                         </Flex>
  
//                         <Text fontSize="lg" fontWeight="semibold">
//                           {item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
//                         </Text>
//                       </Flex>
//                     ))}
//                   </Box>
//                 )}
//               </Box>
//             </Box>
  
//             {/* Courier Selection */}
//             {/* <Box borderBottom="1px" py="6" borderColor="gray.300">
//               <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
//                 Select Courier Service
//               </Heading>
//               <RadioGroup
//                 value={selectedCourier ? selectedCourier.name : ''}
//                 onChange={(value) =>
//                   setSelectedCourier(
//                     availableCouriers.find((courier) => courier.name === value)
//                   )
//                 }
//               >
//                 <Stack>
//                   {availableCouriers.length > 0 ? (
//                     availableCouriers.map((courier) => (
//                       <Radio key={courier.name} value={courier.name}>
//                         {courier.name} - ₹{courier.rate} - Estimated Delivery: {courier.etd}
//                       </Radio>
//                     ))
//                   ) : (
//                     <Text>No couriers available</Text>
//                   )}
//                 </Stack>
//                 {console.log('Render Available Couriers:', availableCouriers)}
//               </RadioGroup>
//             </Box> */}
  
//           </Flex>
  
//           {/* Column 2 */}
//           <Flex
//             direction="column"
//             bgColor="white"
//             justifyContent="space-between"
//             py="8"
//             px="8"
//             shadow="md"
//             rounded="lg"
//             borderColor="gray.300"
//           >
//             <Box>
//               <Heading mb="6" as="h2" fontSize="3xl" fontWeight="bold">
//                 Order Summary
//               </Heading>
  
//               {/* Items Price */}
//               <Flex
//                 borderBottom="1px"
//                 py="2"
//                 borderColor="gray.200"
//                 alignItems="center"
//                 justifyContent="space-between"
//               >
//                 <Text fontSize="xl">Items</Text>
//                 <Text fontWeight="bold" fontSize="xl">
//                   ₹{cart.itemsPrice}
//                 </Text>
//               </Flex>
  
//               {/* Shipping Price */}
//               <Flex
//                 borderBottom="1px"
//                 py="2"
//                 borderColor="gray.200"
//                 alignItems="center"
//                 justifyContent="space-between"
//               >
//                 <Text fontSize="xl">Shipping</Text>
//                 <Text fontWeight="bold" fontSize="xl">
//                   ₹{cart.shippingPrice}
//                 </Text>
//               </Flex>
  
//               {/* Tax Price */}
//               <Flex
//                 borderBottom="1px"
//                 py="2"
//                 borderColor="gray.200"
//                 alignItems="center"
//                 justifyContent="space-between"
//               >
//                 <Text fontSize="xl">Tax</Text>
//                 <Text fontWeight="bold" fontSize="xl">
//                   ₹{cart.taxPrice}
//                 </Text>
//               </Flex>
  
//               {/* Total Price */}
//               <Flex
//                 borderBottom="1px"
//                 py="2"
//                 borderColor="gray.200"
//                 alignItems="center"
//                 justifyContent="space-between"
//               >
//                 <Text fontSize="xl">Total</Text>
//                 <Text fontWeight="bold" fontSize="xl">
//                   ₹{cart.totalPrice}
//                 </Text>
//               </Flex>
//             </Box>
//             <Button
//               size="lg"
//               textTransform="uppercase"
//               colorScheme="yellow"
//               type="button"
//               w="full"
//               onClick={placeOrderHandler}
//               disabled={cart.cartItems.length === 0}
//             >
//               Place Order
//             </Button>
//             {isServiceable === false && (
//               <Message type="error">Address not serviceable</Message>
//             )}
//           </Flex>
//         </Grid>
//       </Flex>
//     );
//   };
  
//   export default PlaceOrderScreen;





{/* <Box borderBottom="1px" py="6" borderColor="gray.300">
            <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
              Select Courier Service
            </Heading>
            <RadioGroup
              value={selectedCourier ? selectedCourier.name : ''}
              onChange={(value) =>
                setSelectedCourier(
                  availableCouriers.find((courier) => courier.name === value)
                )
              }
            >
              <Stack>
                {availableCouriers.length > 0 ? (
                  availableCouriers.map((courier) => (
                    <Radio key={courier.name} value={courier.name}>
                      {courier.name} - ₹{courier.rate} - Estimated Delivery: {courier.etd}
                    </Radio>
                  ))
                ) : (
                  <Text>No couriers available</Text>
                )}
              </Stack>
              {console.log('Render Available Couriers:', availableCouriers)}
            </RadioGroup>
          </Box> */}



          // import {
          //   Box,
          //   Button,
          //   Flex,
          //   Grid,
          //   Heading,
          //   Image,
          //   Link,
          //   Text,
          //   // RadioGroup,
          //   // Radio,
          //   // Stack,
          // } from '@chakra-ui/react';
          // import { useEffect, useState } from 'react';
          // import { useDispatch, useSelector } from 'react-redux';
          // import { Link as RouterLink, useNavigate } from 'react-router-dom';
          // import { createOrder } from '../actions/orderActions';
          // import CheckoutSteps from '../components/CheckoutSteps';
          // import Message from '../components/Message';
          // import axios from 'axios';
          // import { GetToken, CourierServiceability } from '../shiprocket';
          
          // const PlaceOrderScreen = () => {
          //   const dispatch = useDispatch();
          //   const navigate = useNavigate();
          //   const [isServiceable, setIsServiceable] = useState(null);
          
          //   const cart = useSelector((state) => state.cart);
          
          //   cart.itemsPrice = cart.cartItems.reduce(
          //     (acc, currVal) => acc + currVal.price * +currVal.qty,
          //     0
          //   );
          //   cart.shippingPrice = cart.cartItems < 10000 ? 10000 : 0;
          //   cart.taxPrice = (28 * cart.itemsPrice) / 100;
          //   cart.totalPrice = cart.itemsPrice + cart.shippingPrice + cart.taxPrice;
          
          //   const orderCreate = useSelector((state) => state.orderCreate);
          //   const { order, success } = orderCreate;
          
          //   const placeOrderHandler = async () => {
          //     try {
          //       const token = await GetToken({
          //         email: 'patkarsaahas@gmail.com',
          //         password: 'tempUse@486',
          //       });
          
          //       console.log('Token:', token);
          
          //       const serviceabilityResponse = await CourierServiceability({
          //         auth: { token },
          //         params: {
          //           pickup_postcode: '400068',
          //           delivery_postcode: cart.shippingAddress.postalCode,
          //           weight: 2,
          //           cod: 1,
          //         },
          //       });
          
          //       console.log('Serviceability Response:', serviceabilityResponse);
          
          //       if (serviceabilityResponse.error) {
          //         console.error('Serviceability Error:', serviceabilityResponse.error);
          //         setIsServiceable(false);
          //         return;
          //       }
          
          //       const { availableCouriers } = serviceabilityResponse;
          //       console.log('Available Couriers:', availableCouriers);
          
          //       if (availableCouriers && availableCouriers.length > 0) {
          //         setIsServiceable(true);
          //         // setAvailableCouriers(availableCouriers);
          
          //         dispatch(
          //           createOrder({
          //             orderItems: cart.cartItems,
          //             shippingAddress: cart.shippingAddress,
          //             paymentMethod: cart.paymentMethod,
          //             itemsPrice: cart.itemsPrice,
          //             shippingPrice: cart.shippingPrice,
          //             taxPrice: cart.taxPrice,
          //             totalPrice: cart.totalPrice,
          //           })
          //         );
          
          //         const shiprocketOrder = {
          //           order_id: Math.random() * 1000,
          //           order_date: new Date().toISOString(),
          //           pickup_location: 'Borivali',
          //           billing_customer_name: cart.shippingAddress.name || 'Test Name',
          //           billing_last_name: '',
          //           billing_address: cart.shippingAddress.address,
          //           billing_city: cart.shippingAddress.city,
          //           billing_pincode: cart.shippingAddress.postalCode,
          //           billing_state: cart.shippingAddress.state || 'Test State',
          //           billing_country: cart.shippingAddress.country,
          //           billing_email: 'patkarsaahas@gmail.com',
          //           billing_phone: '1234567890',
          //           shipping_is_billing: true,
          //           order_items: cart.cartItems.map((item) => ({
          //             name: item.name,
          //             sku: item.product.toString(),
          //             units: item.qty,
          //             selling_price: item.price,
          //           })),
          //           payment_method: cart.paymentMethod === 'paypal' ? 'Prepaid' : 'COD',
          //           sub_total: cart.itemsPrice,
          //           length: 10,
          //           breadth: 10,
          //           height: 10,
          //           weight: 0.5,
          //         };
          //         console.log(shiprocketOrder);
          
          //         // Create order in Shiprocket
          //         try {
          //           const shiprocketResponse = await axios.post(
          //             'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc',
          //             shiprocketOrder,
          //             {
          //               headers: {
          //                 'Content-Type': 'application/json',
          //                 Authorization: `Bearer ${token}`,
          //               },
          //             }
          //           );
          
          //           console.log('Shiprocket Order Created:', shiprocketResponse.data);
          //         } catch (shiprocketError) {
          //           console.error('Failed to create order in Shiprocket:', shiprocketError.response.data);
          //         }
          //       } else {
          //         setIsServiceable(false);
          //         console.error('No courier companies available for this address.');
          //       }
          //     } catch (error) {
          //       console.error('Error checking serviceability:', error);
          //       setIsServiceable(false);
          //     }
          //   };
          
          //   useEffect(() => {
          //     if (success) {
          //       if (cart.paymentMethod === 'paypal') {
          //         navigate(`/order/${order._id}`);
          //       } else {
          //         navigate(`/razorpay/order/${order._id}`);
          //       }
          //     }
          //   }, [success, navigate, order, cart]);
          
          //   return (
          //     <Flex w="full" direction="column" py="5">
          //       <CheckoutSteps step1 step2 step3 step4 />
          
          //       <Grid templateColumns="3fr 2fr" gap="20">
          //         {/* Column 1 */}
          //         <Flex direction="column">
          //           {/* Shipping */}
          //           <Box borderBottom="1px" py="6" borderColor="gray.300">
          //             <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
          //               Shipping
          //             </Heading>
          //             <Text>
          //               <strong>Address: </strong>
          //               {cart.shippingAddress.address}, {cart.shippingAddress.city},{' '}
          //               {cart.shippingAddress.postalCode}, {cart.shippingAddress.country}
          //             </Text>
          //           </Box>
          
          //           {/* Payment Method */}
          //           <Box borderBottom="1px" py="6" borderColor="gray.300">
          //             <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
          //               Payment Method
          //             </Heading>
          //             <Text>
          //               <strong>Method: </strong>
          //               {cart.paymentMethod.toUpperCase()}
          //             </Text>
          //           </Box>
          
          //           {/* Order Items */}
          //           <Box borderBottom="1px" py="6" borderColor="gray.300">
          //             <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
          //               Order Items
          //             </Heading>
          //             <Box>
          //               {cart.cartItems.length === 0 ? (
          //                 <Message>Your cart is empty</Message>
          //               ) : (
          //                 <Box py="2">
          //                   {cart.cartItems.map((item, idx) => (
          //                     <Flex
          //                       key={idx}
          //                       alignItems="center"
          //                       justifyContent="space-between"
          //                     >
          //                       <Flex py="2" alignItems="center">
          //                         <Image
          //                           src={item.image}
          //                           alt={item.name}
          //                           w="12"
          //                           h="12"
          //                           objectFit="cover"
          //                           mr="6"
          //                         />
          //                         <Link
          //                           fontWeight="bold"
          //                           fontSize="xl"
          //                           as={RouterLink}
          //                           to={`/products/${item.product}`}
          //                         >
          //                           {item.name}
          //                         </Link>
          //                       </Flex>
          
          //                       <Text fontSize="lg" fontWeight="semibold">
          //                         {item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
          //                       </Text>
          //                     </Flex>
          //                   ))}
          //                 </Box>
          //               )}
          //             </Box>
          //           </Box>
          
          //           {/* Courier Selection */}
                    
          
          //         </Flex>
          
          //         {/* Column 2 */}
          //         <Flex
          //           direction="column"
          //           bgColor="white"
          //           justifyContent="space-between"
          //           py="8"
          //           px="8"
          //           shadow="md"
          //           rounded="lg"
          //           borderColor="gray.300"
          //         >
          //           <Box>
          //             <Heading mb="6" as="h2" fontSize="3xl" fontWeight="bold">
          //               Order Summary
          //             </Heading>
          
          //             {/* Items Price */}
          //             <Flex
          //               borderBottom="1px"
          //               py="2"
          //               borderColor="gray.200"
          //               alignItems="center"
          //               justifyContent="space-between"
          //             >
          //               <Text fontSize="xl">Items</Text>
          //               <Text fontWeight="bold" fontSize="xl">
          //                 ₹{cart.itemsPrice}
          //               </Text>
          //             </Flex>
          
          //             {/* Shipping Price */}
          //             <Flex
          //               borderBottom="1px"
          //               py="2"
          //               borderColor="gray.200"
          //               alignItems="center"
          //               justifyContent="space-between"
          //             >
          //               <Text fontSize="xl">Shipping</Text>
          //               <Text fontWeight="bold" fontSize="xl">
          //                 ₹{cart.shippingPrice}
          //               </Text>
          //             </Flex>
          
          //             {/* Tax Price */}
          //             <Flex
          //               borderBottom="1px"
          //               py="2"
          //               borderColor="gray.200"
          //               alignItems="center"
          //               justifyContent="space-between"
          //             >
          //               <Text fontSize="xl">Tax</Text>
          //               <Text fontWeight="bold" fontSize="xl">
          //                 ₹{cart.taxPrice}
          //               </Text>
          //             </Flex>
          
          //             {/* Total Price */}
          //             <Flex
          //               borderBottom="1px"
          //               py="2"
          //               borderColor="gray.200"
          //               alignItems="center"
          //               justifyContent="space-between"
          //             >
          //               <Text fontSize="xl">Total</Text>
          //               <Text fontWeight="bold" fontSize="xl">
          //                 ₹{cart.totalPrice}
          //               </Text>
          //             </Flex>
          //           </Box>
          //           <Button
          //             size="lg"
          //             textTransform="uppercase"
          //             colorScheme="yellow"
          //             type="button"
          //             w="full"
          //             onClick={placeOrderHandler}
          //             disabled={cart.cartItems.length === 0}
          //           >
          //             Place Order
          //           </Button>
          //           {isServiceable === false && (
          //             <Message type="error">Address not serviceable</Message>
          //           )}
          //         </Flex>
          //       </Grid>
          //     </Flex>
          //   );
          // };
          
          // export default PlaceOrderScreen;


//           import { Box, Flex, Grid, Heading, Image, Link, Text, Button } from '@chakra-ui/react';
// import { PayPalButtons, PayPalScriptProvider } from '@paypal/react-paypal-js';
// import { useEffect } from 'react';
// import { useDispatch, useSelector } from 'react-redux';
// import { Link as RouterLink, useParams } from 'react-router-dom';
// import { deliverOrder, getOrderDetails, payOrder } from '../actions/orderActions';
// import Loader from '../components/Loader';
// import Message from '../components/Message';
// import { ORDER_DELIVER_RESET, ORDER_PAY_RESET } from '../constants/orderConstants';

// const OrderScreen = () => {
// 	const dispatch = useDispatch();
// 	const { id: orderId } = useParams();

// 	const orderDetails = useSelector((state) => state.orderDetails);
// 	const { order, loading, error } = orderDetails;
	

// 	const orderPay = useSelector((state) => state.orderPay);
// 	const { loading: loadingPay, success: successPay } = orderPay;

// 	const orderDeliver = useSelector((state) => state.orderDeliver);
// 	const { loading: loadingDeliver, success: successDeliver } = orderDeliver;

// 	const userLogin = useSelector((state) => state.userLogin);
// 	const { userInfo } = userLogin;

// 	useEffect(() => {
// 		dispatch({ type: ORDER_PAY_RESET });
// 		dispatch({ type: ORDER_DELIVER_RESET });

// 		if (!order.taxPrice || successPay || successDeliver) {
// 			dispatch({ type: ORDER_PAY_RESET });
// 			dispatch({ type: ORDER_DELIVER_RESET });

// 			dispatch(getOrderDetails(orderId));
// 		}
// 	}, [dispatch, orderId, order, successPay, successDeliver]);

// 	const successPaymentHandler = (paymentResult) => {
// 		dispatch(payOrder(orderId, paymentResult));
// 	};

// 	const deliverHandler = () => dispatch(deliverOrder(order));
// 	return loading ? (
// 		<Loader />
// 	) : error ? (
// 		<Message type='error'>{error}</Message>
// 	) : (
// 		<>
// 			<Flex w='full' py='5' direction='column'>
// 				<Grid templateColumns='3fr 2fr' gap='20'>
// 					{/* Column 1 */}
// 					<Flex direction='column'>
// 						{/* Shipping */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Shipping
// 							</Heading>
// 							<Text>
// 								Name: <strong>{order.user.name}</strong>
// 							</Text>
// 							<Text>
// 								Email:{' '}
// 								<strong>
// 									<a href={`mailto:${order.user.email}`}>{order.user.email}</a>
// 								</strong>
// 							</Text>
// 							<Text>
// 								<strong>Address: </strong>
// 								{order.shippingAddress.address}, {order.shippingAddress.city},{' '}
// 								{order.shippingAddress.postalCode},{' '}
// 								{order.shippingAddress.country}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isDelivered ? (
// 									<Message type='success'>
// 										Delivered on {order.deliveredAt}
// 									</Message>
// 								) : (
// 									<Message type='warning'>Not Delivered</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Payment Method */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Payment Method
// 							</Heading>
// 							<Text>
// 								<strong>Method: </strong>
// 								{order.paymentMethod?.toUpperCase()}
// 							</Text>
// 							<Text mt='4'>
// 								{order.isPaid ? (
// 									<Message type='success'>Paid on {order.paidAt}</Message>
// 								) : (
// 									<Message type='warning'>Not Paid</Message>
// 								)}
// 							</Text>
// 						</Box>

// 						{/* Order Items */}
// 						<Box borderBottom='1px' py='6' borderColor='gray.300'>
// 							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
// 								Order Items
// 							</Heading>
// 							<Box>
// 								{order.orderItems.length === 0 ? (
// 									<Message>No Order Info</Message>
// 								) : (
// 									<Box py='2'>
// 										{order.orderItems.map((item, idx) => (
// 											<Flex
// 												key={idx}
// 												alignItems='center'
// 												justifyContent='space-between'>
// 												<Flex py='2' alignItems='center'>
// 													<Image
// 														src={item.image}
// 														alt={item.name}
// 														w='12'
// 														h='12'
// 														objectFit='cover'
// 														mr='6'
// 													/>
// 													<Link
// 														fontWeight='bold'
// 														fontSize='xl'
// 														as={RouterLink}
// 														to={`/products/${item.product}`}>
// 														{item.name}
// 													</Link>
// 												</Flex>

// 												<Text fontSize='lg' fontWeight='semibold'>
// 													{item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
// 												</Text>
// 											</Flex>
// 										))}
// 									</Box>
// 								)}
// 							</Box>
// 						</Box>
// 					</Flex>

// 					{/* Column 2 */}
// 					<Flex
// 						direction='column'
// 						bgColor='white'
// 						justifyContent='space-between'
// 						py='8'
// 						px='8'
// 						shadow='md'
// 						rounded='lg'
// 						borderColor='gray.300'>
// 						<Box>
// 							<Heading mb='6' as='h2' fontSize='3xl' fontWeight='bold'>
// 								Order Summary
// 							</Heading>

// 							{/* Items Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Items</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.itemsPrice}
// 								</Text>
// 							</Flex>

// 							{/* Shipping Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Shipping</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.shippingPrice}
// 								</Text>
// 							</Flex>

// 							{/* Tax Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Tax</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.taxPrice}
// 								</Text>
// 							</Flex>

// 							{/* Total Price */}
// 							<Flex
// 								borderBottom='1px'
// 								py='2'
// 								borderColor='gray.200'
// 								alignitems='center'
// 								justifyContent='space-between'>
// 								<Text fontSize='xl'>Total</Text>
// 								<Text fontWeight='bold' fontSize='xl'>
// 									₹{order.totalPrice}
// 								</Text>
// 							</Flex>
// 						</Box>

// 						{/* PAYMENT BUTTON */}
// 						{!order.isPaid && (
// 							<Box>
// 								{loadingPay ? (
// 									<Loader />
// 								) : (
// 									<PayPalScriptProvider
// 										options={{
// 											clientId:
// 												'AUovBO90owBawRxVyoBse0wH_X0K-k_NbScKdEK1ULHjvn3QI2QJgVnoLb11FFYyG69iFdDcNhcGIkjL',
// 											components: 'buttons',
// 										}}>
// 										<PayPalButtons
// 											createOrder={(data, actions) => {
// 												return actions.order.create({
// 													purchase_units: [
// 														{
// 															amount: {
// 																value: order.totalPrice,
// 															},
// 														},
// 													],
// 												});
// 											}}
// 											onApprove={(data, actions) => {
// 												return actions.order.capture().then((details) => {
// 													console.log(details);
// 													const paymentResult = {
// 														id: details.id,
// 														status: details.status,
// 														update_time: details.update_time,
// 														email_address: details.email_address,
// 													};
// 													successPaymentHandler(paymentResult);
// 												});
// 											}}
// 										/>
// 									</PayPalScriptProvider>
// 								)}
// 							</Box>
// 						)}

// 						{/* Order Deliver Button */}
// 						{loadingDeliver && <Loader />}
// 						{userInfo && userInfo.isAdmin && order.isPaid && !order.isDelivered && (
// 							<Button type='button' colorScheme='teal' onClick={deliverHandler}>Mark as Delivered</Button>
// 						)}
// 					</Flex>
// 				</Grid>
// 			</Flex>
// 		</>
// 	);
// };

// export default OrderScreen;