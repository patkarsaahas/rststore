import {
  Box,
  Button,
  Flex,
  Grid,
  Heading,
  Image,
  Link,
  Text,
  // RadioGroup,
  // Radio,
  // Stack,
} from '@chakra-ui/react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { createOrder, createRazorPayOrder, updateOrderWithShiprocketId } from '../actions/orderActions';
import CheckoutSteps from '../components/CheckoutSteps';
import Message from '../components/Message';
import axios from 'axios';
import { fetchAvailableCouriers } from '../actions/courierActions';
import { GetToken } from '../shiprocket';
// import { GetToken, CourierServiceability } from '../shiprocket';

const PlaceOrderScreen = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  // const [isServiceable, setIsServiceable] = useState(null);

  const cart = useSelector((state) => state.cart);

  const userLogin = useSelector((state) => state.userLogin);

  const { userInfo } = userLogin;

  cart.itemsPrice = cart.cartItems.reduce(
    (acc, currVal) => acc + currVal.price * +currVal.qty,
    0
  );
  cart.shippingPrice = cart.cartItems < 10000 ? 10000 : 0;
  cart.taxPrice = (28 * cart.itemsPrice) / 100;
  cart.totalPrice = cart.itemsPrice + cart.shippingPrice + cart.taxPrice;

  const orderCreate = useSelector((state) => state.orderCreate);
  const { order, success } = orderCreate;

  const orderCreateRazor = useSelector((state) => state.orderCreateRazor);
  const { order:orderRazor, success:successRazor } = orderCreateRazor;

  const placeOrderHandler = async () => {
    try {
      // setAvailableCouriers(availableCouriers);

      const params = {
        pickup_postcode: '400068',
        delivery_postcode: cart.shippingAddress.postalCode,
        weight: 2,
        cod: 1,
      }
      dispatch(fetchAvailableCouriers(params));
      const token = await GetToken({
        email: 'patkarsaahas@gmail.com',
        password: 'tempUse@486',
      });

      let orderId = null;
      let orderRazorId = null;

      if (cart.paymentMethod === 'paypal') {
        const createdOrder = await dispatch(
          createOrder({
            orderItems: cart.cartItems,
            shippingAddress: cart.shippingAddress,
            paymentMethod: cart.paymentMethod,
            itemsPrice: cart.itemsPrice,
            shippingPrice: cart.shippingPrice,
            taxPrice: cart.taxPrice,
            totalPrice: cart.totalPrice,
          })
        );
        console.log('Normal Order Created');


        orderId = createdOrder._id;
      } else {
        const createdRazorOrder = await dispatch(
          createRazorPayOrder({
            orderItems: cart.cartItems,
            shippingAddress: cart.shippingAddress,
            paymentMethod: cart.paymentMethod,
            itemsPrice: cart.itemsPrice,
            shippingPrice: cart.shippingPrice,
            taxPrice: cart.taxPrice,
            totalPrice: cart.totalPrice,
          })
        );
        console.log('Razor Order Created', createdRazorOrder);
        

        orderRazorId = createdRazorOrder.id;
        console.log(orderRazorId);
        
      }
      // console.log(userInfo.name);


      // const phoneAsInteger = parseInt(cart.shippingAddress.phone.replace(/\D/g, ''), 10); // Remove non-digit characters and convert to integer


      const shiprocketOrder = {
        order_id: cart.paymentMethod === 'paypal' ? orderId : orderRazorId,
        order_date: new Date().toISOString(),
        pickup_location: 'Borivali',
        billing_customer_name: userInfo.name || 'Test name',
        billing_last_name: '',
        billing_address: cart.shippingAddress.address,
        billing_city: cart.shippingAddress.city,
        billing_pincode: cart.shippingAddress.postalCode,
        billing_state: cart.shippingAddress.state || 'Test State',
        billing_country: cart.shippingAddress.country,
        billing_email: userInfo.email || 'test@gmail.com',
        billing_phone: cart.shippingAddress.phone,
        shipping_is_billing: true,
        order_items: cart.cartItems.map((item) => ({
          name: item.name,
          sku: item.product.toString(),
          units: item.qty,
          selling_price: item.price,
        })),
        payment_method: cart.paymentMethod === 'paypal' || cart.paymentMethod === 'razorpay' ? 'Prepaid' : 'COD',
        sub_total: cart.itemsPrice,
        length: 10,
        breadth: 10,
        height: 10,
        weight: 0.5,
      };
      console.log(shiprocketOrder);

      // Create order in Shiprocket
      try {
        const shiprocketResponse = await axios.post(
          'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc',
          shiprocketOrder,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${token}`,
            },
          }
        );
        const shiprocketOrderId = shiprocketResponse.data.order_id;
        const shiprocketShipmentId = shiprocketResponse.data.shipment_id;
        console.log('Shiprocket Order Created:', shiprocketResponse.data);

        await dispatch(updateOrderWithShiprocketId(cart.paymentMethod === 'paypal'? orderId : orderRazorId, shiprocketOrderId, shiprocketShipmentId))
      } catch (shiprocketError) {
        console.error('Failed to create order in Shiprocket:', shiprocketError.response.data);
      }
    } catch (err) {
      console.error('Error placing order:', err);
    }
  };

  useEffect(() => {
    if (success || successRazor) {
      if (cart.paymentMethod === 'paypal') {
        navigate(`/order/${order._id}`);
      } else {
        navigate(`/razorpay/order/${orderRazor.id}`);
      }
    }
  }, [success, successRazor, navigate, order, orderRazor, cart]);

  return (
    <Flex w="full" direction="column" py="5">
      <CheckoutSteps step1 step2 step3 step4 />

      <Grid templateColumns={{ base: '1fr', md: "3fr 2fr" }} gap="20">
        {/* Column 1 */}
        <Flex direction="column">
          {/* Shipping */}
          <Box borderBottom="1px" py="6" borderColor="gray.300">
            <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
              Shipping
            </Heading>
            <Text>
              <strong>Address: </strong>
              {cart.shippingAddress.address}, {cart.shippingAddress.city},{' '}
              {cart.shippingAddress.postalCode}, {cart.shippingAddress.country}
            </Text>
          </Box>

          {/* Phone No */}
          <Box borderBottom="1px" py="6" borderColor="gray.300">
            <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
              Phone No
            </Heading>
            <Text>
              {cart.shippingAddress.phone}
            </Text>
          </Box>

          {/* Payment Method */}
          <Box borderBottom="1px" py="6" borderColor="gray.300">
            <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
              Payment Method
            </Heading>
            <Text>
              <strong>Method: </strong>
              {cart.paymentMethod.toUpperCase()}
            </Text>
          </Box>

          {/* Order Items */}
          <Box borderBottom="1px" py="6" borderColor="gray.300">
            <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
              Order Items
            </Heading>
            <Box>
              {cart.cartItems.length === 0 ? (
                <Message>Your cart is empty</Message>
              ) : (
                <Box py="2">
                  {cart.cartItems.map((item, idx) => (
                    <Flex
                      key={idx}
                      alignItems="center"
                      justifyContent="space-between"
                    >
                      <Flex py="2" alignItems="center">
                        <Image
                          src={item.image}
                          alt={item.name}
                          w="12"
                          h="12"
                          objectFit="cover"
                          mr="6"
                        />
                        <Link
                          fontWeight="bold"
                          fontSize="xl"
                          as={RouterLink}
                          to={`/products/${item.product}`}
                        >
                          {item.name}
                        </Link>
                      </Flex>

                      <Text fontSize="lg" fontWeight="semibold">
                        {item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
                      </Text>
                    </Flex>
                  ))}
                </Box>
              )}
            </Box>
          </Box>

          {/* Courier Selection */}


        </Flex>

        {/* Column 2 */}
        <Flex
          direction="column"
          bgColor="white"
          justifyContent="space-between"
          py="8"
          px="8"
          shadow="md"
          rounded="lg"
          borderColor="gray.300"
        >
          <Box>
            <Heading mb="6" as="h2" fontSize="3xl" fontWeight="bold">
              Order Summary
            </Heading>

            {/* Items Price */}
            <Flex
              borderBottom="1px"
              py="2"
              borderColor="gray.200"
              alignItems="center"
              justifyContent="space-between"
            >
              <Text fontSize="xl">Items</Text>
              <Text fontWeight="bold" fontSize="xl">
                ₹{cart.itemsPrice}
              </Text>
            </Flex>

            {/* Shipping Price */}
            <Flex
              borderBottom="1px"
              py="2"
              borderColor="gray.200"
              alignItems="center"
              justifyContent="space-between"
            >
              <Text fontSize="xl">Shipping</Text>
              <Text fontWeight="bold" fontSize="xl">
                ₹{cart.shippingPrice}
              </Text>
            </Flex>

            {/* Tax Price */}
            <Flex
              borderBottom="1px"
              py="2"
              borderColor="gray.200"
              alignItems="center"
              justifyContent="space-between"
            >
              <Text fontSize="xl">Tax</Text>
              <Text fontWeight="bold" fontSize="xl">
                ₹{cart.taxPrice}
              </Text>
            </Flex>

            {/* Total Price */}
            <Flex
              borderBottom="1px"
              py="2"
              borderColor="gray.200"
              alignItems="center"
              justifyContent="space-between"
            >
              <Text fontSize="xl">Total</Text>
              <Text fontWeight="bold" fontSize="xl">
                ₹{cart.totalPrice}
              </Text>
            </Flex>
          </Box>
          <Button
            size="lg"
            textTransform="uppercase"
            colorScheme="yellow"
            type="button"
            w="full"
            onClick={placeOrderHandler}
            disabled={cart.cartItems.length === 0}
          >
            Place Order
          </Button>
          {/* {isServiceable === false && (
              <Message type="error">Address not serviceable</Message>
            )} */}
        </Flex>
      </Grid>
    </Flex>
  );
};

export default PlaceOrderScreen;