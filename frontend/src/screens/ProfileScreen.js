import React from 'react'
import {
    Box,
    Button,
    Flex,
    FormControl,
    FormLabel,
    Grid,
    Heading,
    Icon,
    Input,
    Spacer,
    Table,
    Tbody,
    Td,
    Th,
    Thead,
    Tr,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, Link as RouterLink } from 'react-router-dom';
import { getUserDetails, updateUserProfile } from '../actions/userActions';
import FormContainer from '../components/FormContainer';
import Message from '../components/Message';
import { USER_DETAILS_RESET } from '../constants/userConstants';
import Loader from '../components/Loader';
import { IoWarning } from 'react-icons/io5';
import { deleteOrder, listMyOrders } from '../actions/orderActions';
import { ORDER_CANCEL_REQUEST } from '../constants/orderConstants';
import axios from 'axios'
import { GetToken } from '../shiprocket';
const ProfileScreen = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [message, setMessage] = useState('');

    const userDetails = useSelector((state) => state.userDetails);
    const { loading, error, user } = userDetails;

    const userLogin = useSelector((state) => state.userLogin)
    const { userInfo } = userLogin;

    const userUpdateProfile = useSelector((state) => state.userUpdateProfile);
    const { success } = userUpdateProfile;

    const orderMyList = useSelector((state) => state.orderMyList);
    const { loading: loadingOrders, error: errorOrders, orders } = orderMyList;

    const orderCancel = useSelector((state) => state.orderCancel);
    const { success: successCancel } = orderCancel;

    // const orderCreate = useSelector((state) => state.orderCreate);
    // const { order } = orderCreate;

    useEffect(() => {
        if (!userInfo) {
            navigate('/login')
        } else {
            if (!user.name || success || successCancel) {
                dispatch(getUserDetails());
                dispatch(listMyOrders());
                if (successCancel) {
                    dispatch({ type: ORDER_CANCEL_REQUEST });  // Reset successCancel after re-fetching orders
                }
            } else {
                setName(user.name);
                setEmail(user.email);
            }
        }
    }, [dispatch, navigate, user, userInfo, success, successCancel]);

    const submitHandler = (e) => {
        e.preventDefault();
        if (password !== confirmPassword) {
            setMessage('Passwords do not match');
        } else {
            dispatch(updateUserProfile({ id: user._id, name, email, password }));
            dispatch({ type: USER_DETAILS_RESET })
        }
    }

    const cancelOrderHandler = async (id,shiprocketOrderId) => {
        if (window.confirm('Are you sure you want to cancel this order?')) {
            try {
                // Fetch token for Shiprocket API
                const token = await GetToken({
                    email: 'patkarsaahas@gmail.com',
                    password: 'tempUse@486',
                });
                // Cancel the order in Shiprocket
                const cancelData = { ids: [shiprocketOrderId] };
                const cancelOrder = await axios.post('https://apiv2.shiprocket.in/v1/external/orders/cancel', cancelData,
                    {
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${token}`,
                        },
                    }
                );
                console.log('Shiprocket Order Canceled:', cancelOrder.data);
                // Only delete from DB after successful cancellation from Shiprocket

                dispatch(deleteOrder(id));  // Dispatch the deleteOrder action

            } catch (error) {
                console.error('Failed to cancel order in Shiprocket:', error.response?.data || error.message);

            }
        }
    };


    return (
        <>


            <Grid templateColumns={{ base: '1fr', md: '1fr 1fr' }} py={'5'} gap={'10'}>
                <Flex w={'full'} alignItems={'center'} justifyContent={'center'} py={'5'}>
                    <FormContainer>
                        <Heading as={'h1'} mb={'8'} fontSize={'3xl'}>
                            User Profile
                        </Heading>
                        {error && <Message type='error'>{error}</Message>}
                        {message && <Message type='error'>{message}</Message>}

                        <form onSubmit={submitHandler}>
                            <FormControl id='name'>
                                <FormLabel htmlFor='name'>Your Name</FormLabel>
                                <Input id='name' type='text' placeholder='Your Full Name' value={name} onChange={(e) => setName(e.target.value)} w={{base:'fit-content', md:'100%'}}></Input>
                            </FormControl>

                            <Spacer h={'3'}></Spacer>

                            <FormControl id='email'>
                                <FormLabel htmlFor='email'>Your Email Address</FormLabel>
                                <Input id='email' type='email' placeholder='userdomain@example.com' value={email} onChange={(e) => setEmail(e.target.value)} w={{base:'fit-content', md:'100%'}}></Input>
                            </FormControl>

                            <Spacer h={'3'}></Spacer>


                            <FormControl id='password'>
                                <FormLabel htmlFor='password'>Your Password</FormLabel>
                                <Input id='password' type='password' placeholder='*******' value={password} onChange={(e) => setPassword(e.target.value)} w={{base:'fit-content', md:'100%'}}></Input>
                            </FormControl>

                            <Spacer h={'3'}></Spacer>

                            <FormControl id='confirmpassword'>
                                <FormLabel htmlFor='confirmpassword'>Your Password</FormLabel>
                                <Input id='confirmpassword' type='password' placeholder='*******' value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)} w={{base:'fit-content', md:'100%'}}></Input>
                            </FormControl>

                            <Button type='submit' colorScheme='teal' mt='4' isLoading={loading}>Update</Button>
                        </form>
                    </FormContainer>
                </Flex>
                <Box bgColor='transparent' rounded='lg' shadow='lg' px='5' py='5' overflowX='auto' width='100%'>
                    <Flex direction='column'>
                        <Heading as='h2' mb='4'>
                            My Orders
                        </Heading>
                        {/* 
                        {loadingCancel && <Loader />}
                        {errorCancel && <Message type='error'>{errorCancel}</Message>} */}


                        {loadingOrders && orders.length === 0 ? (
                            <Loader />
                        ) : errorOrders ? (
                            <Message type='error'>{errorOrders}</Message>
                        ) : (

                            <Table variant='striped'>
                                <Thead>
                                    <Tr>
                                        <Th>ID</Th>
                                        <Th>DATE</Th>
                                        <Th>TOTAL</Th>
                                        <Th>PAID</Th>
                                        <Th>METHOD</Th>
                                        <Th>CANCEL ORDER</Th>
                                        <Th>DELIVERED</Th>
                                        <Th>TRACK</Th>
                                        <Th>DETAILS</Th>
                                    </Tr>
                                </Thead>
                                <Tbody>
                                    {orders.map((order) => (
                                        <Tr key={order._id}>
                                            <Td>{order._id}</Td>
                                            <Td>{new Date(order.createdAt).toDateString()}</Td>
                                            <Td>₹{order.totalPrice}</Td>
                                            <Td>
                                                {order.isPaid ? (
                                                    new Date(order.paidAt).toDateString()
                                                ) : (
                                                    <Icon as={IoWarning} color='red' />
                                                )}{' '}
                                            </Td>
                                            <Td>{order.paymentMethod}</Td>
                                            <Td>{order.isPaid === false ? (
                                                <Button
                                                    onClick={() => cancelOrderHandler(order._id, order.shiprocketOrderId)}
                                                    colorScheme='red'
                                                    size='sm'>
                                                    Cancel Order
                                                </Button>
                                            ) : (
                                                <Icon as={IoWarning} color='red' />

                                            )}</Td>
                                            <Td>
                                                {order.isDelivered ? (
                                                    new Date(order.deliveredAt).toDateString()
                                                ) : (
                                                    <Icon as={IoWarning} color='red' />
                                                )}{' '}
                                            </Td>
                                            <Td><Button as={RouterLink} to='https://www.shiprocket.in/shipment-tracking/' backgroundColor='#735ae5' color='white' _hover={{ backgroundColor: '#9888e3', color: 'white' }} target='_blank'>Track</Button></Td>
                                            <Td>
                                                {
                                                    order.paymentMethod === 'razorpay' ? (
                                                        <Button
                                                            as={RouterLink}
                                                            to={`/razorpay/order/${order._id}`}
                                                            colorScheme='blue'
                                                            size='sm'>
                                                            Details
                                                        </Button>
                                                    ) : (
                                                        <Button
                                                            as={RouterLink}
                                                            to={`/order/${order._id}`}
                                                            colorScheme='teal'
                                                            size='sm'>
                                                            Details
                                                        </Button>
                                                    )
                                                }

                                            </Td>
                                        </Tr>
                                    ))}
                                </Tbody>
                            </Table>
                        )}
                    </Flex>
                </Box>

            </Grid>
        </>
    )
}

export default ProfileScreen