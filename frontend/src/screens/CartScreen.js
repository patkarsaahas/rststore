import {
    Box,
    Heading,
    Text,
    Image,
    Icon,
    Select,
    Button,
    Flex,
    Grid,
    Link,
}from '@chakra-ui/react';
import { useEffect } from 'react';
import { IoTrashBinSharp } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import {
    Link as RouterLink,
    useNavigate,
    useSearchParams,
    useParams,
} from 'react-router-dom';
import { addToCart, removeFromCart } from '../actions/cartActions';
import Message from '../components/Message';

const CartScreen = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const { id } = useParams();

    const [searchParams] = useSearchParams();
    let qty = searchParams.get('qty');

    const cart = useSelector((state) => state.cart);
    const { cartItems } = cart;

    useEffect(() => {
        if (id) {
            dispatch(addToCart(id, +qty))//+qty converted to number because it was in string
        }
    }, [dispatch, id, qty]);

    const removeFromCartHandler = (id) => {
        dispatch(removeFromCart(id));
    }

    const checkoutHandler = () => {
        navigate(`/login?redirect=/shipping`);
    }

    console.log(cartItems);
    return (
        <>
            <Grid>
                <Box>
                    <Heading mb={'8'}>Shopping Cart</Heading>
                    <Flex>
                        {cartItems.length === 0 ? (
                            <Message>
                                Your Cart is Empty. {' '}
                                <Link as={RouterLink} to={'/'}>Go Back</Link>
                            </Message>
                        ) : (
                            <Grid templateColumns={{base:'1fr',md:'4fr 2fr'}} gap={'10'} w='full'>
                                <Flex direction={'column'}>
                                    {cartItems.map((item) => (
                                    <Grid key={item.product} size='100%' alignItems={'center'} justifyContent={'space-between'} borderBottom={'1px'} borderColor={'gray.200'} py={'4'} px={'2'} rounded={'lg'} _hover={{ bgColor: 'gray.50' }}
                                        templateColumns={{base:'1fr 2fr 2fr',md:'1fr 4fr 2fr 2fr 2fr'}} gap={{base:'1rem'}}>
                                        {/* Product Image */}

                                        <Image src={item.image}
                                            alt={item.name}
                                            borderRadius={'lg'}
                                            w='100'
                                            h='100'
                                            objectFit={'contain'}></Image>

                                        {/* Product Name */}
                                        <Text fontWeight={'semibold'} fontSize={'lg'}>
                                            <Link as={RouterLink} to={`/product/${item.product}`}>{item.name}</Link>
                                        </Text>

                                        {/* Product Price */}
                                        <Text fontWeight={{base:'bold',md:'semibold'}} fontSize={'lg'}>₹{item.price}</Text>

                                        {/* Quantity Select Box */}
                                        <Select value={item.qty} onChange={(e) => dispatch(addToCart(item.product, +e.target.value))} width={'20'} border={{base:'1px solid black'}}>
                                            {[...Array(item.countInStock).keys()].map((i) => (
                                            <option key={i + 1}>{i + 1}</option>
                                        ))}
                                        </Select>

                                        <Button type='button' colorScheme='red' onClick={() => removeFromCartHandler(item.product)}>
                                            <Icon as={IoTrashBinSharp}></Icon>
                                        </Button>
                                    </Grid>
                                ))}
                                </Flex>

                                {/* Second Column */}

                                <Flex direction={'column'} bgColor={'gray.200'} rounded={'md'} padding={'5'} height={'48'} justifyContent={'space-between'}>
                                    <Flex direction={'column'}>
                                        <Heading as={'h2'} fontSize={'2xl'} mb='2'>
                                            Subtotal(
                                                {cartItems.reduce((acc, currVal) => acc + currVal.qty, 0)}{' '}
                                            items)
                                        </Heading>
                                        <Text fontWeight={'bold'} fontSize={'2xl'} color={'blue.600'} mb={'4'}>₹{cartItems.reduce(
                                            (acc,currVal) => acc + currVal.price * currVal.qty, 0
                                        )}</Text>

                                        <Button type='button' disabled={cartItems.length === 0} size={'lg'} colorScheme='teal' bgColor={'gray.800'} onClick={checkoutHandler}>Proceed To Checkout</Button>
                                    </Flex>
                                </Flex>
                            </Grid>
                        )}
                    </Flex>
                </Box>
            </Grid>
        </>
    );
}

export default CartScreen;