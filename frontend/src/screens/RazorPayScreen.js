import React, { useMemo, useState } from 'react'
import { Box, Flex, Grid, Heading, Image, Link, Text, Button, Select } from '@chakra-ui/react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link as RouterLink, useParams } from 'react-router-dom';
import { deliverOrder, getOrderDetails, payRazorOrder } from '../actions/orderActions';
import Loader from '../components/Loader';
import Message from '../components/Message';
import { ORDER_DELIVER_RESET, ORDER_RAZORPAY_RESET } from '../constants/orderConstants';
import useRazorpay from 'react-razorpay';
import { useCallback } from "react";
import { GetToken } from '../shiprocket';
import { fetchAvailableCouriers, generateAWB } from '../actions/courierActions';
import axios from 'axios';

const RazorPayScreen = () => {
  const dispatch = useDispatch();
  const { id: orderId } = useParams();
  const [Razorpay] = useRazorpay();

  const [selectedCourier, setSelectedCourier] = useState(null);
  const [estimatedDelivery, setEstimatedDelivery] = useState('');

  const orderDetails = useSelector((state) => state.orderDetails);
  const { order, loading: loadingRazor, error: errorRazor } = orderDetails;

  const availableCouriersState = useSelector((state) => state.availableCouriers);
  const { loading: loadingCouriers, error: errorCouriers, availableCouriers } = availableCouriersState;

  const orderRazorpay = useSelector((state) => state.orderRazorpay);
  const { loading: loadingRazorPay, success: successPay } = orderRazorpay;

  const orderDeliver = useSelector((state) => state.orderDeliver);
  const { loading: loadingDeliver, success: successDeliver } = orderDeliver;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const handleCourierChange = (e) => {
    const selectedCourier = availableCouriers.find(courier => courier.courier_name === e.target.value);
    setSelectedCourier(selectedCourier);
    if (selectedCourier) {
      setEstimatedDelivery(selectedCourier.etd); // Assuming the ETD is part of the courier object
      localStorage.setItem('selectedCourier', JSON.stringify(selectedCourier));
    }
  };

  const calculateTotalPrice = useMemo(() => {
    return (
      order?.itemsPrice +
      (selectedCourier ? selectedCourier.rate : order?.shippingPrice) +
      order?.taxPrice
    );
  }, [order, selectedCourier]);

  useEffect(() => {
    dispatch({ type: ORDER_RAZORPAY_RESET });
    dispatch({ type: ORDER_DELIVER_RESET });

    if (!order || successPay || successDeliver || order._id !== orderId) {
      // dispatch({ type: ORDER_RAZORPAY_RESET });
      // dispatch({ type: ORDER_DELIVER_RESET });

      dispatch(getOrderDetails(orderId));
    } else if (order.shippingAddress) {
      const params = {
        pickup_postcode: '400068',
        delivery_postcode: order.shippingAddress.postalCode,
        weight: 2,
        cod: order.paymentMethod === 'COD' ? 1 : 0,
      };

      const storedCourierInfo = localStorage.getItem('courierInfo');
      const storedSelectedCourier = localStorage.getItem('selectedCourier');

      if (storedCourierInfo) {
        const storedCouriers = JSON.parse(storedCourierInfo).data.available_courier_companies;
        dispatch({
          type: 'FETCH_AVAILABLE_COURIERS_SUCCESS',
          payload: storedCouriers,
        });
      } else {
        dispatch(fetchAvailableCouriers(params));
      }

      if (storedSelectedCourier) {
        const selectedCourier = JSON.parse(storedSelectedCourier);
        setSelectedCourier(selectedCourier);
        setEstimatedDelivery(selectedCourier.etd);
      }
    } else {
      console.log('Order Details', order);

    }
  }, [dispatch, orderId, order, successPay, successDeliver]);

  // console.log('shiprocket shipment id:', order);
  // console.log('order object:', order);

  // const razorpayOrderId = order?.razorpayOrderId; // Extract razorpayOrderId from the order object


  const handlePayment = useCallback(
    async (calculateTotalPrice) => {
      // Use the `razorPayOrderId` from the existing order
      if (!order || !order.razorpayOrderId) {
        console.error("Razorpay order ID not found in order details");
        return;
      }

      const options = {
        key: "rzp_test_scVhdCqnABHXEi",
        amount: Math.round(calculateTotalPrice * 100), // Amount in paise
        currency: "INR",
        name: "Acme Corp",
        description: "Test Transaction",
        image: "https://example.com/your_logo",
        order_id: order.razorPayOrderId, // Use the existing Razorpay order ID
        handler: async (res) => {
          console.log("Razorpay Payment Success:", res);

          const paymentResult = {
            id: res.razorpay_payment_id,
            status: "COMPLETED",
            update_time: new Date().toISOString(),
            email_address: res.email || "",
          };

          console.log("Changed Order ID:", orderId); // Log the Razorpay Order ID

          // Update the payment status in the database
          try {
            await dispatch(payRazorOrder(orderId, paymentResult));
            console.log('Payment update successfully');

          } catch (error) {
            console.error('Payment Error:', error.message)
          }

          try {
            console.log("Attempting to generate AWB and confirm shipment");

            const token = await GetToken({
              email: "patkarsaahas@gmail.com",
              password: "tempUse@486",
            });

            if (order?.shiprocketShipmentId) {
              console.log("Generating AWB for shipment ID:", order.shiprocketShipmentId);

              // Generate AWB
              const awbData = await generateAWB(order.shiprocketShipmentId);

              if (awbData.awb_assign_status === 1) {
                console.log("AWB Assigned:", awbData);

                // Confirm shipment
                const confirmShipmentPayload = {
                  shipment_id: order.shiprocketShipmentId,
                };

                const confirmShipmentResponse = await axios.post(
                  "https://apiv2.shiprocket.in/v1/external/courier/generate/pickup",
                  confirmShipmentPayload,
                  {
                    headers: {
                      "Content-Type": "application/json",
                      Authorization: `Bearer ${token}`,
                    },
                  }
                );

                console.log("Shipment Confirmed:", confirmShipmentResponse.data);
              } else {
                console.error("AWB Assignment failed:", awbData.message);
              }
            }
          } catch (error) {
            console.error("Error confirming shipment:", error.response?.data || error.message);
          }
        },
        prefill: {
          name: "Piyush Garg",
          email: "youremail@example.com",
          contact: "9999999999",
        },
        notes: {
          address: "Razorpay Corporate Office",
        },
        theme: {
          color: "#3399cc",
        },
      };

      console.log("Opening Razorpay Payment Gateway with options:", options);

      const rzpay = new Razorpay(options);
      rzpay.open();
    },
    [dispatch, order, orderId, Razorpay]
  );




  const deliverHandler = () => dispatch(deliverOrder(order));

  const invoiceHandler = async (shiprocketOrderId) => {
    try {
      const token = await GetToken({
        email: 'patkarsaahas@gmail.com',
        password: 'tempUse@486',
      })

      const invoiceData = { ids: [shiprocketOrderId] };
      const invoiceGenerate = await axios.post('https://apiv2.shiprocket.in/v1/external/orders/print/invoice', invoiceData, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
      console.log('Invoice Generated:', invoiceGenerate.data);

      const { is_invoice_created, invoice_url } = invoiceGenerate.data;

      if (is_invoice_created) {
        // setInvoiceUrl(invoice_url); //Save the URL state
        // alert(`Invoice genrated!😊 You can download it here: ${invoice_url}`)
        // window.open(invoice_url, '_blank'); //open url in new tab
        const link = document.createElement('a');
        link.href = invoice_url;
        link.download = 'invoice.pdf'; // Optional: Provide a filename
        link.click();
      } else {
        alert('Invoice could not be generated');
      }

    } catch (error) {
      console.error('Failed to generate invoice:', error.response?.data || error.message);
      alert('Failed to generate invoice. Please try again.');

    }
  }

  return loadingRazor ? (
    <Loader />
  ) : errorRazor ? (
    <Message type='error'>{errorRazor}</Message>
  ) : (
    <>
      <h1>Order ID:  {order._id}</h1>
      {order.shiprocketShipmentId && (
        <div>Shipment ID: {order.shiprocketShipmentId}</div>
      )}
      <Flex w='full' py='5' direction='column'>
        <Grid templateColumns='3fr 2fr' gap='20'>
          {/* Column 1 */}
          <Flex direction='column'>
            {/* Shipping */}
            <Box borderBottom='1px' py='6' borderColor='gray.300'>
              <Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
                Shipping
              </Heading>
              <Text>
                Name: <strong>{order.user.name}</strong>
              </Text>
              <Text>
                Email:{' '}
                <strong>
                  <a href={`mailto:${order.user.email}`}>{order.user.email}</a>
                </strong>
              </Text>
              <Text>
                <strong>Address: </strong>
                {order.shippingAddress.address}, {order.shippingAddress.city},{' '}
                {order.shippingAddress.postalCode},{' '}
                {order.shippingAddress.country}
              </Text>
              <Text mt='4'>
                {order.isDelivered ? (
                  <Message type='success'>
                    Delivered on {order.deliveredAt}
                  </Message>
                ) : (
                  <Message type='warning'>Not Delivered</Message>
                )}
              </Text>
            </Box>

            {/* Payment Method */}
            <Box borderBottom='1px' py='6' borderColor='gray.300'>
              <Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
                Payment Method
              </Heading>
              <Text>
                <strong>Method: </strong>
                {order.paymentMethod?.toUpperCase()}
              </Text>
              <Text mt='4'>
                {order.isPaid ? (
                  <Message type='success'>Paid on {order.paidAt}</Message>
                ) : (
                  <Message type='warning'>Not Paid</Message>
                )}
              </Text>
            </Box>

            {/* Order Items */}
            <Box borderBottom='1px' py='6' borderColor='gray.300'>
              <Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
                Order Items
              </Heading>
              <Box>
                {order.orderItems.length === 0 ? (
                  <Message>No Order Info</Message>
                ) : (
                  <Box py='2'>
                    {order.orderItems.map((item, idx) => (
                      <Flex
                        key={idx}
                        alignItems='center'
                        justifyContent='space-between'>
                        <Flex py='2' alignItems='center'>
                          <Image
                            src={item.image}
                            alt={item.name}
                            w='12'
                            h='12'
                            objectFit='cover'
                            mr='6'
                          />
                          <Link
                            fontWeight='bold'
                            fontSize='xl'
                            as={RouterLink}
                            to={`/products/${item.product}`}>
                            {item.name}
                          </Link>
                        </Flex>

                        <Text fontSize='lg' fontWeight='semibold'>
                          {item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
                        </Text>
                      </Flex>
                    ))}
                  </Box>
                )}
              </Box>
            </Box>
            {/* Available Couriers */}
            {
              !order.isPaid ? (

                <Box borderBottom="1px" py="6" borderColor="gray.300">
                  <Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
                    Available Couriers
                  </Heading>
                  {loadingCouriers ? (
                    <Loader />
                  ) : errorCouriers ? (
                    <Message type="error">{errorCouriers}</Message>
                  ) : availableCouriers && availableCouriers.length > 0 ? (
                    <Box>
                      <Select
                        placeholder="Select a courier"
                        value={selectedCourier?.courier_name || ''}
                        onChange={handleCourierChange}
                      >
                        {availableCouriers.map((courier, index) => (
                          <option key={index} value={courier.courier_name}>
                            {courier.courier_name + ' ' + courier.rate}
                          </option>
                        ))}
                      </Select>
                    </Box>
                  ) : (
                    <Message type="info">No couriers available.</Message>
                  )}
                </Box>
              ) : (
                <Box mt='4'>
                  <Text fontSize='xl'>
                    <strong>Estimated Delivery Time: </strong>
                    {estimatedDelivery}
                  </Text>
                </Box>

              )}
          </Flex>

          {/* Column 2 */}
          <Flex
            direction='column'
            bgColor='white'
            justifyContent='space-between'
            py='8'
            px='8'
            shadow='md'
            rounded='lg'
            borderColor='gray.300'>
            <Box>
              <Heading mb='6' as='h2' fontSize='3xl' fontWeight='bold'>
                Order Summary
              </Heading>

              {/* Items Price */}
              <Flex
                borderBottom='1px'
                py='2'
                borderColor='gray.200'
                alignitems='center'
                justifyContent='space-between'>
                <Text fontSize='xl'>Items</Text>
                <Text fontWeight='bold' fontSize='xl'>
                  ₹{order.itemsPrice}
                </Text>
              </Flex>

              {/* Shipping Price */}
              <Flex
                borderBottom='1px'
                py='2'
                borderColor='gray.200'
                alignitems='center'
                justifyContent='space-between'>
                <Text fontSize='xl'>Shipping</Text>
                <Text fontWeight='bold' fontSize='xl'>
                  ₹{selectedCourier ? selectedCourier.rate : order.shippingPrice}
                </Text>
              </Flex>

              {/* Tax Price */}
              <Flex
                borderBottom='1px'
                py='2'
                borderColor='gray.200'
                alignitems='center'
                justifyContent='space-between'>
                <Text fontSize='xl'>Tax</Text>
                <Text fontWeight='bold' fontSize='xl'>
                  ₹{order.taxPrice}
                </Text>
              </Flex>

              {/* Total Price */}
              <Flex
                borderBottom='1px'
                py='2'
                borderColor='gray.200'
                alignitems='center'
                justifyContent='space-between'>
                <Text fontSize='xl'>Total</Text>
                <Text fontWeight='bold' fontSize='xl'>
                  ₹{calculateTotalPrice}
                </Text>
              </Flex>
            </Box>

            {/* PAYMENT BUTTON */}
            {!order.isPaid && (
              <Box>
                {loadingRazorPay ? (
                  <Loader />
                ) : (
                  <Box><Button type='button' colorScheme='blue' onClick={() => handlePayment(calculateTotalPrice)}>RazorPay</Button></Box>
                )}
              </Box>
            )}

            {/* Order Deliver Button */}
            {loadingDeliver && <Loader />}
            {userInfo && userInfo.isAdmin && order.isPaid && !order.isDelivered && (
              <Button type='button' colorScheme='teal' onClick={deliverHandler}>Mark as Delivered</Button>
            )}

						{/* Download Invoice Button */}
            {

              order.isPaid && (
                <Button type='button' colorScheme='teal' onClick={() => invoiceHandler(order.shiprocketOrderId)}>Download Invoice</Button>

              )
            }
          </Flex>
        </Grid>
      </Flex>
    </>
  );
}

export default RazorPayScreen