import {
    Button,
    Checkbox,
    Flex,
    FormControl,
    FormLabel,
    Heading,
    Link,
    Spacer,
    Input,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link as RouterLink, useNavigate, useParams } from 'react-router-dom';
import { updateUser, getUser } from '../actions/userActions';
import FormContainer from '../components/FormContainer';
import Loader from '../components/Loader';
import Message from '../components/Message';
import { USER_UPDATE_RESET } from '../constants/userConstants';

const UserEditScreen = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { id: userId } = useParams();
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [isAdmin, setIsAdmin] = useState(false);

    const userDetails = useSelector((state) => state.userDetails);
    const { loading, error, user } = userDetails;

    const userUpdate = useSelector((state) => state.userUpdate);
    const {
        loading: loadingUpdate,
        error: errorUpdate,
        success: successUpdate,
    } = userUpdate;

    useEffect(() => {
        if (successUpdate) {
            dispatch({ type: USER_UPDATE_RESET });
            navigate('/admin/userlist');
        } else if (!user.name || user._id !== userId) {
            dispatch(getUser(userId));

        } else {
            setName(user.name);
            setEmail(user.email);
            setIsAdmin(user.isAdmin);
        }

    }, [user, dispatch, navigate, userId, successUpdate]);

    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(updateUser({ _id: userId, name, email, isAdmin }));
    }
    return (
        <>
            <Link as={RouterLink} to='/admin/userlist'>
                Go Back
            </Link>
            <Flex w={'full'} alignItems={'center'} justifyContent={'center'} py='5'>
                <FormContainer>
                    <Heading as={'h1'} mb='8' fontSize={'3xl'}>
                        Edit User
                    </Heading>
                    {loadingUpdate && <Loader />}
                    {errorUpdate && <Message type='error'>{errorUpdate}</Message>}

                    {loading ? (
                        <Loader></Loader>
                    ) : error ? (
                        <Message type='error'>{error}</Message>
                    ) : (
                        <form onSubmit={submitHandler}>
                            <FormControl id='name' isRequired>
                                <FormLabel>Name</FormLabel>
                                <Input type='text' placeholder='Enter full name' value={name} onChange={(e) => setName(e.target.value)}>

                                </Input>
                            </FormControl>
                            <Spacer h='3'></Spacer>

                            <FormControl id='email' isRequired>
                                <FormLabel>Email</FormLabel>
                                <Input type='email' placeholder='Enter email address' value={email} onChange={(e) => setEmail(e.target.value)}>

                                </Input>
                            </FormControl>
                            <Spacer h='3'></Spacer>

                            <FormControl id='isAdmin' isRequired>
                                <FormLabel>Is Admin</FormLabel>
                                {/* {
                            user.isAdmin ? 
                            <Checkbox size='lg'colorScheme='teal' defaultChecked>Is Admin?</Checkbox>:
                            <Checkbox size='lg'colorScheme='teal' value={isAdmin} onChange={(e)=>e.target.value}>Is Admin?</Checkbox>
                            
                        } */}

                                <Checkbox size='lg' colorScheme='teal' isChecked={isAdmin} onChange={(e) => e.target.checked}>Is Admin?</Checkbox>


                            </FormControl>
                            <Spacer h='3'></Spacer>

                            <Button type='submit' isLoading={loading} colorScheme='teal' mt='4'>Update</Button>
                        </form>
                    )}
                </FormContainer>
            </Flex>
        </>
    )
}

export default UserEditScreen