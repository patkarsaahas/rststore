import React, { useEffect } from 'react'
import {Box, Heading} from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { ordersTotalSales } from '../actions/orderActions';
import Loader from '../components/Loader';
import Message from '../components/Message';
const TotalSalesScreen = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const orderTotalSales = useSelector((state)=> state.orderTotalSales);
    const {loading, totalsales, error} = orderTotalSales;

    const userLogin = useSelector((state)=>state.userLogin)
    const {userInfo} = userLogin;

    useEffect(() => {
        if (userInfo && userInfo.isAdmin) {
            dispatch(ordersTotalSales());
        }
        else {
            navigate('/login')
        }
    }, [
        dispatch,
        navigate,
        userInfo,

    ])
  return (
    <>
    {
        loading ? (
            <Loader></Loader>
        ) : error ? (
            <Message type='error'>{error}</Message>
        ) : (
            <Box w={'fit-content'} h={'150px'} boxShadow='dark-lg' p='6' rounded='md' bg='white' ml={{base:'auto', md:'auto'}} mr={{base:'auto', md:'auto'}}>
                <Heading as={'h1'}>ToTal Sales: ₹ {totalsales.totalSales}</Heading>
            </Box>

        )
    }
    </>
    
  )
}

export default TotalSalesScreen