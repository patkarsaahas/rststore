import { Box, Flex, Grid, Heading, Image, Link, Text, Button, Select } from '@chakra-ui/react';
import { PayPalButtons, PayPalScriptProvider } from '@paypal/react-paypal-js';
import { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link as RouterLink, useParams } from 'react-router-dom';
import { deliverOrder, getOrderDetails, payOrder } from '../actions/orderActions';
import Loader from '../components/Loader';
import Message from '../components/Message';
import { ORDER_DELIVER_RESET, ORDER_PAY_RESET } from '../constants/orderConstants';
import { fetchAvailableCouriers, generateAWB } from '../actions/courierActions';
import { GetToken } from '../shiprocket';
import axios from 'axios';

const OrderScreen = () => {
	const dispatch = useDispatch();
	const { id: orderId } = useParams();
	const [selectedCourier, setSelectedCourier] = useState(null);
	const [estimatedDelivery, setEstimatedDelivery] = useState('');
	// const [invoiceUrl, setInvoiceUrl] = useState('');
	// const [isInvoiceCreated, setIsInvoiceCreated] = useState(false);

	const orderDetails = useSelector((state) => state.orderDetails);
	const { order, loading, error } = orderDetails;

	const availableCouriersState = useSelector((state) => state.availableCouriers);
	const { loading: loadingCouriers, error: errorCouriers, availableCouriers } = availableCouriersState;

	const orderPay = useSelector((state) => state.orderPay);
	const { loading: loadingPay, success: successPay } = orderPay;

	const orderDeliver = useSelector((state) => state.orderDeliver);
	const { loading: loadingDeliver, success: successDeliver } = orderDeliver;

	const userLogin = useSelector((state) => state.userLogin);
	const { userInfo } = userLogin;

	const handleCourierChange = (e) => {
		const selectedCourier = availableCouriers.find(courier => courier.courier_name === e.target.value);
		setSelectedCourier(selectedCourier);
		if (selectedCourier) {
			setEstimatedDelivery(selectedCourier.etd); // Assuming the ETD is part of the courier object
			localStorage.setItem('selectedCourier', JSON.stringify(selectedCourier));
		}
	};

	const calculateTotalPrice = useMemo(() => {
		return (
			order?.itemsPrice +
			(selectedCourier ? selectedCourier.rate : order?.shippingPrice) +
			order?.taxPrice
		);
	}, [order, selectedCourier]);

	useEffect(() => {
		dispatch({ type: ORDER_PAY_RESET });
		dispatch({ type: ORDER_DELIVER_RESET });

		if (!order || successPay || successDeliver || order._id !== orderId) {
			dispatch(getOrderDetails(orderId));
		} else if (order.shippingAddress) {
			const params = {
				pickup_postcode: '400068',
				delivery_postcode: order.shippingAddress.postalCode,
				weight: 2,
				cod: order.paymentMethod === 'COD' ? 1 : 0,
			};

			const storedCourierInfo = localStorage.getItem('courierInfo');
			const storedSelectedCourier = localStorage.getItem('selectedCourier');

			if (storedCourierInfo) {
				const storedCouriers = JSON.parse(storedCourierInfo).data.available_courier_companies;
				dispatch({
					type: 'FETCH_AVAILABLE_COURIERS_SUCCESS',
					payload: storedCouriers,
				});
			} else {
				dispatch(fetchAvailableCouriers(params));
			}

			if (storedSelectedCourier) {
				const selectedCourier = JSON.parse(storedSelectedCourier);
				setSelectedCourier(selectedCourier);
				setEstimatedDelivery(selectedCourier.etd);
			}
		}
		else {
			console.log('Order Details', order);
		}
	}, [dispatch, orderId, order, successPay, successDeliver]);

	console.log('Order Details', order);

	const successPaymentHandler = async (paymentResult) => {
		dispatch(payOrder(orderId, paymentResult));

		try {
			const token = await GetToken({
				email: 'patkarsaahas@gmail.com',
				password: 'tempUse@486',
			});

			if (order.shiprocketShipmentId) {
				console.log('Generating AWB for shipment ID:', order.shiprocketShipmentId);

				// Call the generateAWB function
				const awbData = await generateAWB(order.shiprocketShipmentId);

				// Check if AWB was successfully assigned
				if (awbData.awb_assign_status === 1) {
					console.log('AWB Assigned:', awbData);
					// const awbNumber = awbData.response?.data?.awb_code || awbData.awb_number;
					// console.log('Generated AWB Number:', awbNumber);

					const confirmShipmentPayload = {
						shipment_id: order.shiprocketShipmentId,
					};

					// Proceed to confirm shipment
					const confirmShipmentResponse = await axios.post(
						'https://apiv2.shiprocket.in/v1/external/courier/generate/pickup',
						confirmShipmentPayload,
						{
							headers: {
								'Content-Type': 'application/json',
								Authorization: `Bearer ${token}`,
							},
						}
					);

					console.log('Shipment Confirmed:', confirmShipmentResponse.data);
				} else {
					console.error('AWB Assignment failed:', awbData.message);
				}
			}
		} catch (error) {
			console.error('Error confirming shipment:', error.response ? error.response.data : error.message);
		}
	};

	const deliverHandler = () => dispatch(deliverOrder(order));

	const invoiceHandler = async (shiprocketOrderId) => {
		try {
			const token = await GetToken({
				email: 'patkarsaahas@gmail.com',
				password: 'tempUse@486',
			})

			const invoiceData = { ids: [shiprocketOrderId] };
			const invoiceGenerate = await axios.post('https://apiv2.shiprocket.in/v1/external/orders/print/invoice', invoiceData, {
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
			});
			console.log('Invoice Generated:', invoiceGenerate.data);

			const { is_invoice_created, invoice_url } = invoiceGenerate.data;

			if (is_invoice_created) {
				// setInvoiceUrl(invoice_url); //Save the URL state
				// alert(`Invoice genrated!😊 You can download it here: ${invoice_url}`)
				// window.open(invoice_url, '_blank'); //open url in new tab
				const link = document.createElement('a');
				link.href = invoice_url;
				link.download = 'invoice.pdf'; // Optional: Provide a filename
				link.click();
			} else {
				alert('Invoice could not be generated');
			}

		} catch (error) {
			console.error('Failed to generate invoice:', error.response?.data || error.message);
			alert('Failed to generate invoice. Please try again.');

		}
	}

	console.log("Available Couriers State:", availableCouriersState);
	return loading ? (
		<Loader />
	) : error ? (
		<Message type='error'>{error}</Message>
	) : (
		<>
			<h1>Order ID:  {order._id}</h1>
			{order.shiprocketShipmentId && (
				<div>Shipment ID: {order.shiprocketShipmentId}</div>
			)}
			<Flex w='full' py='5' direction='column'>
				<Grid templateColumns={{ base: "1fr", md: '3fr 2fr' }} gap='20'>
					{/* Column 1 */}
					<Flex direction='column'>
						{/* Shipping */}
						<Box borderBottom='1px' py='6' borderColor='gray.300'>
							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
								Shipping
							</Heading>
							<Text>
								Name: <strong>{order.user.name}</strong>
							</Text>
							<Text>
								Email:{' '}
								<strong>
									<a href={`mailto:${order.user.email}`}>{order.user.email}</a>
								</strong>
							</Text>
							<Text>
								<strong>Address: </strong>
								{order.shippingAddress.address}, {order.shippingAddress.city},{' '}
								{order.shippingAddress.postalCode},{' '}
								{order.shippingAddress.country}
							</Text>
							<Text>
								<strong>Phone No: </strong>
								{order.shippingAddress.phone}
							</Text>
							<Text mt='4'>
								{order.isDelivered ? (
									<Message type='success'>
										Delivered on {order.deliveredAt}
									</Message>
								) : (
									<Message type='warning'>Not Delivered</Message>
								)}
							</Text>
						</Box>

						{/* Payment Method */}
						<Box borderBottom='1px' py='6' borderColor='gray.300'>
							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
								Payment Method
							</Heading>
							<Text>
								<strong>Method: </strong>
								{order.paymentMethod?.toUpperCase()}
							</Text>
							<Text mt='4'>
								{order.isPaid ? (
									<Message type='success'>Paid on {order.paidAt}</Message>
								) : (
									<Message type='warning'>Not Paid</Message>
								)}
							</Text>
						</Box>

						{/* Order Items */}
						<Box borderBottom='1px' py='6' borderColor='gray.300'>
							<Heading as='h2' mb='3' fontSize='2xl' fontWeight='semibold'>
								Order Items
							</Heading>
							<Box>
								{order.orderItems.length === 0 ? (
									<Message>No Order Info</Message>
								) : (
									<Box py='2'>
										{order.orderItems.map((item, idx) => (
											<Flex
												key={idx}
												alignItems='center'
												justifyContent='space-between'>
												<Flex py='2' alignItems='center'>
													<Image
														src={item.image}
														alt={item.name}
														w='12'
														h='12'
														objectFit='cover'
														mr='6'
													/>
													<Link
														fontWeight='bold'
														fontSize='xl'
														as={RouterLink}
														to={`/products/${item.product}`}>
														{item.name}
													</Link>
												</Flex>

												<Text fontSize='lg' fontWeight='semibold'>
													{item.qty} x ₹{item.price} = ₹{+item.qty * item.price}
												</Text>
											</Flex>
										))}
									</Box>
								)}
							</Box>
						</Box>

						{/* Available Couriers */}
						{
							!order.isPaid ? (

								<Box borderBottom="1px" py="6" borderColor="gray.300">
									<Heading as="h2" mb="3" fontSize="2xl" fontWeight="semibold">
										Available Couriers
									</Heading>
									{loadingCouriers ? (
										<Loader />
									) : errorCouriers ? (
										<Message type="error">{errorCouriers}</Message>
									) : availableCouriers && availableCouriers.length > 0 ? (
										<Box>
											<Select
												placeholder="Select a courier"
												value={selectedCourier?.courier_name || ''}
												onChange={handleCourierChange}
											>
												{availableCouriers.map((courier, index) => (
													<option key={index} value={courier.courier_name}>
														{courier.courier_name + ' ' + courier.rate}
													</option>
												))}
											</Select>
										</Box>
									) : (
										<Message type="info">No couriers available.</Message>
									)}
								</Box>
							) : (
								<Box mt='4'>
									<Text fontSize='xl'>
										<strong>Estimated Delivery Time: </strong>
										{estimatedDelivery}
									</Text>
								</Box>

							)}

					</Flex>

					{/* Column 2 */}
					<Flex
						direction='column'
						bgColor='white'
						justifyContent='space-between'
						py='8'
						px='8'
						shadow='md'
						rounded='lg'
						borderColor='gray.300'>
						<Box>
							<Heading mb='6' as='h2' fontSize='3xl' fontWeight='bold'>
								Order Summary
							</Heading>

							{/* Items Price */}
							<Flex
								borderBottom='1px'
								py='2'
								borderColor='gray.200'
								alignitems='center'
								justifyContent='space-between'>
								<Text fontSize='xl'>Items</Text>
								<Text fontWeight='bold' fontSize='xl'>
									₹{order.itemsPrice}
								</Text>
							</Flex>

							{/* Shipping Price */}
							<Flex
								borderBottom='1px'
								py='2'
								borderColor='gray.200'
								alignitems='center'
								justifyContent='space-between'>
								<Text fontSize='xl'>Shipping</Text>
								<Text fontWeight='bold' fontSize='xl'>
									₹{selectedCourier ? selectedCourier.rate : order.shippingPrice}
								</Text>
							</Flex>

							{/* Tax Price */}
							<Flex
								borderBottom='1px'
								py='2'
								borderColor='gray.200'
								alignitems='center'
								justifyContent='space-between'>
								<Text fontSize='xl'>Tax</Text>
								<Text fontWeight='bold' fontSize='xl'>
									₹{order.taxPrice}
								</Text>
							</Flex>

							{/* Total Price */}
							<Flex
								borderBottom='1px'
								py='2'
								borderColor='gray.200'
								alignitems='center'
								justifyContent='space-between'>
								<Text fontSize='xl'>Total</Text>
								<Text fontWeight='bold' fontSize='xl'>
									₹{calculateTotalPrice}
								</Text>
							</Flex>


						</Box>

						{/* PAYMENT BUTTON */}
						{!order.isPaid && (
							<Box>
								{loadingPay ? (
									<Loader />
								) : (
									<PayPalScriptProvider
										options={{
											clientId:
												'AUovBO90owBawRxVyoBse0wH_X0K-k_NbScKdEK1ULHjvn3QI2QJgVnoLb11FFYyG69iFdDcNhcGIkjL',
											components: 'buttons',
										}}>
										<PayPalButtons
											createOrder={(data, actions) => {
												const totalPrice = calculateTotalPrice.toFixed(2);
												return actions.order.create({
													purchase_units: [
														{
															amount: {
																value: totalPrice,
															},
														},
													],
												});
											}}
											onApprove={(data, actions) => {
												return actions.order.capture().then((details) => {
													console.log(details);
													const paymentResult = {
														id: details.id,
														status: details.status,
														update_time: details.update_time,
														email_address: details.email_address,
													};
													successPaymentHandler(paymentResult);
												});
											}}
										/>
									</PayPalScriptProvider>
								)}
							</Box>
						)}

						{/* Order Deliver Button */}
						{loadingDeliver && <Loader />}
						{userInfo && userInfo.isAdmin && order.isPaid && !order.isDelivered && (
							<Button type='button' colorScheme='teal' onClick={deliverHandler}>Mark as Delivered</Button>
						)}

						{/* Download Invoice Button */}

						{

							order.isPaid && (
								<Button type='button' colorScheme='teal' onClick={() => invoiceHandler(order.shiprocketOrderId)}>Download Invoice</Button>

							)
						}

						{/* {invoiceUrl && (
							<Box>
								<Heading as='h3'>Invoice Generated</Heading>
								<Button type='button' colorScheme='blue'><Link as={RouterLink} href={invoiceUrl} target="_blank" rel="noopener noreferrer" _hover={{ textDecor: 'none' }}>
									Download Invoice
								</Link></Button>
							</Box>
						)} */}


					</Flex>
				</Grid>
			</Flex>
		</>
	);
};

export default OrderScreen;