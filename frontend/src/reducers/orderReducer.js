import {
	ORDER_CREATE_FAIL,
	ORDER_CREATE_REQUEST,
	ORDER_CREATE_SUCCESS,
	ORDER_DETAILS_FAIL,
	ORDER_DETAILS_REQUEST,
	ORDER_DETAILS_SUCCESS,
	ORDER_PAY_FAIL,
	ORDER_PAY_REQUEST,
	ORDER_PAY_RESET,
	ORDER_PAY_SUCCESS,
	ORDER_LIST_REQUEST,
	ORDER_LIST_SUCCESS,
	ORDER_LIST_FAIL,
	ORDER_LIST_RESET,
	ORDER_ADMIN_REQUEST,
	ORDER_ADMIN_SUCCESS,
	ORDER_ADMIN_FAIL,
	ORDER_DELIVER_REQUEST,
	ORDER_DELIVER_SUCCESS,
	ORDER_DELIVER_FAIL,
	ORDER_DELIVER_RESET,
	ORDER_TOTAL_SALES_REQUEST,
	ORDER_TOTAL_SALES_SUCCESS,
	ORDER_TOTAL_SALES_FAIL,
	ORDER_RAZOR_CREATE_REQUEST,
	ORDER_RAZOR_CREATE_SUCCESS,
	ORDER_RAZOR_CREATE_FAIL,
	ORDER_RAZORPAY_REQUEST,
	ORDER_RAZORPAY_SUCCESS,
	ORDER_RAZORPAY_FAIL,
	ORDER_RAZORPAY_RESET,
	ORDER_UPDATE_SHIPROCKET_ID_REQUEST,
	ORDER_UPDATE_SHIPROCKET_ID_SUCCESS,
	ORDER_UPDATE_SHIPROCKET_ID_FAIL,
	ORDER_CANCEL_REQUEST,
	ORDER_CANCEL_SUCCESS,
	ORDER_CANCEL_FAIL,
	ORDER_CANCEL_RESET
} from '../constants/orderConstants';

export const orderCreateReducer = (state = {}, action) => {
	switch (action.type) {
		case ORDER_CREATE_REQUEST:
			return { loading: true };
		case ORDER_CREATE_SUCCESS:
			return { loading: false, success: true, order: action.payload };
		case ORDER_CREATE_FAIL:
			return { loading: false, error: action.payload };
		default:
			return state;
	}
};

export const orderCancelReducer = (state = {}, action) => {
	switch (action.type) {
		case ORDER_CANCEL_REQUEST:
			return { loading: true };
		case ORDER_CANCEL_SUCCESS:
			return { loading: false, success: true, order: action.payload };
		case ORDER_CANCEL_FAIL:
			return { loading: false, error: action.payload };
		case ORDER_CANCEL_RESET:
			return {};
		default:
			return state;
	}
};

export const orderRazorPayCreateReducer = (state = {}, action) => {
	switch (action.type) {
		case ORDER_RAZOR_CREATE_REQUEST:
			return { loading: true };
		case ORDER_RAZOR_CREATE_SUCCESS:
			return { loading: false, success: true, order: action.payload };
		case ORDER_RAZOR_CREATE_FAIL:
			return { loading: false, error: action.payload };
		default:
			return state;
	}
};

export const orderDetailsReducer = (state = { order: { orderItems: [], shippingAddress: {}, user: {} } }, action) => {
	switch (action.type) {
		case ORDER_DETAILS_REQUEST:
			return { ...state, loading: true };
		case ORDER_DETAILS_SUCCESS:
			return { loading: false, success: true, order: action.payload };
		case ORDER_DETAILS_FAIL:
			return { loading: false, error: action.payload };
		default:
			return state;
	}
}

export const orderTotalSalesReducer = (state = { totalsales: {} }, action) => {
	switch (action.type) {
		case ORDER_TOTAL_SALES_REQUEST:
			return { ...state, loading: true };
		case ORDER_TOTAL_SALES_SUCCESS:
			return { loading: false, success: true, totalsales: action.payload };
		case ORDER_TOTAL_SALES_FAIL:
			return { loading: false, error: action.payload };
		default:
			return state;
	}
}

export const orderPayReducer = (state = {}, action) => {
	switch (action.type) {
		case ORDER_PAY_REQUEST:
			return { ...state, loading: true };
		case ORDER_PAY_SUCCESS:
			return { loading: false, success: true };
		case ORDER_PAY_FAIL:
			return { loading: false, error: action.payload };
		case ORDER_PAY_RESET:
			return {};
		default:
			return state;
	}
}

export const orderRazorPayReducer = (state = {}, action) => {
	switch (action.type) {
		case ORDER_RAZORPAY_REQUEST:
			return { ...state, loading: true };
		case ORDER_RAZORPAY_SUCCESS:
			return { loading: false, success: true };
		case ORDER_RAZORPAY_FAIL:
			return { loading: false, error: action.payload };
		case ORDER_RAZORPAY_RESET:
			return {};
		default:
			return state;
	}
}


export const orderListReducer = (state = { orders: [] }, action) => {
	switch (action.type) {
		case ORDER_LIST_REQUEST:
			return { ...state, loading: true };
		case ORDER_LIST_SUCCESS:
			return { loading: false, orders: action.payload };
		case ORDER_LIST_FAIL:
			return { loading: false, error: action.payload };
		case ORDER_LIST_RESET:
			return { orders: [] };
		default:
			return state;
	}
}

export const orderAdminListReducer = (state = { orders: [] }, action) => {
	switch (action.type) {
		case ORDER_ADMIN_REQUEST:
			return { ...state, loading: true };
		case ORDER_ADMIN_SUCCESS:
			return { loading: false, orders: action.payload };
		case ORDER_ADMIN_FAIL:
			return { loading: false, error: action.payload };
		default:
			return state;
	}
}

export const orderDeliverReducer = (state = {}, action) => {
	switch (action.type) {
		case ORDER_DELIVER_REQUEST:
			return { ...state, loading: true };
		case ORDER_DELIVER_SUCCESS:
			return { loading: false, orders: action.payload };
		case ORDER_DELIVER_FAIL:
			return { loading: false, error: action.payload };
		case ORDER_DELIVER_RESET:
			return {};
		default:
			return state;
	}
}

export const orderUpdateShiprocketIdReducer = (state = {}, action) => {
	switch (action.type) {
		case ORDER_UPDATE_SHIPROCKET_ID_REQUEST:
			return { loading: true };
		case ORDER_UPDATE_SHIPROCKET_ID_SUCCESS:
			return { loading: false, success: true, order: action.payload };
		case ORDER_UPDATE_SHIPROCKET_ID_FAIL:
			return { loading: false, error: action.payload };
		default:
			return state;
	}
};