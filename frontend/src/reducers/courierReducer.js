import { 
    FETCH_AVAILABLE_COURIERS_REQUEST, 
    FETCH_AVAILABLE_COURIERS_SUCCESS, 
    FETCH_AVAILABLE_COURIERS_FAIL 
  } from '../constants/shipConstants';
  
  export const availableCouriersReducer = (state = { availableCouriers: {} }, action) => {
    switch (action.type) {
      case FETCH_AVAILABLE_COURIERS_REQUEST:
        return { loading: true, ...state };
      case FETCH_AVAILABLE_COURIERS_SUCCESS:
        return { loading: false, availableCouriers: action.payload };
      case FETCH_AVAILABLE_COURIERS_FAIL:
        return { loading: false, error: action.payload };
      default:
        return state;
    }
  };