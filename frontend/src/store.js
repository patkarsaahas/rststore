import {applyMiddleware, combineReducers, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {productCreateReducer, productDeleteReducer, productDetailsReducer, productListReducer, productMensListReducer, productReviewCreateReducer, productUpdateReducer, productWomensListReducer} from './reducers/productReducer'
import { cartReducer } from './reducers/cartReducer';
import { getUserReducer, userDeleteReducer, userDetailsReducer, userListReducer, userLoginReducer, userRegisterReducer, userUpdateProfileReducer, userUpdateReducer } from './reducers/userReducer';
import { orderAdminListReducer, orderCancelReducer, orderCreateReducer, orderDeliverReducer, orderDetailsReducer, orderListReducer, orderPayReducer, orderRazorPayCreateReducer, orderRazorPayReducer, orderTotalSalesReducer, orderUpdateShiprocketIdReducer,  } from './reducers/orderReducer';//orderRazorPayCreateReducer
import { availableCouriersReducer } from './reducers/courierReducer';
const reducer = combineReducers({
    productList : productListReducer,
    productMensList: productMensListReducer,
    productWomensList: productWomensListReducer,
    productDetails: productDetailsReducer,
    productDelete: productDeleteReducer,
    productCreate: productCreateReducer,
    productUpdate: productUpdateReducer,
    productReviewCreate: productReviewCreateReducer,
    cart: cartReducer,
    userLogin: userLoginReducer,
    userRegister: userRegisterReducer,
    userDetails: userDetailsReducer,
    userUpdateProfile: userUpdateProfileReducer,
    userList: userListReducer,
    userDelete: userDeleteReducer,
    userUpdate: userUpdateReducer,
    getUserfromAdmin: getUserReducer,
    orderCreate: orderCreateReducer,
    orderCancel: orderCancelReducer,
    orderCreateRazor: orderRazorPayCreateReducer,
    orderDetails: orderDetailsReducer,
    orderPay: orderPayReducer,
    orderRazorpay: orderRazorPayReducer,
    orderMyList: orderListReducer,
    orderAdminList: orderAdminListReducer,
    orderDeliver: orderDeliverReducer,
    orderTotalSales: orderTotalSalesReducer,
    availableCouriers:availableCouriersReducer,
    updateShipRocketId: orderUpdateShiprocketIdReducer,
});

const cartItemsFromStorage = localStorage.getItem('cartItems')
? JSON.parse(localStorage.getItem('cartItems')) : [];

const userInfoItemFromStorage = localStorage.getItem('userInfo')
? JSON.parse(localStorage.getItem('userInfo')) : null;

const shippingAddressFromStorage = localStorage.getItem('shippingAddress') 
? JSON.parse(localStorage.getItem('shippingAddress')) : {};

const paymentMethodFromStorage = localStorage.getItem('paymentMethod')
? JSON.parse(localStorage.getItem('paymentMethod')) : 'paypal';

const courierFromStorage = localStorage.getItem('courierInfo')
? JSON.parse(localStorage.getItem('courierInfo')) : [];



const initialState = {
    cart: {cartItems: cartItemsFromStorage, shippingAddress:shippingAddressFromStorage, paymentMethod:paymentMethodFromStorage},
    userLogin: {userInfo: userInfoItemFromStorage}, availableCouriers:{courierInfo: courierFromStorage}
};

const middlewares = [thunk];

const store = createStore(
    reducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middlewares))
)

export default store;