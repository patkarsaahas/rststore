import { Flex } from "@chakra-ui/react";
import Footer from "./components/Footer";
import Header from "./components/Header";
import HomeScreen from "./screens/HomeScreen";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ProductScreen from "./screens/ProductScreen";
import CartScreen from "./screens/CartScreen";
import LoginScreen from "./screens/LoginScreen";
import RegisterScreen from "./screens/RegisterScreen";
import ProfileScreen from "./screens/ProfileScreen";
import ShippingScreen from "./screens/ShippingScreen";
import PaymentScreen from "./screens/PaymentScreen";
import PlaceOrderScreen from "./screens/PlaceOrderScreen";
import UserListScreen from "./screens/UserListScreen";
import UserEditScreen from "./screens/UserEditScreen";
import ProductListScreen from "./screens/ProductListScreen";
import ProductEditScreen from "./screens/ProductEditScreen";
import OrderListScreen from "./screens/OrderListScreen";
import MenScreen from "./screens/MenScreen";
import OrderScreen from "./screens/OrderScreen";
// import RazorPayScreen from "./screens/RazorPayScreen";
import TotalSalesScreen from "./screens/TotalSalesScreen";
import RazorPayScreen from "./screens/RazorPayScreen";
import NewHomeScreen from "./screens/NewHomeScreen";
import WomenScreen from "./screens/WomenScreen";
// import Example from "./screens/Example";
// import ThemeButton from "./screens/ThemeButton";
// import Reducer from "./screens/Reducer";
// import CallBack from "./screens/CallBack";
// import Ref from "./screens/Ref";

function App() {
  return (
    <>
      <BrowserRouter>
        <Header/>
        <Flex
				as='main'
				mt='72px'
				direction='column'
				py='6'
				px='6'
				bgColor='gray.200'>
          <Routes>
            <Route path='/' element={<NewHomeScreen/>}/>
            <Route path='/products' element={<HomeScreen/>}/>
            <Route path='/products/men/fashion' element={<MenScreen/>}/>
            <Route path='/products/women/fashion' element={<WomenScreen/>}/>
            <Route path='/product/:id' element={<ProductScreen/>}/>
            <Route path='/cart' element={<CartScreen/>}/>
            <Route path='/cart/:id' element={<CartScreen/>}/>
            <Route path='/login' element={<LoginScreen/>}/>
            <Route path='/register' element={<RegisterScreen/>}/>
            <Route path='/profile' element={<ProfileScreen/>}/>
            <Route path='/shipping' element={<ShippingScreen/>}/>
            <Route path='/payment' element={<PaymentScreen/>}/>
            <Route path='/placeorder' element={<PlaceOrderScreen/>}/>
            <Route path='/order/:id' element={<OrderScreen/>}/>
            <Route path='/razorpay/order/:id' element={<RazorPayScreen/>}/>
            <Route path='/admin/userlist' element={<UserListScreen/>}/>
            <Route path='/admin/user/:id/edit' element={<UserEditScreen/>}/>
            <Route path='/admin/productlist' element={<ProductListScreen/>}/>
            <Route path='/admin/product/:id/edit' element={<ProductEditScreen/>}/>
            <Route path='/admin/orderlist' element={<OrderListScreen/>}/>
            <Route path='/admin/totalsales' element={<TotalSalesScreen/>}/>
          </Routes>
			</Flex>
        <Footer/>
      </BrowserRouter>
    </>
  );
}

export default App;
