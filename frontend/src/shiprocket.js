import axios from "axios";
const Utils = require("./utils");

///  Get Token
export async function GetToken({ email, password }) {
  const token = await Utils.GetToken(email, password);
  return token;
}

//// Get Tracking Data through Order ID
export async function Tracking_OrderId({ auth, params }) {
  if (!auth.email && !auth.password && !auth.token) {
    return console.log("pass the valid props");
  }

  if (!params) {
    return console.log("order_id params is required");
  }

  var token = auth.token;
  if (!token) {
    token = await Utils.GetToken(auth.email, auth.password);
  }

  const parameterGenerator = new URLSearchParams(params);
  const parameter = parameterGenerator.toString();

  const url = `https://apiv2.shiprocket.in/v1/external/courier/track?${parameter}`;

  const myHeaders = {
    "Content-Type": "application/json",
    Authorization: `Bearer ${token}`,
  };

  const requestOptions = {
    url,
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };

  try {
    const response = await axios(requestOptions);
    const trackingDetails = await response.data;
    return trackingDetails;
  } catch (err) {
    console.log(err);
    const status = err.response.status
    return status
  }
}


///  Check Courier Serviceability
export async function CourierServiceability({ auth, params }) {
  if (!auth.email && !auth.password && !auth.token) {
    return console.log("pass the valid props");
  }

  var token = auth.token;
  if (!token) {
    token = await Utils.GetToken(auth.email, auth.password);
  }

  const parameterGenerator = new URLSearchParams(params);
  const parameter = parameterGenerator.toString();
  const url = `https://apiv2.shiprocket.in/v1/external/courier/serviceability/?${parameter}`;

  const myHeaders = {
    "Content-Type": "application/json",
    Authorization: `Bearer ${token}`,
  };

  const requestOptions = {
    url,
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };

  try {
    const response = await axios(requestOptions);
    if (response.data.status === "422") {
      return { error: "Invalid Pincode", availableCouriers: [] };
    }
    if (response.data.status === "404") {
      return { error: response.data.message, availableCouriers: [] };
    }

    const json = response.data;
    const availableCouriers = json.data.available_courier_companies.map(company => ({
      name: company.courier_name,
      etd: company.etd,
      rate: company.rate,
    }));
    return { availableCouriers };
  } catch (err) {
    console.log(err);
    const status = err.response ? err.response.status : 'Unknown error';
    return { error: status, availableCouriers: [] };
  }
}

