import { Box, Link, Image, Flex, Heading, Text } from '@chakra-ui/react'
import { Link as RouterLink } from 'react-router-dom'
import React from 'react'
// import Rating from './Rating'
const NewProductCard = ({ product }) => {
    return (
        <Link as={RouterLink}
            to={`/product/${product._id}`}
            _hover={{ textDecor: 'none' }}>
            <Box>
                <Image src={product.image}
                    alt={product.name}
                    w={{base:'100%',md:'fit-content'}}
                    h='372px'
                    objectFit='cover'
                />

                <Flex py={'5'} px={'4'} direction={'column'} justifyContent={'space-between'}>
                    <Heading as={'h4'} fontSize={'lg'} mb={'3'} fontWeight={'semi-bold'}>
                        {product.name}
                    </Heading>

                    <Flex alignItems={'center'} justifyContent={'space-between'}>
                        <Text fontSize='2xl' fontWeight='semi-bold' color='black.600'>₹{product.price}</Text>
                    </Flex>
                </Flex>
            </Box>

        </Link>
    )
}

export default NewProductCard