import { useState, useEffect } from 'react';

const Clock = () => {

    const [seconds, setSeconds] = useState(0);

    useEffect(() => {
        // Define a function to update the 'seconds' state every second
        const interval = setInterval(() => {
            setSeconds(seconds => seconds + 1);
        }, 1000);

        // Clean up the interval to prevent memory leaks
        return () => clearInterval(interval);
    }, []); // Empty dependency array means this effect runs only once
    return (
        <div>
            <p>Elapsed time: {seconds} seconds</p>
        </div>
    )
}

export default Clock