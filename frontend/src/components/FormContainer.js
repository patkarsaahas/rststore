import { Flex } from "@chakra-ui/react";

import React from 'react'

const FormContainer = ({children}) => {
  return (
    <Flex direction={'column'}
    boxShadow={'md'}
    rounded={'md'}
    bgColor={'white'}
    p={'10'}
    width={{base:'100%',md:'xl'}}
    >{children}</Flex>
  )
}

export default FormContainer