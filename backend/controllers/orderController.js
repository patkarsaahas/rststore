import axios from 'axios';
import Order from '../models/orderModel.js';
import Razorpay from 'razorpay';

const razorpayInstance = new Razorpay({
	key_id: "rzp_test_scVhdCqnABHXEi", // Your Razorpay API Key
	key_secret: "wGWnq30kZVolQCRIcBdqC3OZ", // Your Razorpay API Secret
});

/**
 * @desc		Create new order
 * @router	POST /api/orders
 * @access	private
 */
const createOrder = async (req, res) => {
	const {
		orderItems,
		shippingAddress,
		paymentMethod,
		itemsPrice,
		taxPrice,
		shippingPrice,
		totalPrice,
		shiprocketShipmentId,
	} = req.body;
	console.log('Request Body:', req.body); // Log the received data

	if (orderItems && orderItems.length === 0) {
		res.status(400);
		throw new Error('No order items');
	} else {
		const order = new Order({
			orderItems,
			user: req.user._id,
			shippingAddress,
			paymentMethod,
			itemsPrice,
			taxPrice,
			shippingPrice,
			totalPrice,
			shiprocketShipmentId,
		});

		const createdOrder = await order.save();
		res.status(201).json(createdOrder);
	}

};

/**
 * @desc		Delete the order created
 * @router	DELETE /api/orders
 * @access	private
 */

const deleteOrder = async (req, res) => {
	// const {
	// 	user,
	// 	orderItems,
	// } = req.body;

	// if (orderItems && orderItems.isPaid === false && user._id) {
	// 	const deleteOrder = Order.delete();
	// 	res.status(201).json(deleteOrder);

	// } else {
	// 	res.status(400);
	// 	throw new Error('No order fount to delete');
	// }


	try {
		const order = await Order.findById(req.params.id);

		if (!order) {
			res.status(400)
			throw new Error('Order not found')
		}

		// Ensure the order belongs to the user and is not paid yet
		if (order.user.toString() === req.user._id.toString() && order.isPaid === false) {
			await order.deleteOne();//Delete the order
			res.status(200).json({ message: 'Order removed sucessfully' })
		} else {
			res.status(400);
			throw new Erro('Order cannot be deleted')
		}
	} catch (error) {
		res.status(500)
		throw new Error(error.message)
	}
}
/**
 * @desc		Create new razorpay order
 * @router	POST /api/razorpay/orders/
 * @access	private
 */

// const createRazorOrder = async (req, res) => {
// 	const {
// 		orderItems,
// 		shippingAddress,
// 		paymentMethod,
// 		itemsPrice,
// 		taxPrice,
// 		shippingPrice,
// 		totalPrice,
// 	} = req.body;

// 	if (orderItems && orderItems.length === 0) {
// 		res.status(400);
// 		throw new Error('No order items');
// 	} else {
// 		const order = new Order({
// 			orderItems,
// 			user: req.user._id,
// 			shippingAddress,
// 			paymentMethod,
// 			itemsPrice,
// 			taxPrice,
// 			shippingPrice,
// 			totalPrice,
// 		});

// 		const createdRazorOrder = await order.save();
// 		res.status(201).json(createdRazorOrder);
// 	}
// }

const createRazorOrder = async (req, res) => {
	const {
		orderItems,
		shippingAddress,
		paymentMethod,
		itemsPrice,
		taxPrice,
		shippingPrice,
		totalPrice,
		shiprocketShipmentId,
	} = req.body;

	if (orderItems && orderItems.length === 0) {
		res.status(400);
		throw new Error('No order items');
	}

	// Generate a Razorpay order
	const options = {
		amount: Math.round(totalPrice) * 100,  // Amount is in paise, so multiply by 100
		currency: "INR",
		receipt: `receipt_order_${Date.now()}`,  // Unique order receipt ID
		notes: {
			shipping_address: shippingAddress,
		},
	};

	try {
		const razorpayOrder = await razorpayInstance.orders.create(options);
		console.log(`Razorpay order ID is: ${razorpayOrder.id}`);


		// Save order in DB
		const order = new Order({
			orderItems,
			user: req.user._id,
			shippingAddress,
			paymentMethod,
			itemsPrice,
			taxPrice,
			shippingPrice,
			totalPrice,
			razorpayOrderId: razorpayOrder.id,  // Save the Razorpay order ID
			shiprocketShipmentId,
		});

		const createdRazorOrder = await order.save();
		console.log("Saved Order:", createdRazorOrder);

		res.status(201).json({
			id: createdRazorOrder._id,
			razorpayOrderId: razorpayOrder.id,  // Send Razorpay order ID to frontend
		});

	} catch (error) {
		res.status(500).json({ message: "Error creating Razorpay order" });
	}
};



/**
 * @desc Get order by ID
 * @router GET/api/orders/:id
 * @access private
 */

const getOrderById = async (req, res) => {
	const order = await Order.findById(req.params.id).populate(
		'user',
		'name email'
	);

	if (order) {
		res.json(order);
	} else {
		res.status(404);
		throw new Error('Order not found');
	}
}

/**
 * @desc Update order to paid
 * @router PUT/api/orders/:id/pay
 * @access private
 */

const updateOrderToPaid = async (req, res) => {
	const order = await Order.findById(req.params.id);

	if (order) {
		order.isPaid = true;
		order.paidAt = Date.now();
		order.paymentResult = {
			id: req.body.id,
			state: req.body.state,
			update_time: req.body.update_time,
			email_address: req.body.email_address,
		};

		// order.razorpay_payment_id = {
		// 	id: req.body.id,
		// }

		// if (req.body.awb_code) {
		//     order.awb_code = req.body.awb_code;
		// }

		const updatedOrder = await order.save();
		res.json(updatedOrder);

	}
	else {
		res.status(404);
		throw new Error('Order not found');
	}
}

/**
 * @desc Update order to paid
 * @router PUT/api/razorpay/orders/:id/pay
 * @access private
 */

const updateRazorOrderToPaid = async (req, res) => {
	// const { id: razorpayPaymentId, status, email_address } = req.body.paymentResult;

	// try {
	// 	const order = await Order.findById(req.params.id);

	// 	if (order) {
	// 		order.isPaid = true;
	// 		order.paidAt = Date.now();
	// 		order.paymentResult = {
	// 			id: razorpayPaymentId, // Razorpay payment ID
	// 			status,
	// 			update_time: Date.now(),
	// 			email_address,
	// 		};

	// 		const updatedOrder = await order.save();
	// 		res.json(updatedOrder);
	// 	} else {
	// 		res.status(404);
	// 		throw new Error('Order not found');
	// 	}
	// } catch (error) {
	// 	res.status(500).json({ message: error.message });
	// }
	// const order = await Order.findById(req.params.id);

	// if (order) {
	// 	order.isPaid = true;
	// 	order.paidAt = Date.now();
	// 	order.paymentResult = {
	// 		id: req.body.id,
	// 		state: req.body.state,
	// 		update_time: req.body.update_time,
	// 		email_address: req.body.email_address,
	// 	};

	// 	order.razorpay_payment_id = {
	// 		id: req.body.id,
	// 	}

	// 	const updatedRazorOrder = await order.save();
	// 	console.log('Updated Razor Order: ', updatedRazorOrder);

	// 	res.json(updatedRazorOrder);

	// }
	// else {
	// 	res.status(404);
	// 	throw new Error('Order not found');
	// }

	try {
		console.log("Request Body:", req.body);
		if (!req.body.paymentResult) {
			return res.status(400).json({ message: "Payment result is missing in the request body" });
		}

		const { id, status, update_time, email_address } = req.body.paymentResult;

		const order = await Order.findById(req.params.id);
		if (!order) {
			return res.status(404).json({ message: "Order not found" });
		}

		order.isPaid = true;
		order.paidAt = Date.now();
		order.paymentResult = { id, status, update_time, email_address };

		const updatedOrder = await order.save();
		console.log("Updated Razorpay Order:", updatedOrder);

		res.json(updatedOrder);
	} catch (error) {
		console.error("Error updating Razorpay payment:", error.message);
		res.status(500).json({ message: error.message });
	}

};




/**
 * @desc Get logged in user's orders
 * @router GET/api/orders/myorders
 * @access private
 */

const getMyOrders = async (req, res) => {
	const orders = await Order.find({ user: req.user._id });
	res.json(orders);
}

/**
 * @desc Get all orders
 * @router GET /api/orders
 * @access private/admin
 */

const getOrders = async (req, res) => {
	const orders = await Order.find({}).populate('user', 'name');
	res.json(orders);
}

/**
 * @desc Get total sales
 * @router GET/api/total_sales
 * @access private/admin
 */

const getTotalSales = async (req, res) => {
	try {
		const orders = await Order.find({});
		let totalSales = 0;
		orders.forEach(order => {
			totalSales += order.totalPrice;
		});
		res.json({ totalSales });
	} catch (error) {
		res.status(500).json({ error: 'Internal Server Error' });
	}
}


/**
 * @desc Update order to delivered
 * @router PUT /api/orders/:id
 * @access private/admin
 */

const updateOrderToDelivered = async (req, res) => {
	const order = await Order.findById(req.params.id);
	if (order) {
		order.isDelivered = true;
		order.deliveredAt = Date.now();

		const updatedOrder = await order.save();
		res.json(updatedOrder);
	} else {
		res.status(404);
		throw new Error('Order not found');
	}
}

const updateOrderWithShiprocketId = async (req, res) => {
	const { shiprocketOrderId, shiprocketShipmentId } = req.body;

	const order = await Order.findById(req.params.id);

	if (order) {
		order.shiprocketOrderId = shiprocketOrderId;
		order.shiprocketShipmentId = shiprocketShipmentId;
		const updatedOrder = await order.save();
		res.json(updatedOrder);
	} else {
		res.status(404);
		throw new Error('Order not found');
	}
};
export { createOrder, deleteOrder, createRazorOrder, getOrderById, updateOrderToPaid, updateRazorOrderToPaid, getMyOrders, getOrders, updateOrderToDelivered, getTotalSales, updateOrderWithShiprocketId };//checkout
