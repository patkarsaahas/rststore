import express from 'express';
import { createOrder, createRazorOrder, deleteOrder, getMyOrders, getOrderById, getOrders, getTotalSales, updateOrderToDelivered, updateOrderToPaid, updateOrderWithShiprocketId, updateRazorOrderToPaid } from '../controllers/orderController.js';
import { protect,admin } from '../middlewares/authMiddleware.js';

const router = express.Router();

router.route('/').post(protect, createOrder).get(protect, admin, getOrders)
router.route('/razorpay').post(protect, createRazorOrder);
router.route('/totalsales').get(protect, admin, getTotalSales);
router.route('/myorders').get(protect, getMyOrders);
router.route('/:id').get(protect, getOrderById).delete(protect, deleteOrder);;
router.route('/:id/pay').put(protect, updateOrderToPaid);
router.route('/razorpay/:id/pay').put(protect, updateRazorOrderToPaid)
router.route('/:id/deliver').put(protect, updateOrderToDelivered);
router.route('/:id/update-shiprocket-id').put(protect, updateOrderWithShiprocketId);

export default router;
